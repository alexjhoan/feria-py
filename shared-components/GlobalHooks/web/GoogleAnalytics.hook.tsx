import { useContext, useEffect } from "react";
import { ConfigStateContext } from "../../Contexts/Configurations/context";
import ReactGA from "react-ga";
import { useRouter } from "next/router";

export const useGoogleAnalytics = () => {
	const router = useRouter();
	const { analytics_id } = useContext(ConfigStateContext);
	const IS_BROWSER = typeof window !== "undefined";

	const Init = () => {
		ReactGA.initialize(analytics_id, { titleCase: false });
		window["GA_INITIALIZED"] = true;
		console.log("%cInit Google Analytics", "color:white; background: #faaa00; padding:2px 4px");
	};

	const PageViewEvent = () => {
		try {
			if (IS_BROWSER && window["GA_INITIALIZED"]) {
				ReactGA.set({ version: "IC2", page: window.location.pathname });
				ReactGA.pageview(window.location.pathname);
				console.log(
					"%cGoogle Analytics PageView",
					"color:white; background: #faaa00; padding:2px 4px"
				);
			}
		} catch (err) {}
	};

	const GAevent = ({
		action,
		category,
		label = "",
		value = 1,
	}: {
		action: string;
		category: string;
		label?: string;
		value?: number;
	}) => {
		ReactGA.event({ action: action, category: category, label: label, value: value });
		console.log(
			"%cGoogle Analytics Event",
			"color:white; background: #faaa00; padding:2px 4px"
		);
	};

	useEffect(() => {
		if (IS_BROWSER && !window["GA_INITIALIZED"] && analytics_id) {
			Init();
			PageViewEvent();
			setTimeout(() => {
				GAevent({
					category: "usario_valido",
					action: "el usuario paso mas de 30 segundos",
				});
			}, 30000);

			// al cambiar de pagina con shallow routing que mande el evento de PageView
			const customHistory = [window.location.href];
			router.events.on("routeChangeComplete", (e, e2) => {
				if (customHistory[0] != window.location.href) {
					PageViewEvent();
					customHistory.unshift(window.location.href);
				}
			});
			// end al cambiar de pagina con shallow routing que mande el evento de PageView
		}
	}, [analytics_id]);

	return {
		Event: GAevent,
	};
};
