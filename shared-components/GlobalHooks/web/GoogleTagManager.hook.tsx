import { useContext, useEffect } from "react";
import { ConfigStateContext } from "../../Contexts/Configurations/context";
import TagManager from "react-gtm-module";
import { useUser } from "../../Components/User/User.hook";
import { useRouter } from "next/router";

export const useGoogleTagManager = () => {
	const router = useRouter();
	const { google_tag_manager_id } = useContext(ConfigStateContext);
	const { user, isLoggedIn } = useUser();
	const IS_BROWSER = typeof window !== "undefined";
	const userData = isLoggedIn ? { email: user.data?.email } : {};

	const Init = () => {
		TagManager.initialize({
			dataLayer: {
				user: userData,
			},
			gtmId: google_tag_manager_id,
		});
		window["GTM_INITIALIZED"] = true;
		console.log(
			"%cInit Google Tag Manager",
			"color:white; background: #33a952; padding:2px 4px"
		);
	};

	const GTMevent = ({ name, data }: { name?: string; data?: object }) => {
		if (window["GTM_INITIALIZED"]) {
			TagManager.dataLayer({
				dataLayer: {
					user: userData,
					...data,
				},
				dataLayerName: name || "dataLayer",
			});
		}
	};
	const PageView = () => {
		GTMevent({ name: "PageView", data: { page: window.location.href } });
		console.log(
			"%cGoogle Tag Manager PageView",
			"color:white; background: #33a952; padding:2px 4px"
		);
	};

	useEffect(() => {
		if (IS_BROWSER && !window["GTM_INITIALIZED"] && google_tag_manager_id) {
			Init();
			PageView();
			// al cambiar de pagina con shallow routing que mande el evento de PageView
			const customHistory = [window.location.href];
			router.events.on("routeChangeComplete", (e, e2) => {
				if (customHistory[0] != window.location.href) {
					PageView();
					customHistory.unshift(window.location.href);
				}
			});
			// end al cambiar de pagina con shallow routing que mande el evento de PageView
		}
	}, [google_tag_manager_id]);

	return {
		Event: GTMevent,
	};
};
