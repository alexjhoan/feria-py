import { useContext, useEffect } from "react";
import { useUser } from "../../Components/User/User.hook";
import { ConfigStateContext } from "../../Contexts/Configurations/context";
import Console from "../../../lib/console";
import { useTranslation } from "react-i18next";
import { useGoogleAnalytics } from "./GoogleAnalytics.hook";
import { useRouter } from "next/router";

var OneSignal;

export const useOneSignal = () => {
	const { onesignal_configuration, site_name } = useContext(ConfigStateContext);
	const { t } = useTranslation();
	const GA = useGoogleAnalytics();
	const { isLoggedIn, user } = useUser();
	const IS_BROWSER = typeof window !== "undefined";

	const Init = () => {
		OneSignal = window["OneSignal"] || [];

		OneSignal.push(() => {
			OneSignal.init({
				appId: onesignal_configuration.app_id,
				allowLocalhostAsSecureOrigin: true,
				autoRegister: true,
				notifyButton: { enable: false },
				promptOptions: {
					actionMessage: t(
						"Nos gustaría enviarte notificaciones de las últimas novedades!"
					),
					acceptButtonText: t("Aceptar"),
					cancelButtonText: t("No gracias"),
				},
				welcomeNotification: {
					disable: true,
					title: site_name,
					message: t("Gracias por suscribirte!"),
				},
			}).catch(e => {
				Console.warn("No se pudo initializar OneSignal");
				Console.log(e);
			});
		});

		//guardar que accion tomó el usuario con prompt nativo de permisos del browser
		OneSignal.push(() => {
			OneSignal.on("notificationPermissionChange", function(permissionChange) {
				var currentPermission = permissionChange.to;
				GA.Event({
					action: "WebPushNotifications",
					category: "permissionChange",
					label: currentPermission,
				});
			});
		});

		//trackeo cuando muestro prompt nativo del browser
		OneSignal.push(() => {
			OneSignal.on("permissionPromptDisplay", function(event) {
				GA.Event({
					action: "WebPushNotifications",
					category: "showNativePromptPermission",
				});
			});
		});

		// state check
		if (isLoggedIn) {
			OneSignal.push(function() {
				OneSignal.isPushNotificationsEnabled(function(isEnabled) {
					if (isEnabled) {
						OneSignal.push(function() {
							OneSignal.sendTags({
								ic_user_id: user.data.me.id,
								nombreContacto: user.data.me.me.name,
								ic_email: user.data.me.me.email,
								ic_id_pais: user.data.me.me.country_id,
								particular: user.data.me.me.type,
							});
						});
					}
				});
			});
		}
	};

	useEffect(() => {
		if (IS_BROWSER && !window["OneSignal_INITIALIZED"] && onesignal_configuration) {
			Init();
			window["OneSignal_INITIALIZED"] = true;
			console.log("%cInit One Signal", "color:white; background: #e54b4d; padding:2px 4px");
		}
	}, [onesignal_configuration]);

	return {};
};
