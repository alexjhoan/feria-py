import { useEffect } from "react";

export function WindowEventListener(event, handler, passive = false, trigger) {
  const callbackTriger = trigger ? [trigger] : null;
  useEffect(() => {
    // initiate the event handler
    window.addEventListener(event, handler, passive);

    // this will clean up the event every time the component is re-rendered
    return function cleanup() {
      window.removeEventListener(event, handler);
    };
  }, callbackTriger);
}
