// theme
const theme = {
	images: {
		logo:
			'https://cdn2.infocasas.com.uy/web/5ee3722bdec05_infocdn__logo-infocasas@2x.png',
		favicon:
			'https://cdn1.infocasas.com.uy/web/5ee376daa7764_infocdn__favicon.ico',
		icono:
			'https://cdn2.infocasas.com.uy/web/5ee372edae68f_infocdn__app@2x.png',
	},
	fontFamily: {
		sansSerif:
			'TruliaSans, system, -apple-system, Roboto, Segoe UI Bold, Arial, sans-serif',
	},
	colors: {
		primary: '#fc7b27',
		primaryHover: '#DF6c19',
		primaryOpacity: 'rgba(252, 123, 39, 0.5)',
		secondary: '#4db7ad',
		secondaryHover: '#5BA29C',
		secondaryOpacity: '#5BA29C80',

		text: '#333333',
		textHover: '#777777',
		textLight: '#666666',
		textLightHover: '#555555',
		textLighter: '#a7a7a7',
		textLighterHover: '#777777',

		background: '#fff',
		backgroundLight: '#f6f6f6',
		backgroundHover: '#ededed',

		metric: {
			positive: '#69AD6C',
			negative: '#F24E1E',
		},

		link: '#1eaaf1',
		linkHover: '#0d8ecf',
		border: '#ddd',
		warning: '#fff3cd',
		success: '#d4edda',
		favorite: '#ff6767',
		color1: '#cccccc',
		color2: '#777777',
	},
	breakPoints: {
		xs: '480px',
		sm: '576px',
		md: '768px',
		lg: '992px',
		xl: '1200px',
		xxl: '1600px',
	},
	headerHeight: 64,
	spacing: {
		sm: 8,
		md: 12,
		lg: 16,
		xl: 32,
	},
	fontSizes: {
		text: 16,
		appText: 18,
		notes: 13, // error, small texts in general
	},
	borderRadius: {
		sm: 4,
		md: 8,
		lg: 16,
		xl: 24,
		xxl: 32,
	},
};

export default theme;
