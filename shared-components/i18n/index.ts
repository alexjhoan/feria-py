import i18n from "i18next";
import { langs } from "./Translations";
import { initReactI18next } from "react-i18next";
import intervalPlural from "i18next-intervalplural-postprocessor";

i18n
	.use(intervalPlural)
	.use(initReactI18next)
	.init({
		resources: langs,
		lng: "es",
		fallbackLng: "es",
		keySeparator: false,
		interpolation: {
			escapeValue: false,
		},
	});

export default i18n;
