import { decode as atob, encode as btoa } from "base-64";

export const UY = 1;
export const PY = 2;
export const CO = 3;
export const PE = 4;
export const BO = 5;
export const CEEE = 37;

export const parseMoney = (
	value: any,
	maximumFractionDigits = 0,
	nonSignificantDigits = 0,
	locale: "it-IT" | "en" = "it-IT"
) => {
	const float = parseFloat(value);
	const digits = Math.round(float).toString().length;
	const maximumSignificantDigits =
		digits > nonSignificantDigits ? digits - nonSignificantDigits : digits;
	return float.toLocaleString(locale, { maximumFractionDigits, maximumSignificantDigits });
};
export const formatMoney = (value: any) => {
	value = isNaN(value) ? value : value.toString();
	value = value.replace(/[^\d-]/g, "");
	value = value.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
	return value;
};
export const parserMoney = (value: any) => {
	value = isNaN(value) ? value : value.toString();
	value = value.replace(/[^\d-]/g, "");
	return Number(value);
};
export const isSuperHighlighted = (highlight: number) => {
	return [1, 10, 11].includes(highlight);
};
export const isHighlighted = (highlight: number) => {
	return ![2, 7, 8, 13, 14].includes(highlight);
};

export const isAuction = (operation_type_id: number) => {
	return operation_type_id == 6;
};
export const isTemporal = (operation_type_id?: number) => {
	return operation_type_id == 4;
};

export const isSell = (operation_type_id: number) => {
	return operation_type_id == 1;
};

export const isRent = (operation_type_id: number) => {
	return operation_type_id == 2;
};

export const isCommerce = (property_type: number) => {
	return property_type == 4;
}

export const showGuests = (operation_type_id?: number) => {
	return isTemporal(operation_type_id);
};

export const showBedrooms = (property_type_id: any = [], operation_type_id: number) => {
	return [1, 2].some(e => property_type_id.includes(e)) && !isAuction(operation_type_id);
};

export const showBathrooms = (property_type_id: any = [], operation_type_id: number) => {
	return (
		property_type_id.length == 0 ||
		([1, 2, 4, 5, 9].some(e => property_type_id.includes(e)) && !isAuction(operation_type_id))
	);
};

export const showRooms = (property_type_id: any = [], operation_type_id: number) => {
	return !showBedrooms(property_type_id, operation_type_id) && !property_type_id.includes(3);
};

export const showM2 = (operation_type_id: number) => {
	return !isTemporal(operation_type_id);
};

export const showDateRange = (operation_type_id: number) => {
	return isTemporal(operation_type_id);
};
export const showSeason = (operation_type_id: number, country_id: number) => {
	return isTemporal(operation_type_id) && [UY, CEEE].includes(country_id);
};
export const showTemporalChanger = (country_id: number) => {
	return [UY, CEEE].includes(country_id);
};

export const propertiesTypeInTemporal = (property_type_id: number) => {
	return property_type_id == 1 || property_type_id == 2 || property_type_id == 6;
}

export const showPropState = (property_type_id: any = [], operation_type_id: number) => {
	if (property_type_id && property_type_id.length == 1 && property_type_id[0] == 3) return false;
	return !isTemporal(operation_type_id);
};

export const showPropertyType = (operation_type_id: number) => {
	return !isTemporal(operation_type_id);
};

export const showLocation = (operation_type_id: number) => {
	return !isAuction(operation_type_id);
};

export const showCommonExpenses = (operation_type_id: number) => {
	return operation_type_id == 2;
};
export const showPrice = (operation_type_id: number) => {
	return !isAuction(operation_type_id);
};

export const showFloors = (property_type_id: any = []) => {
	return [2].some(e => property_type_id.includes(e));
};

export const showDispositions = (property_type_id: number[] = []) => {
	return property_type_id.length == 0 || [2].some(e => property_type_id.includes(e));
};

export const showFacilities = (property_type_id: any = [], operation_type_id: number) => {
	return (
		property_type_id.length == 0 ||
		([1, 2, 4, 5, 6, 10, 11, 13].some(e => property_type_id.includes(e)) &&
			!isAuction(operation_type_id))
	);
};

export const showSocialHousing = (property_type_id: any = [], operation_type_id: number) => {
	return (
		(operation_type_id == 1 && property_type_id.length == 0) ||
		[1, 2, 3, 5, 6, 12, 13].some(e => property_type_id.includes(e))
	);
};

export const showPrivateOwner = (operation_type_id: number) => {
	return !isAuction(operation_type_id);
};
export const showSeaDistance = (operation_type_id: number, country_id?: number) => {
	return !isAuction(operation_type_id) && country_id && [UY, PE, CEEE].includes(country_id);
};

export const hideInformationFromText = (text: string, mask: string = "***") => {
	let r = text;
	const pattern_email = "(\\S*@\\S*?\\.\\S+)";
	let re = new RegExp(pattern_email, "gi");
	r = r.replace(re, mask);

	const pattern_url =
		"\\b((?:https?:|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4})(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`\\!()\\[\\]{};:'\".,<>?«»“”‘’]))";
	re = new RegExp(pattern_url, "gi");
	r = r.replace(re, mask);

	const pattern_phone = "((\\+\\s?)?(\\d[\\s|\\.]?){7,16})";
	re = new RegExp(pattern_phone, "gi");
	r = r.replace(re, mask);

	return r;
};
export const encodeHashUrl = (input: object) => {
	let hash = btoa(unescape(encodeURIComponent(JSON.stringify(input))));

	return hash;
};
export const decodeHashUrl = (input: string) => {
	let unHash = JSON.parse(decodeURIComponent(escape(atob(input))));

	return unHash;
};

export const capitalize = (name: string): string => name.charAt(0).toUpperCase() + name.slice(1);

export const removeUrlProtocol = (url: string): string => {
	return url.replace(/(^\w+:|^)\/\//, "//");
};

export const shortNumber = (number: number) => {
	const oneMillion = 1000000;
	const oneThousand = 1000;

	if (number >= oneMillion) {
		return `${Math.round(number / oneMillion)}M`;
	} else if (number >= oneThousand) {
		return `${Math.round(number / oneThousand)}K`;
	}

	return number;
};

export const makePriceText = (price: any): string =>
	`${price.currency?.name} ${parseMoney(price.amount)}`;

export const cleanHtmlFromString = (html: string, keepLineBreaks: boolean = false) => {
	if (html) {
		if (keepLineBreaks) html = html.replace(/<br>/g, "$br$");
		html = html.replace(/<\/?[^>]+(>|$)/g, "");
		html = html.replace(/\$br\$/g, "<br><br>");
	}
	return html;
};

export const postRequest = async (url, data) => {
	const response = await fetch(url, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(data)
	})
	try{
		return response.json()
	} catch {
		return "error"
	}
}
