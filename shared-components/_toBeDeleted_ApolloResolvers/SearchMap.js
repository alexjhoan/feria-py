import {gql} from "@apollo/client";
import {
  APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST,
  APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST,
} from "./constants";

const _PERSIST_ = true;
const _NAME_ = "SearchMap";

const typename =
  (_PERSIST_
    ? APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST
    : APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST) + _NAME_;
const initialState = {
  show: false,
  __typename: typename,
};

export const QUERY_SEARCH_MAP = gql`
    query QUERY_SEARCH_MAP{
        ${typename} @client  {
            show
        }
    }`;
export const TYPENAME_SEARCH_MAP = typename;
export default {
  typename,
  initialState: { [typename]: initialState },
  customQueries: {},
  mutations: {},
};
