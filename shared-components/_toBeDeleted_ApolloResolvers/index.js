import {gql} from "@apollo/client";
import BuscadorPropiedades from "./BuscadorPropiedades";
import SignUpModal from "./SignUpModal";
import SearchMap from "./SearchMap";
import Country from "./Country";
import BannerPopUp from "./BannerPopUp";
import Filters, {TYPENAME_GENERIC_FILTER} from "./Filters";
import SavedFilters from "./SavedFilters";
import URL from "./URL"

export function PutTypeNames(obj, typename) {
  if (
    typeof obj.__typename === "undefined" &&
    typeof typename !== "undefined"
  ) {
    obj.__typename = typename;
  }
  for (const k in obj) {
    if (typeof obj[k] === "object" && !Array.isArray(obj[k])) {
      obj[k].__typename = obj.__typename + "" + k;
    }
    if (typeof obj[k] === "object" && Array.isArray(obj[k])) {
      obj[k] = PutTypeNames(obj[k]);
    }
  }
  if (typeof typename !== "undefined") {
    return { [typename]: obj };
  } else {
    return obj;
  }
}
export function ObjToQuery(obj) {
  let j = "";
  for (const k in obj) {
    if (typeof obj[k] === "object" && !Array.isArray(obj[k])) {
      j = j + ` ${k} ` + ObjToQuery(obj[k]) + " ";
    } else {
      j = j + ` ${k} `;
    }
  }

  if (j.length > 0) {
    return "{ " + j + " } ";
  } else {
    return j;
  }
}


export const addTypenames = (xxx,t) => {
  //this is like a legaxy mode por get url passed params

  const addTextValue = (aaa) => Object.keys(aaa).reduce((acc, o) => {
    if (Array.isArray(aaa[o])) {
      if (aaa[o].length === 0) {
        acc[o] = [];
      } else if (typeof aaa[o][0].value === "undefined") {
        acc[o] = aaa[o].map((o) => {
          return { value: o, text: "auto_generado_array" };
        });
      } else {
        acc[o] = aaa[o];
      }
    } else {
      if (aaa[o] === null) {
        acc[o] = null;
      } else if (typeof aaa[o].value === "undefined") {
        acc[o] = { value: aaa[o], text: "auto_generado" };
      } else {
        acc[o] = aaa[o];
      }
    }
    return acc;
  }, {});


  const addTypename2 = (obj,t) => {
    if (obj !== null && typeof obj === "object") {
      if (typeof obj.__typename === "undefined" && !Array.isArray(obj))
        obj.__typename = t+"_generic_typename";
      for(let prop in obj) {
        addTypename2(obj[prop],t);
      }
    }
  };

  let r = {...xxx}//addTextValue({...xxx});
  addTypename2(r,t);
  return r;
};

export const DefaultLocalState = {
  ...BuscadorPropiedades.initialState,
  ...SignUpModal.initialState,
  ...SearchMap.initialState,
  ...Country.initialState,
  ...BannerPopUp.initialState,
  ...Filters.initialState,
  ...SavedFilters.initialState,
  ...URL.initialState,
};
export const MUTATION_LOCAL_STORE = gql`
  mutation {
    updateLocalStore(new_state: $new_state, query: $query) @client
  }
`;
export function StateResolvers(getState, writeState) {
  return {
    Query: {
      CustomQueryQueNoSeUsa(_, appState) {
        const x = getState(gql`
          query MESADA {
            state @client {
              appState {
                isDarkModeEnabled
              }
            }
          }
        `);
        return {
          appState: {
            isDarkModeEnabled: false,
            __typename: "AppState",
          },
          __typename: "State",
        };
      },
      ...BuscadorPropiedades.customQueries,
      ...SignUpModal.customQueries,
      ...SearchMap.customQueries,
      ...Country.customQueries,
      ...BannerPopUp.customQueries,
      ...Filters.customQueries,
      ...SavedFilters.customQueries,
      ...URL.customQueries,
    },
    Mutation: {
      CustomMutationNoSeUsa(_, { newAppState }) {
        // get current / initial state from cache
        const state = getState(gql`
          query MESADA {
            state @client {
              appState {
                isDarkModeEnabled
              }
            }
          }
        `);
        const newState = {
          ...state,
          appState: Object.assign({}, state.appState, newAppState),
        };
        writeState(newState);
        return newState;
      },
      updateLocalStore: (_, x, context) => {
        const previous = getState(x.query);
        const data = {
          [x.query.definitions[0].selectionSet.selections[0].name.value]: addTypenames({
            ...previous[
                x.query.definitions[0].selectionSet.selections[0].name.value
                ],
            ...x.new_state,
          },x.query.definitions[0].selectionSet.selections[0].name.value),
        };

        writeState({ query: x.query, data: data });
        return data;
      },

      ...BuscadorPropiedades.mutations,
      ...SignUpModal.mutations,
      ...SearchMap.mutations,
      ...Country.mutations,
      ...BannerPopUp.mutations,
      ...Filters.mutations,
      ...SavedFilters.mutations,
      ...URL.mutations,
    },
  };
}
