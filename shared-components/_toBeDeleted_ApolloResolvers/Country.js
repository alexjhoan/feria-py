import {gql} from "@apollo/client";
import {
  APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST,
  APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST,
} from "./constants";

const _PERSIST_ = true;
const _NAME_ = "Country";

const typename =
  (_PERSIST_
    ? APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST
    : APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST) + _NAME_;

const initialState = {
  country_id: null,
  country_name: null,
  country_code: null,
  main_domain: "www.infocasas.com.uy",
  country_flag: null,
  __typename: typename,
};

export const QUERY_COUNTRY = gql`
    query QUERY_COUNTRY{
        ${typename} @client  {
            country_id
            country_name
            country_code
            main_domain
            country_flag
            __typename
        }
    }
`;

export const TYPENAME_COUNTRY = typename;
export default {
  typename,
  initialState: { [TYPENAME_COUNTRY]: initialState },
  customQueries: {},
  mutations: {},
};
