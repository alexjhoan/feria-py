import {gql} from "@apollo/client";
import {
  APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST,
  APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST,
} from "./constants";

const _PERSIST_ = true;
const _NAME_ = "BuscadorPropiedades";

const typename =
  (_PERSIST_
    ? APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST
    : APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST) + _NAME_;
const initialState = {
  operacion: [1],
  tipos: [1, 2],
  season: ["diciembre"],
  dates: {
    start: null,
    end: null,
    __typename: typename + "Dates",
  },
  __typename: typename,
};

export const QUERY_BUSCADOR_PROPIEDADES = gql`
    query QUERY_BUSCADOR_PROPIEDADES{
        ${typename} @client  {
              operacion
              tipos
              season
              dates {
                start
                end
              }
            }
        
    }`;
export const QUERY_OPERATION = gql`
  query QUERY_BUSCADOR_PROPIEDADES{
      ${typename} @client  {
        operacion
      }  
  }`;
export const TYPENAME_BUSCADOR_PROPIEDADES = typename;
export default {
  typename,
  initialState: { [typename]: initialState },
  customQueries: {},
  mutations: {},
};
