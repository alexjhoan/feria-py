import {gql} from "@apollo/client";
import {
  APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST,
  APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST,
} from "./constants";

const _PERSIST_ = true;
const _NAME_ = "BannerPopUp";

const typename =
  (_PERSIST_
    ? APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST
    : APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST) + _NAME_;
const initialState = {
  last: 0,
  __typename: typename,
};

export const QUERY_LAST_BANNER_POPUP = gql`
    query QUERY_LAST_BANNER_POPUP{
        ${typename} @client  {
            last
        }
    }`;
export const TYPENAME_LAST_BANNER_POPUP = typename;
export default {
  typename,
  initialState: { [typename]: initialState },
  customQueries: {},
  mutations: {},
};
