import {gql} from "@apollo/client";
import {APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST, APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST} from "./constants";

const _PERSIST_ = false;
const _NAME_ = "SingUpModal";

const typename = (_PERSIST_?APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST:APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST) + _NAME_;
const initialState = {
  open: false,
  type: "inicio",
  __typename: typename
};

export const QUERY_SIGN_UP_MODAL = gql`
    query QUERY_SIGN_UP_MODAL{
        ${typename} @client  {
            open
            type
        }
    }`;
export const TYPENAME_SIGN_UP_MODAL = typename;
export default {
  typename,
  initialState: { [typename]: initialState },
  customQueries: {},
  mutations: {}
};
