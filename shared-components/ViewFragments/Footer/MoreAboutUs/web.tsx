import { Space, Typography } from "antd";

import { AppLinks } from "./AppLinks/web";
import { Flags } from "./Flags/web";
import { useTranslation } from "react-i18next";

const { Title } = Typography;

export const MoreAboutUs = () => {
	const { t } = useTranslation();

	return (
		<div className="more-about-us">
			<Title level={4}>{t("Más InfoCasas")}</Title>
			<Space direction="vertical" size="middle">
				<AppLinks />
				<Flags />
			</Space>
		</div>
	);
};
