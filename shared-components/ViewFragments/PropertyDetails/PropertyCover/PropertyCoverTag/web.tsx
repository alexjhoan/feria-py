import {
	CompassOutlined,
	FileDoneOutlined,
	PictureOutlined,
	PieChartOutlined,
	VideoCameraOutlined,
} from "@ant-design/icons";
import { Skeleton, Space, Tag } from "antd";

import React from "react";
import { usePropertyCoverTag } from "./hook";
import { useTheme } from "../../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

export const PropertyCoverTags = ({ id, onClick }) => {
	const { image_count, has_map, has_video, has_floorPlans, loading } = usePropertyCoverTag({
		id,
	});
	const { t } = useTranslation();
	const { theme } = useTheme();

	const onTabClick = (e, tab) => {
		e.stopPropagation();
		onClick(tab);
	};

	if (loading)
		return (
			<Space size={theme.spacing.smSpacing}>
				{[...Array(3)].map((o, i) => (
					<Skeleton
						key={"property-cover-tags_sk_" + i}
						title={false}
						active
						round
						paragraph={{ rows: 1, width: 50, style: { margin: 0 } }}
					/>
				))}
			</Space>
		);

	return (
		<>
			<div className={"pd-property-cover-tags"}>
				{image_count > 0 && (
					<Tag
						onClick={e => onTabClick(e, "photos")}
						color={"default"}
						icon={<PictureOutlined />}>
						{image_count}
					</Tag>
				)}
				{has_video && (
					<Tag
						onClick={e => onTabClick(e, "video")}
						color={"default"}
						icon={<VideoCameraOutlined />}>
						{t("Video")}
					</Tag>
				)}
				{has_map && (
					<Tag
						onClick={e => onTabClick(e, "map")}
						color={"default"}
						icon={<CompassOutlined />}>
						{t("Mapa")}
					</Tag>
				)}
				{has_floorPlans && (
					<Tag
						onClick={e => onTabClick(e, "floorPlans")}
						color={"default"}
						icon={<FileDoneOutlined />}>
						{t("Planos")}
					</Tag>
				)}
				{/* <Tag
					onClick={e => onTabClick(e, "bigData")}
					color={"default"}
					icon={<PieChartOutlined />}>
					{t("Big Data")}
				</Tag> */}
			</div>
			<style jsx global>{`
				.pd-property-cover-tags .ant-tag {
					border-color: transparent;
					background: ${theme.colors.textColor};
					color: ${theme.colors.backgroundColor};
					cursor: pointer;
				}
				.pd-property-cover-tags .ant-tag:last-child {
					margin-right: 0;
				}
			`}</style>
		</>
	);
};
