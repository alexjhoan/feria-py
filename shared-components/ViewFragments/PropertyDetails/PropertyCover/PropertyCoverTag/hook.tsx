import { FragmentDefiner, useReadFragment } from "../../../../GlobalHooks/useReadFragment";

export const FRAGMENT_PROPERTY_COVER_TAG = new FragmentDefiner(
	"Property",
	`
    image_count
    youtube
    files {
        id
    }
    latitude
`
);

interface usePropertyCoverTagResponse {
	image_count: number;
	has_video: boolean;
	has_map: boolean;
	has_floorPlans: boolean;
	loading: boolean;
}

export const usePropertyCoverTag = ({ id }): usePropertyCoverTagResponse => {
	const { data, loading } = useReadFragment(FRAGMENT_PROPERTY_COVER_TAG, id);

	return {
		image_count: data?.image_count,
		has_video: data?.video,
		has_map: data?.latitude && data.latitude != 0 ? true : false,
		has_floorPlans: data?.files.length > 0,
		loading,
	};
};
