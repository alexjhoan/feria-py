import { gql } from "@apollo/client";
import { useQuery } from "@apollo/client";
import { FRAGMENT_TYPOLOGYTAG } from "../../Components/Property/TypologyTag/TypologyTag.hook";
import { FRAGMENT_PRICETAG } from "../../Components/Property/PriceTag/PriceTag.hook";
import { FRAGMENT_DESCRIPTION } from "../../Components/Property/Description/Description.hook";
import { FRAGMENT_TITLE } from "../../Components/Property/Title/Title.hook";
import { FRAGMENT_FACILITIES } from "../../Components/Property/Facilities/Facilities.hook";
import { FRAGMENT_TECHNICAL_SHEET } from "../../Components/Property/TechnicalSheet/hook";
import { FRAGMENT_LOCATIONTAG } from "../../Components/Property/LocationTag/LocationTag.hook";
import { FRAGMENT_PROPERTY_COVER } from "./PropertyCover/hook";
import { FRAGMENT_PROPERTY_MODAL } from "./PropertyModal/PropertyModal.hook";
import { FRAGMENT_SOCIAL_SHARE } from "../../Components/Property/SocialShare/hook";
import { FRAGMENT_MASKED_PHONE } from "../../Components/Property/InformationRequest/LeadButtons/PhoneButton/hook";
import { FRAGMENT_SUBSIDIARIES } from "../../Components/Property/Subsidiaries/hook";
import { FRAGMENT_HAS_WHATSAPP } from "../../Components/Property/InformationRequest/LeadButtons/WhatsappButton/hook";
import { FRAGMENT_OCCUPANCIES } from "../../Components/Property/AvailabilityCalendar/hook";
import { FRAGMENT_PROPERTY_BIGDATA } from "../../Components/BigData/SingleProp/hook";
import { FRAGMENT_PROPERTY_MODAL_FLOOR_PLANS } from "./PropertyModal/PropertyModalFloorPlans/PropertyModalFloorPlans.hook";
import { FRAGMENT_PROJECT_INFO } from "../../Components/Project/Info/hook";
import { FRAGMENT_UNITS } from "../../Components/Project/Units/hook";
import { FRAGMENT_WORK_PROGRESS } from "../../Components/Project/WorkProgress/WorkProgress.hook";
import { useState, useEffect } from "react";
import { FRAGMENT_TEMPORAL_SEASONS } from "../../Components/Property/TemporaryRental/hook";
import { useFilters } from "../../Components/Filters/Filters.hook";

const QUERY_PROPERTY_DETAILS = gql`
    query propertyDetails($id: ID!) {
        property(id:$id) {
            __typename
            id
            country_id
            active
            ${FRAGMENT_PRICETAG.query()}
            ${FRAGMENT_TYPOLOGYTAG.query()}
            ${FRAGMENT_PROPERTY_COVER.query()}
            ${FRAGMENT_TITLE.query()}
            ${FRAGMENT_DESCRIPTION.query()}
            ${FRAGMENT_FACILITIES.query()}
            ${FRAGMENT_TECHNICAL_SHEET.query()}
            ${FRAGMENT_LOCATIONTAG.query()}
            ${FRAGMENT_PROPERTY_MODAL.query()}
            ${FRAGMENT_SOCIAL_SHARE.query()}
            ${FRAGMENT_MASKED_PHONE.query()}
            ${FRAGMENT_SUBSIDIARIES.query()}
            ${FRAGMENT_HAS_WHATSAPP.query()}
            ${FRAGMENT_PROPERTY_BIGDATA.query()}
            ${FRAGMENT_PROPERTY_MODAL_FLOOR_PLANS.query()}
            ${FRAGMENT_OCCUPANCIES.query()}
            ${FRAGMENT_TEMPORAL_SEASONS.query()}
        }
    }
`;

const QUERY_PROJECT_DETAILS = gql`
    query projectDetails($id: ID!) {
        project(id:$id) {
            __typename
            id
            active
            commercial_units {
                id
                active
                ${FRAGMENT_PRICETAG.query()}
                ${FRAGMENT_TYPOLOGYTAG.query()}
                ${FRAGMENT_PROPERTY_COVER.query()}
                ${FRAGMENT_TITLE.query()}
                ${FRAGMENT_DESCRIPTION.query()}
                ${FRAGMENT_FACILITIES.query()}
                ${FRAGMENT_TECHNICAL_SHEET.query()}
                ${FRAGMENT_LOCATIONTAG.query()}
                ${FRAGMENT_PROPERTY_MODAL.query()}
                ${FRAGMENT_SOCIAL_SHARE.query()}
                ${FRAGMENT_MASKED_PHONE.query()}
                ${FRAGMENT_SUBSIDIARIES.query()}
                ${FRAGMENT_HAS_WHATSAPP.query()}
                ${FRAGMENT_PROPERTY_MODAL_FLOOR_PLANS.query()}
                ${FRAGMENT_PROJECT_INFO.query()}
                ${FRAGMENT_UNITS.query()}
                ${FRAGMENT_WORK_PROGRESS.query()}
            }
        }
    }
`;

const usePropertyDetails = ({ id, mode }) => {
	const QUERY = mode === "project" ? QUERY_PROJECT_DETAILS : QUERY_PROPERTY_DETAILS;
	const { data, loading, error } = useQuery(QUERY, {
		variables: {
			id,
        },
	});
    const [propId, setPropId] = useState();
    const [operationType, setOperationType] = useState(null);
    const { changeFilters } = useFilters();
    const [isActive, setIsActive] = useState<boolean>(true);

	useEffect(() => {
        if(data) {
            if (mode === "project" && data.project?.commercial_units.length) {
                setPropId(data.project.commercial_units[0].id);
                changeFilters({
                    operation_type_id: {
                        text: data.project.commercial_units[0].property_type?.name,
                        value: data.project.commercial_units[0].property_type?.id
                    }
                });
                setIsActive(data.project?.active);
            }else{
                setIsActive(data.property?.active);
                
                setOperationType(data.property.operation_type?.id);
                changeFilters({
                    operation_type_id: {
                        text: data.property.operation_type?.name,
                        value: data.property.operation_type?.id
                    }
                });
                setPropId(id);
            }
        }
	}, [data]);

	return {
		loading,
		data: {
            propId,
            operationType,
            isActive,
			...data,
		},
        error,
	};
};

export { usePropertyDetails };
