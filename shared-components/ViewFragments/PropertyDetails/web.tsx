import "./styles.less";

import { BreadCrumbs, SeoText } from "../../Components/SEO/web";
import { Card, Col, Grid, Modal, Row, Typography } from "antd";
import React, { useEffect, useState } from "react";

import { AvailabilityCalendar } from "../../Components/Property/AvailabilityCalendar/web";
import { BigDataSingleProp } from "../../Components/BigData/SingleProp/web";
import { CloseCircleOutlined } from "@ant-design/icons";
import { Description } from "../../Components/Property/Description/web";
import { Facilities } from "../../Components/Property/Facilities/web";
import { Financing } from "../../Components/Property/Financing/web";
import { Footer } from "../Footer/web";
import { InformationRequest } from "../../Components/Property/InformationRequest/web";
import { Navigation } from "./Navigation/web";
import { OnScreen } from "../../Components/OnView/web";
import { PicHomeFooter } from "../Footer/PicHomeFooter/web";
import { PriceTag } from "../../Components/Property/PriceTag/web";
import { ProjectInfo } from "../../Components/Project/Info/web";
import { PropComponentMode } from "../../Components/Property/PropertyInterfaces";
import { PropertyAffixBottom } from "./PropertyAffixBottom/web";
import { PropertyAffixHeader } from "./PropertyAffixHeader/web";
import { PropertyCover } from "./PropertyCover/web";
import { PropertyLocalInformationMap } from "./PropertyLocalInformation/web";
import { PropertyModal } from "./PropertyModal/web";
import { PropertySimilars } from "./PropertySimilars/web";
import { Subsidiaries } from "../../Components/Property/Subsidiaries/web";
import { TechnicalSheet } from "../../Components/Property/TechnicalSheet/web";
import { TemporaryRental } from "../../Components/Property/TemporaryRental/web";
import { Title } from "../../Components/Property/Title/web";
import { TypologyTag } from "../../Components/Property/TypologyTag/web";
import { Units } from "../../Components/Project/Units/web";
import { WorkProgress } from "../../Components/Project/WorkProgress/web";
import { isTemporal } from "../../Utils/Functions";
import { useAuthCheck } from "../../Components/User/useAuthCheck";
import { useGoogleTagManager } from "../../GlobalHooks/web/GoogleTagManager.hook";
import { useMarkSeen } from "../../Components/Property/SeenStatus/SeenStatus.hook";
import { usePropertyDetails } from "./PropertyDetails.hook";
import { useTheme } from "../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";
import Paragraph from "antd/lib/typography/Paragraph";
import { PropertySimilarsCards } from "./PropertySimilars/PropertySimilarsCards/web";

const { Text } = Typography;
const { useBreakpoint } = Grid;

interface PropertyDetailsProps {
	id: string;
	mode?: PropComponentMode;
}

export function PropertyDetails({ id, mode = "auto" }: PropertyDetailsProps) {
	const {
		data: { propId, operationType, property, isActive },
		loading,
		error,
	} = usePropertyDetails({ id, mode });
	const { t } = useTranslation();
	const { theme } = useTheme();
	const screen = useBreakpoint();
	const GTM = useGoogleTagManager();

	const [isModalOpen, setModalOpen] = useState(false);
	const [modalTabOpen, setModalTabOpen] = useState("photos");
	const [showAffix, setShowAffix] = useState(false);
	const [showAffixBottom, setShowAffixBottom] = useState(true);
	const { markSeen } = useMarkSeen();
	const { isLoggedIn } = useAuthCheck();

	useEffect(() => {
		if (isLoggedIn) markSeen(id);
	}, [isLoggedIn]);

	useEffect(() => {
		if (property) {
			GTM.Event({
				name: "offerdetail",
				data: {
					listing_id: property.id,
					listing_pagetype: "offerdetail",
					listing_totalvalue: property.price.amount,
				},
			});
		}
	}, [property]);

	return (
		<div className="property-detail">
			{/* Show Affix && Property Affix Header */}
			{showAffix && screen.lg && (
				<PropertyAffixHeader id={propId} operationType={operationType} />
			)}

			{/* Property Modal */}
			<Modal
				className="pd-property-modal"
				visible={isModalOpen}
				centered
				footer={null}
				closeIcon={<CloseCircleOutlined />}
				onCancel={() => setModalOpen(false)}>
				<PropertyModal
					activeTab={modalTabOpen}
					onChangeTab={tab => setModalTabOpen(tab)}
					id={propId}
					mode={mode}
				/>
			</Modal>

			{/* Main */}
			<Row gutter={[0, theme.spacing.xxlSpacing]} className="pd-main">
				<Col span={24} className="pd-main-top">
					<OnScreen
						onEnterView={() => setShowAffix(false)}
						onLeaveView={() => setShowAffix(true)}>
						<Row
							className="pd-main-top-row"
							gutter={[
								0,
								{
									xs: theme.spacing.lgSpacing,
									sm: theme.spacing.lgSpacing,
									lg: theme.spacing.xlSpacing,
								},
							]}>
							<Col span={24}>
								<Row
									gutter={[
										0,
										{
											xs: theme.spacing.smSpacing,
											sm: theme.spacing.smSpacing,
											lg: theme.spacing.mdSpacing,
										},
									]}>
									{/* Navigation */}
									{screen.lg && (
										<Col span={0} lg={24} className={"pd-navigation-col"}>
											<div className="container">
												<Navigation type="default" />
											</div>
										</Col>
									)}

									{/* In Active Prop Declaimer */}
									{ !isActive && (<>
										<Col span={0} lg={24}>
											<div className="container">
												<Card bodyStyle={{background: theme.colors.warningColor}}>
													<Typography.Title style={{margin: 0, textAlign: 'center'}} level={5}>
														{t('Estás viendo esta página porque la propiedad que estabas buscando no está disponible.')}
													</Typography.Title>
												</Card>
											</div>
										</Col>
										<Col span={24}>
											<div className="container pd-container">
												<PropertySimilarsCards id={propId} mode={mode} />
											</div>
										</Col>
									</>)}

									{/* Property Cover */}
									<Col span={24} className={"pd-property-cover-col"}>
										<div className="container">
											<PropertyCover
												mode={mode}
												id={propId}
												onTabClick={tab => {
													setModalTabOpen(tab);
													setModalOpen(true);
												}}
											/>
										</div>
									</Col>
								</Row>
							</Col>
							<Col span={24} className={"pd-main-content"}>
								<div className="container pd-container">
									<Row
										gutter={[theme.spacing.lgSpacing, theme.spacing.smSpacing]}>
										<Col span={24} lg={16}>
											<Row
												gutter={[
													{
														sm: theme.spacing.smSpacing,
														xs: theme.spacing.smSpacing,
														lg: theme.spacing.mdSpacing,
													},
													theme.spacing.lgSpacing,
												]}>
												{/* Title */}
												<Col span={24} lg={14}>
													<Title
														id={propId}
														limitLength={2}
														hideInformation={true}
														mode={mode}
													/>
													{/* Typology Tag Desktop */}
													<TypologyTag
														className="property-typology-tag-desktop"
														id={propId}
														mode={mode}
														showIcons
													/>
												</Col>

												{/* Price Tag */}
												{!isTemporal(operationType) ? (
													<Col span={14} lg={10}>
														<PriceTag
															id={propId}
															showOperationType={true}
															shortExpensesText={false}
															mode={mode}
														/>
													</Col>
												) : null}

												{/* Typology Tag Mobile */}
												<Col span={10} lg={24}>
													<TypologyTag
														className="property-typology-tag-mobile"
														id={propId}
														mode={mode}
														showIcons
													/>
												</Col>

												{/* Property Local Information */}
												<Col span={24}>
													<PropertyLocalInformationMap
														onStreetViewClick={() => {
															setModalTabOpen("streetView");
															setModalOpen(true);
														}}
														onMapClick={() => {
															setModalTabOpen("map");
															setModalOpen(true);
														}}
														id={propId}
														mode={mode}
													/>
												</Col>
											</Row>
										</Col>

										{/* Information Request */}
										<Col
											span={0}
											lg={8}
											className="col-information-request-small-left">
											<InformationRequest
												id={propId}
												identifier={2}
												mode={mode}
											/>
										</Col>
									</Row>
								</div>
							</Col>
						</Row>
					</OnScreen>
				</Col>

				{/* Technical Sheet */}
				{mode === "property" && (
					<Col span={24}>
						<div className="container pd-container">
							<Typography.Title level={4}>
								{t("Detalles de la Popiedad")}
							</Typography.Title>
							<TechnicalSheet mode={mode} id={propId} />
						</div>
					</Col>
				)}

				{/* Infomación del proyecto */}
				{mode === "project" && (
					<Col span={24}>
						<div className="container pd-container">
							<Typography.Title level={4}>
								{t("Información del proyecto")}
							</Typography.Title>
							<ProjectInfo id={propId} />
						</div>
					</Col>
				)}

				{/* Ambientes && Amenities */}
				<Col span={24}>
					<div className="container pd-container">
						<Facilities id={propId} mode={mode}/>
					</div>
				</Col>

				{/* Description */}
				{(property?.description || mode === 'project') && (
					<Col span={24}>
						<div className="container pd-container">
							<Description
								id={propId}
								hideInformation={false}
								mode={mode}
								showTitle={true}
								keepLineBreaks
							/>
						</div>
					</Col>
				)}

				{/* Calendar */}
				{isTemporal(operationType) ? (
					<Col span={24}>
						<div className="container pd-container">
							<Typography.Title level={4}>
								{t("Calendario de disponibilidad")}
							</Typography.Title>
							<AvailabilityCalendar id={propId} />
						</div>
					</Col>
				) : null}

				{/* Temporary Rental */}
				{isTemporal(operationType) ? (
					<Col span={24}>
						<div className="container pd-container">
							<Typography.Title level={4}>{t("Alquiler Temporal")}</Typography.Title>
							<TemporaryRental id={propId} />
						</div>
					</Col>
				) : null}

				{/* Financiación */}
				<Col span={24}>
					<div className="container pd-container">
						<Financing id={propId} operationType={operationType} countryId={property?.country_id} propertyType={property?.property_type?.id} />
					</div>
				</Col>

				{/* Unidades */}
				{mode === "project" && (
					<Col span={24}>
						<div className="container pd-container">
							<Typography.Title level={4}>{t("Unidades")}</Typography.Title>
							<Units id={propId} />
						</div>
					</Col>
				)}

				{/* Avances de obra */}
				{mode === "project" && (
					<Col span={24}>
						<div className="container pd-container">
							<WorkProgress id={propId} />
						</div>
					</Col>
				)}

				{/* Big Data Single Prop */}
				{mode !== "project" && !isTemporal(operationType) && (
					<Col span={24}>
						<div className="container pd-container">
							<BigDataSingleProp id={propId} />
						</div>
					</Col>
				)}

				{/* Property Similars */}
				<Col span={24}>
					<div className="container pd-container">
						<PropertySimilars
							id={propId}
							mode={mode}
							isTemporal={isTemporal(operationType)}
						/>
					</div>
				</Col>

				{/* Sub Sidiaries && Information Request */}
				<Col span={24}>
					<div className="container pd-ir-container-big pd-container">
						<OnScreen
							onEnterView={() => setShowAffixBottom(false)}
							onLeaveView={() => setShowAffixBottom(true)}>
							<InformationRequest id={propId} identifier={3} mode={mode}>
								<Subsidiaries id={propId} mode={mode} />
							</InformationRequest>
						</OnScreen>
					</div>
				</Col>

				{/* Seo Text */}
				<Col span={24}>
					<div className="container pd-container">
						<SeoText />
					</div>
				</Col>

				{/* Seo Text */}
				<Col span={24} lg={0}>
					<div className="container pd-container">
						<Text>
							<BreadCrumbs />
						</Text>
					</div>
				</Col>

				{/* footer */}
				<Col span={24}>
					<div className="container pd-container">
						<Footer />
					</div>
				</Col>
			</Row>

			{/* Footer Image */}
			<PicHomeFooter />

			{/* show Affix Bottom && Property Affix Bottom */}
			{showAffixBottom && !screen.lg && <PropertyAffixBottom id={propId} mode={mode} />}

			<style jsx global>{`
				.header_container .header {
					border-bottom: 1px solid ${theme.colors.borderColor};
				}
			`}</style>
		</div>
	);
}
