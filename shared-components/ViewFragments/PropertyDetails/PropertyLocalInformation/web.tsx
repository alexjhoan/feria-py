import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import { Typography, Row, Col, Carousel, Skeleton } from "antd";
import { LocationTag } from "../../../Components/Property/LocationTag/web";
import { PropertyMap } from "../../../Components/Property/PropertyMap/web";
import { CarouselArrow } from "../../../Components/Property/LazyImageGallery/CarouselArrow/web";
import { PropertyStreetViewStatic } from "../../../Components/Property/PropertyStreetView/web";
import { useTheme } from "../../../Styles/ThemeHook";
import "./styles.less";

const { Title, Paragraph } = Typography;

export function PropertyLocalInformation({ id }) {
	const { t } = useTranslation();
	const { theme } = useTheme();

	const data = [
		{
			title: "Mapa",
			paragraph: "Explora la zona que rodea el inmueble.",
			icon: (
				<PropertyMap
					id={id}
					width={"100px"}
					height={"100px"}
					type={"leaflet"}
					defaultZoom={15}
					options={{
						dragging: false,
						keyboard: false,
						noMoveStart: true,
						doubleClickZoom: false,
						scrollWheelZoom: false,
						touchZoom: false,
						zoomControl: false,
					}}
				/>
			),
			// onClick: () => openPropertyModal({ tab: "Mapa" }),
		},
		{
			title: "Street View",
			paragraph: "Inicia una recorrida virtual al rededor de la propiedad.",
			icon: <img src="" alt="" />,
			// onClick: () => openPropertyModal({ tab: "StreetView" }),
		},
		{
			title: "Big Data",
			paragraph: "Analiza los datos de la zona para tomar mejores decisiones.",
			icon: <img src="" alt="" />,
			// onClick: () => openPropertyModal({ tab: "BigData" }),
		},
		{
			title: "Escuelas",
			paragraph: "Inicia una recorrida virtual al rededor de la propiedad.",
			icon: <img src="" alt="" />,
			// onClick: () => openPropertyModal({ tab: "EscuelasYNegocios" }),
		},
	];

	const [settings, setSettings] = useState({
		render: true,
		arrows: true,
		dots: false,
		nextArrow: <CarouselArrow side={"right"} />,
		prevArrow: <CarouselArrow side={"left"} />,
		responsive: [],
		infinite: true,
		speed: 500,
		autoplaySpeed: 5500,
		autoplay: false,
		slidesToShow: 4,
		slidesToScroll: 1,
	});

	useEffect(() => {
		setSettings({
			...settings,
			render: false,
			responsive: [
				{
					breakpoint: Number(
						theme.breakPoints.md.substring(0, theme.breakPoints.md.length - 2)
					),
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						dots: true,
					},
				},
				{
					breakpoint: Number(
						theme.breakPoints.sm.substring(0, theme.breakPoints.sm.length - 2)
					),
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						dots: true,
					},
				},
				{
					breakpoint: Number(
						theme.breakPoints.xs.substring(0, theme.breakPoints.xs.length - 2)
					),
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						dots: true,
					},
				},
			],
		});
	}, []);

	useEffect(() => {
		if (!settings.render) setSettings({ ...settings, render: true });
	}, [settings.render]);
	if (!settings.render) return null;

	return (
		<>
			<div className="propertyLocalInformation">
				<Row>
					<Col span={12}>
						<Title level={4}>{t("Información Local")}</Title>
					</Col>
					<Col span={12}>
						<LocationTag id={id} showAddress={true} />
					</Col>
				</Row>
				<Carousel {...settings}>
					{data.map((item, i) => (
						<div key={"propertyLocalInformation" + i}>
							<Row
								justify={"center"}
								style={{ textAlign: "center", width: "calc(100% - 16px)" }}>
								<Col span={24}>
									<Skeleton.Image />
								</Col>
								<Col span={24}>
									<Title level={3}>{t(item.title)}</Title>
								</Col>
								<Col span={24}>
									<Paragraph>{t(item.paragraph)}</Paragraph>
								</Col>
							</Row>
						</div>
					))}
				</Carousel>
			</div>
		</>
	);
}

// Este es el que se esta usando como PropertyLocalInformation
export const PropertyLocalInformationMap = ({
	id,
	onStreetViewClick,
	onMapClick,
	mode = "auto",
}) => {
	const { t } = useTranslation();
	const { theme } = useTheme();
	return (
		<>
			<div className="property-local-information-map">
				<Row
					gutter={[
						theme.spacing.xsSpacing,
						{ sm: theme.spacing.xsSpacing, xs: theme.spacing.xsSpacing, lg: 0 },
					]}>
					<Col span={8}>
						<Title level={4}>{t("Ubicación")}</Title>
					</Col>
					<Col span={16} className="address">
						<LocationTag id={id} showAddress={true} mode={mode} />
					</Col>
					<Col span={24} className="map-col">
						<div className="map-container" onClick={onMapClick}>
							<PropertyMap
								id={id}
								width={"100%"}
								height={"100%"}
								options={{
									scrollWheelZoom: false,
									dragging: false,
									zoomControl: false,
								}}
							/>
						</div>

						<div className="streetview-container">
							<PropertyStreetViewStatic
								id={id}
								size={"small"}
								onClick={onStreetViewClick}
							/>
						</div>
					</Col>
				</Row>
			</div>
			<style jsx global>{`
				.property-local-information-map .map-col {
					border-radius: ${theme.spacing.mdSpacing}px;
					position: relative;
				}
				.property-local-information-map .map-col .streetview-container {
					position: absolute;
					z-index:5;
					right: ${theme.spacing.mdSpacing}px;
					bottom: ${theme.spacing.mdSpacing}px;
					width: 140px;
					height: 110px;
					text-align: center;
					color: ${theme.colors.backgroundColor};
					border: 2px solid white;
					border-color: ${theme.colors.backgroundColor};
					border-radius: ${theme.spacing.smSpacing}px;
					overflow: hidden;
			`}</style>
		</>
	);
};
