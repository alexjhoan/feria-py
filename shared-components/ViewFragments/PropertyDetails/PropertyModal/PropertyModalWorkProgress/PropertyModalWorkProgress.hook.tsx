import { FragmentDefiner, useReadFragment } from "../../../../GlobalHooks/useReadFragment";
import { useEffect } from "react";
import { PropComponentMode } from "../../../../Components/Property/PropertyInterfaces";

export interface PropWorkProgressProps{
	id:number
	mode: PropComponentMode
}

export const FRAGMENT_PROPERTY_MODAL_FLOOR_PLANS = new FragmentDefiner(
	"Property",
	`
	id
	project {
		id
		files {
			id
			file
			title
		}
	}
    files {
        id
        file
        title
    }
`
);

export function usePropertyModalWorkProgress({ id, mode }:PropWorkProgressProps) {
	const { loading, data, error } = useReadFragment(FRAGMENT_PROPERTY_MODAL_FLOOR_PLANS, id);

	return {
		loading,
		data,
		floorPlans: mode == "project" ? data?.project[0]?.files : data?.files,
		error,
	};
}
