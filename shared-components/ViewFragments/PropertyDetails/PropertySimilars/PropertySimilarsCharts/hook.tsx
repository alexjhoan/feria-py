import {usePropertySimilars} from "../hook";
import {FragmentDefiner, useReadFragment} from "../../../../GlobalHooks/useReadFragment";
import { useState, useEffect } from "react";

export const FRAGMENT_PROPERTY_SIMILARS_CHARTS = new FragmentDefiner("Property",`
    price {
        amount
        currency {
           name
        }
    }
    price_amount_usd
    m2
    link
    isExternal
`);


export const usePropertySimilarsCharts = ({property_id}) => {
    const {loading, data, error} = usePropertySimilars({property_id});
    const [validData, setValidData] = useState([]);
    const [chartPrice, setChartPrice] = useState([]);
    const [chartPriceM2, setChartPriceM2] = useState([]);

    useEffect(() => {
        if(data){
            setValidData([...data].filter(({ m2 }) => m2 != 0));
        }
    }, [data]);

    useEffect(() => {
        if(validData.length > 0){
            setChartPrice(validData.sort((a, b) => (a.price_amount_usd > b.price_amount_usd) ? 1 : -1).map(o => {
                return {
                    id: o.id,
                    value: o.price_amount_usd,
                    url: o.link,
                    isExternal: o.isExternal
                }
            }));
            setChartPriceM2(validData.sort((a, b) => (a.price_amount_usd / a.m2 > b.price_amount_usd / b.m2) ? 1 : -1).map(o => {
                return {
                    id: o.id,
                    value: Math.round(o.price_amount_usd / o.m2),
                    url: o.link,
                    isExternal: o.isExternal
                }
            }));
        }
    }, [validData]);

    return {
        loading,
        error,
        chartPrice,
        chartPriceM2
    }

};
