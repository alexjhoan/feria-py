import "./styles.less";

import { Carousel, Typography } from "antd";
import {
	PropertyCard,
	PropertyCardSkeleton,
} from "../../../../Components/Property/PropertyCard/web";
import React, { ReactNode, useEffect, useState } from "react";

import { CarouselArrow } from "../../../../Components/Property/LazyImageGallery/CarouselArrow/web";
import { LastSearchCard } from "../../../../Components/SEO/web";
import { PropComponentMode } from "../../../../Components/Property/PropertyInterfaces";
import { usePropertySimilarsCards } from "./hook";
import { useTheme } from "../../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

export function PropertySimilarsCards({
	id,
	mode,
	grid = { xs: 1, sm: 1, md: 2, lg: 3, xl: 3, xxl: 3 },
	title = null,
	onPropertyClick,
}: {
	id: string;
	mode: PropComponentMode;
	grid?: { xs: number; sm: number; md: number; lg: number; xl: number; xxl: number };
	title?: ReactNode;
	onPropertyClick?: () => void;
}) {
	const { data, loading } = usePropertySimilarsCards({ property_id: id, mode });
	const { t } = useTranslation();
	const { theme } = useTheme();
	const [settings, setSettings] = useState({
		dots: false,
		arrows: true,
		nextArrow: <CarouselArrow type={"filled"} position={"outside"} side={"right"} />,
		prevArrow: <CarouselArrow type={"filled"} position={"outside"} side={"left"} />,
		infinite: false,
		speed: 400,
		autoplaySpeed: 5500,
		autoplay: false,
		slidesToScroll: grid.xxl,
		slidesToShow: grid.xxl,
		render: true,
		responsive: [],
		variableWidth: true,
	});

	useEffect(() => {
		setSettings({
			...settings,
			render: false,
			responsive: [
				{
					breakpoint: getBreakpointNumber(theme.breakPoints.xl),
					settings: { slidesToShow: grid.xl, slidesToScroll: grid.xl },
				},
				{
					breakpoint: getBreakpointNumber(theme.breakPoints.lg),
					settings: { slidesToShow: grid.lg, slidesToScroll: grid.lg },
				},
				{
					breakpoint: getBreakpointNumber(theme.breakPoints.md),
					settings: { slidesToShow: grid.md, slidesToScroll: grid.md },
				},
				{
					breakpoint: getBreakpointNumber(theme.breakPoints.sm),
					settings: { slidesToShow: grid.sm, slidesToScroll: grid.sm },
				},
				{
					breakpoint: getBreakpointNumber(theme.breakPoints.xs),
					settings: { slidesToShow: grid.xs, slidesToScroll: grid.xs },
				},
			],
		});
	}, []);

	useEffect(() => {
		if (!settings.render) setSettings({ ...settings, render: true });
	}, [settings.render]);

	if (!loading && !data) return null;
	if (!settings.render) return null;

	const similars = loading ? [...Array(4)] : data;

	return (
		<>
			{title ? (
				title
			) : (
				<Typography.Title level={4}>{t("Propiedades Similares")}</Typography.Title>
			)}
			<Carousel {...settings} className="similar-carousel">
				{similars.map((o, i) => {
					return (
						<div key={"similard_card_" + i} className={"similar"}>
							{loading ? (
								<PropertyCardSkeleton />
							) : (
								<PropertyCard
									id={o.id}
									hideActions
									staticGallery
									onPropertyClick={onPropertyClick}
								/>
							)}
						</div>
					);
				})}
				{!loading && (
					<div key={"similard_card_" + data.length + 1} className="similar">
						<LastSearchCard />
					</div>
				)}
			</Carousel>
			<style jsx global>{`
				.similar-carousel .similar {
					padding: ${theme.spacing.xsSpacing}px ${theme.spacing.smSpacing}px;
					margin: 8px -${theme.spacing.smSpacing - 4}px;
				}
			`}</style>
		</>
	);
}

const getBreakpointNumber = bp => {
	return Number(bp.substring(0, bp.length - 2));
};
