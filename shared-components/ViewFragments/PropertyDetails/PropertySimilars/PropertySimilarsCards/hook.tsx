import { usePropertySimilars } from "../hook";
import { PropComponentMode } from "../../../../Components/Property/PropertyInterfaces";

interface usePropertySimilarsCardsProps {
	property_id: string;
	mode: PropComponentMode;
}

export const usePropertySimilarsCards = ({
	property_id,
	mode = "property",
}: usePropertySimilarsCardsProps) => {
	const { data, loading, error } = usePropertySimilars({ property_id, mode });
	const similarProperties = data?.filter(e => e.id != property_id);

	return {
		data: similarProperties,
		loading,
		error,
	};
};
