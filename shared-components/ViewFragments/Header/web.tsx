import "./styles.less";

import { Button, Col, Drawer, Layout, Row } from "antd";
import React, { useState } from "react";
import { SeoHeaderDesktop, SeoHeaderMobile } from "./SeoHeader/web";

import { Brand } from "../../Components/Logo/web";
import { CloseCircleFilled } from "@ant-design/icons";
import LogRocket from "logrocket";
import { MenuOutlined } from "@ant-design/icons";
import { Navigation } from "../PropertyDetails/Navigation/web";
import { SearchBar } from "./SearchBar/web";
import { UserBox } from "./UserBox/web";
import getConfig from "next/config";
import { useAuthCheck } from "../../Components/User/useAuthCheck";
import useBreakpoint from "antd/lib/grid/hooks/useBreakpoint";
import { useTheme } from "../../Styles/ThemeHook";
import { useUser } from "../../Components/User/User.hook";

const { logrocketID } = getConfig().publicRuntimeConfig;

const { Header: HeaderComponent } = Layout;

export const Header = ({ showSearchBar = true }) => {
	const screen = useBreakpoint();

	const { theme } = useTheme();

	const {
		user: { data, loading },
		isLoggedIn,
	} = useUser();

	if (isLoggedIn && data && logrocketID) {
		LogRocket.identify(data.me.id, {
			name: data.me.name,
			email: data.me.email,
			individual: data.me.individual,
		});
	}

	const [mobileMenuVisible, setMobileMenuVisible] = useState(false);

	return (
		<>
			<div className="ghost-header"></div>
			<div className="header_container">
				<HeaderComponent className="header">
					<Row
						style={{ height: 64 }}
						align={"middle"}
						gutter={theme.spacing.lgSpacing}
						justify={"space-between"}>
						{showSearchBar && !screen.lg && (
							<Col flex={screen.xs && "1"}>
								<Navigation type={"none"} />
							</Col>
						)}
						<Col flex={screen.lg ? "150px" : "1 1 auto"} style={{ maxWidth: 150 }}>
							<Brand type="logo" link />
						</Col>
						{showSearchBar && ((screen.sm && !screen.lg) || screen.xl) && (
							<Col
								lg={8}
								xl={6}
								flex={screen.lg ? "1 1 260px" : "2 1 0"}
								className="header-search-100">
								<SearchBar keywordLocationClassName="search-results-keyword-location" />
							</Col>
						)}
						<Col xs={0} lg={16} flex={1}>
							<SeoHeaderDesktop />
						</Col>
						<Col xs={0} lg={8} flex={1}>
							<UserBox />
						</Col>
						<Col
							lg={0}
							flex={screen.xs ? "1" : "0"}
							style={screen.xs && { display: "flex", justifyContent: "flex-end" }}>
							<Button
								className="btn-menu-mobile"
								type={"text"}
								icon={<MenuOutlined />}
								onClick={() => setMobileMenuVisible(true)}
							/>
						</Col>
					</Row>
				</HeaderComponent>
			</div>

			<MenuMobile
				visible={mobileMenuVisible && !screen.lg}
				onClose={() => setMobileMenuVisible(false)}
			/>

			<style jsx global>{`
				.ghost-header {
					height: ${theme.headerHeight}px;
				}
				.header_container .header .btn-menu-mobile {
					color: ${theme.colors.primaryColor};
				}

				@media screen and (max-width: ${theme.breakPoints.xs}) {
					.header-search-100:hover,
					.header-search-100:active {
						background: ${theme.colors.backgroundColor};
					}
				}
			`}</style>
		</>
	);
};

const MenuMobile = ({ visible, onClose }) => {
	const { theme } = useTheme();
	const { isLoggedIn } = useAuthCheck();

	return (
		<>
			<Drawer
				width={320}
				className="header-menu-mobile"
				placement="right"
				closable={true}
				visible={visible}
				bodyStyle={{ backgroundColor: theme.colors.backgroundMenuColor }}
				onClose={() => onClose()}
				closeIcon={<CloseCircleFilled />}>
				<UserBox />
				<SeoHeaderMobile />
			</Drawer>
			<style jsx global>{`
				.header-menu-mobile
					.ant-drawer-content
					.ant-drawer-wrapper-body
					.ant-drawer-header-no-title
					.ant-drawer-close {
					color: ${theme.colors.backgroundColor};
					background: ${theme.colors.textColor};
					padding: ${theme.spacing.lgSpacing + 3}px;
				}

				.header-menu-mobile
					.ant-drawer-content
					.ant-drawer-wrapper-body
					.ant-drawer-header-no-title
					.ant-drawer-close
					.anticon-close-circle {
					margin-right: ${theme.spacing.smSpacing}px;
				}
			`}</style>
		</>
	);
};

export default Header;
