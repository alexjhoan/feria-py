import { useContext } from "react";
import { ConfigStateContext } from "../../../Contexts/Configurations/context";
import { QUERY_DESTACADO_HOME } from "../../Home/BannerHome/BannerHome.hook";
import { removeUrlProtocol } from "../../../Utils/Functions";
import { useQuery } from "@apollo/client";

const parseLink = primaryLink => {
	const { data } = useQuery(QUERY_DESTACADO_HOME, {
		variables: primaryLink.banner_params,
		fetchPolicy: "cache-first"
	});

	const { image, name, image_vertical, url } = data?.featuredBannerHome ?? {};
	const banner = {
		img: image_vertical,
		link: removeUrlProtocol(url ?? ""),
		text: name,
		subtitle: name,
		title: name,
	};

	const links = primaryLink.links.map(link => {
		let categoryNames = [];

		link.links.forEach(({ category }) => {
			if (!categoryNames.includes(category)) {
				categoryNames.push(category);
			}
		});

		const categories = categoryNames.map(categoryName => {
			const linksCategory = link.links.filter(
				({ category }) => category === categoryName
			);

			return {
				title: categoryName,
				links: linksCategory,
			};
		});

		return {
			...link,
			categories,
		};
	});

	return {
		...primaryLink,
		banner,
		links,
	};
};

export const useSeoHeader = () => {
	const { header_links } = useContext(ConfigStateContext);

	const headerLinks = header_links?.map(parseLink) ?? [];

	return {
		headerLinks,
	};
};
