import "./styles.less";

import { Col, Row, Space, Tag, Typography } from "antd";

import Link from "next/link";
import { useTheme } from "../../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Paragraph } = Typography;

export const SeoHeaderBanner = ({ banner }) => {
	const { t } = useTranslation();
	const { theme } = useTheme();

	return (
		<>
			<Link href={banner.link}>
				<a className="banner_seoheader">
					<Row gutter={0} align="bottom" className={"row-container"}>
						<Col span={8} lg={0}>
							<div className={"banner-image"} />
						</Col>
						<Col span={16} lg={24}>
							<Space direction="vertical">
								<Paragraph
									style={{ fontSize: 18, textTransform: "capitalize" }}
									strong>
									{banner.title}
								</Paragraph>

								<Paragraph
									style={{ fontSize: 12, textTransform: "uppercase" }}
									type={"secondary"}>
									{banner.subtitle}
								</Paragraph>

								<Paragraph>{banner.text}</Paragraph>
							</Space>
						</Col>
						<Col
							span={0}
							lg={24}
							style={{
								textAlign: "center",
							}}>
							<Tag color="processing" style={{ margin: 0 }}>
								{t("Auspiciado")}
							</Tag>
						</Col>
					</Row>
				</a>
			</Link>
			<style jsx global>{`
				.banner_seoheader {
					background-image: url(${banner.img});
				}

				.banner_seoheader .row-container {
					padding: ${theme.spacing.lgSpacing}px 0;
				}

				.banner_seoheader .row-container .ant-tag {
					font-size: ${theme.spacing.smSpacing + 2}px;
					color: ${theme.colors.backgroundColor};
					border-color: ${theme.colors.backgroundColor};
				}

				@media screen and (min-width: ${theme.breakPoints.lg}) {
					.banner_seoheader .ant-typography {
						color: ${theme.colors.backgroundColor} !important;
					}
				}

				@media screen and (max-width: ${theme.breakPoints.lg}) {
					.banner_seoheader {
						padding: ${theme.spacing.smSpacing}px;
						border-radius: ${theme.spacing.xsSpacing}px;
					}
					.banner_seoheader .banner-image {
						background-image: url(${banner.img});
						border-radius: ${theme.spacing.xsSpacing}px;
					}
				}
			`}</style>
		</>
	);
};
