import "./styles.less";

import { Badge, Button, Col, Menu, Row } from "antd";
import { BellOutlined, UserOutlined } from "@ant-design/icons";

import { Logout } from "../../../Components/User/LogOut/web";
import { NotificationList } from "../../../Components/User/NotificationList/web";
import React, {useContext} from "react";
import { openAuthModal } from "../../../Components/User/AuthModal/web";
import { useAuthCheck } from "../../../Components/User/useAuthCheck";
import useBreakpoint from "antd/lib/grid/hooks/useBreakpoint";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";
import { useUser } from "../../../Components/User/User.hook";
import { ConfigStateContext } from "../../../Contexts/Configurations/context";

export const UserBox = () => {
	const { isLoggedIn } = useAuthCheck();
	const { theme } = useTheme();
	const { user } = useUser();
	const screen = useBreakpoint();
	const { t } = useTranslation();
	const {main_domain} = useContext(ConfigStateContext)

	const DATA_PUBILCATE_PROPERTY_DROPDOWN = [
		{
			title: "Inmobiliaria",
			url: "/soyinmobiliaria",
		},
		{
			title: "Dueño Vende",
			url: "/publicar",
		},
		{
			title: "Dueño Alquila Anual",
			url: "/publicar",
		},
		{
			title: "Dueño Alquila Temporario",
			url: "/publicar-alquiler-temporario",
		},
	];

	const DATA_USER_DROPDOWN = {
		IS_LOGGED_INDIVIDUAL: [
			{
				title: "Perfil",
				url: "/dashboard",
			},
			{
				title: "Favoritos",
				url: "/favoritos",
			},
		],

		NOT_IS_LOGGED_INDIVIDUAL: [
			{
				title: "Panel Inmobiliario",
				url: "/sitio/index.php?mid=inmobiliarias&func=panel",
			},
			{
				title: "Alquileres Temporales",
				url: "/sitio/index.php?mid=inmobiliarias&func=panelPropiedades",
			},
			{
				title: "Administrador",
				url: "/sitio/index.php?mid=superadmin&func=vendedores",
			},
			{
				title: "Panel de Pagos",
				url: "/sitio/index.php?mid=pagos",
			},
			{
				title: "Tarjetas de Crédito",
				url: "/sitio/index.php?mid=pagos&func=view_administrar_tarjeta",
			},
		],
	};

	const data = user?.data?.me?.individual
		? DATA_USER_DROPDOWN.IS_LOGGED_INDIVIDUAL
		: DATA_USER_DROPDOWN.NOT_IS_LOGGED_INDIVIDUAL;

	const PublicatePropertyButton = (
		<Menu.Item
			key={"key_property_button" + (screen.lg ? "_desktop" : "_mobile")}
			className="li-userbox-property">
			<Button
				className="btn-userbox-property"
				type="text"
				href={'https://'+main_domain+"/sitio/index.php?mid=propiedades&func=add"}>
				{t("Publicar propiedad")}
			</Button>
		</Menu.Item>
	);

	const LoginUserButton = (
		<Menu.Item
			key={"key_login_button" + (screen.lg ? "_desktop" : "_mobile")}
			className="li-userbox-login">
			<Button
				className="btn-userbox-login"
				type="link"
				onClick={() => openAuthModal({ defaultModalType: "LogIn" })}>
				{t("Ingresar")}
			</Button>
		</Menu.Item>
	);

	const dataPP = user?.data?.me?.individual
		? DATA_PUBILCATE_PROPERTY_DROPDOWN.slice(1, DATA_PUBILCATE_PROPERTY_DROPDOWN.length)
		: DATA_PUBILCATE_PROPERTY_DROPDOWN;

	const PublicatePropertyDropdown = (
		<Menu.SubMenu
			popupOffset={[0, 12]}
			popupClassName="userbox-dropdown-popup"
			className="btn-userbox-property"
			title={t("Publicar propiedad")}>
			{dataPP.map((e, i) => (
				<Menu.Item key={"key_favorite" + i + (screen.lg ? "_desktop" : "_mobile")}>
					<a href={'https://'+main_domain+e.url}>{t(e.title)}</a>
				</Menu.Item>
			))}
		</Menu.SubMenu>
	);

	return (
		<>
			<Menu
				mode={screen.lg ? "horizontal" : "inline"}
				className={screen.lg ? "userbox-desktop" : "userbox-mobile"}>
				{/* Publicar Propiedad */}
				{isLoggedIn
					? user?.data?.me?.individual
						? PublicatePropertyDropdown
						: PublicatePropertyButton
					: PublicatePropertyDropdown}

				{/* Login */}
				{isLoggedIn ? (
					<Menu.SubMenu
						popupOffset={[0, 12]}
						popupClassName="userbox-dropdown-popup"
						className="btn-userbox"
						icon={<UserOutlined />}
						title={user?.data?.me.name}>
						{data.map((e, i) => (
							<Menu.Item key={"key_user_data " + i}>
								<a href={'https://'+main_domain+e.url}>{t(e.title)}</a>
							</Menu.Item>
						))}
						<Menu.Item key={"key_logout" + (screen.lg ? "_desktop" : "_mobile")}>
							<Logout />
						</Menu.Item>
					</Menu.SubMenu>
				) : (
					LoginUserButton
				)}

				{/* Notificaciones */}
				{isLoggedIn && (
					<Menu.SubMenu
						popupOffset={[-14, 0]}
						popupClassName="notificaction-dropdown-popup"
						className="btn-notifications secondary"
						icon={
							<Row gutter={{ xs: theme.spacing.mdSpacing }}>
								<Col>
									<Badge
										count={user?.data?.me.unread_notifications}
										overflowCount={9}
										size={"small"}>
										<BellOutlined />
									</Badge>
								</Col>
								<Col lg={0}>{t("Notificaciones")}</Col>
							</Row>
						}>
						<div className="notifications-panel">
							<NotificationList />
						</div>
					</Menu.SubMenu>
				)}
			</Menu>
			<style jsx global>{`
				.userbox-desktop .btn-userbox-property .ant-menu-submenu-title {
					border-radius: ${theme.spacing.smSpacing}px;
					padding: ${theme.spacing.smSpacing}px ${theme.spacing.lgSpacing}px !important;
					margin: 0 ${theme.spacing.smSpacing}px 0 0 !important;
					color: ${theme.colors.textColor} !important;
					background-color: ${theme.colors.backgroundColor};
				}

				.userbox-desktop .li-userbox-property .btn-userbox-property {
					color: ${theme.colors.textColor};
					background-color: ${theme.colors.backgroundColor};
				}

				.userbox-desktop .li-userbox-property .btn-userbox-property:hover {
					color: ${theme.colors.backgroundColor};
					background-color: ${theme.colors.linkHoverColor};
				}

				.userbox-desktop .btn-userbox-property .ant-menu-submenu-title:hover {
					color: ${theme.colors.backgroundColor} !important;
					background-color: ${theme.colors.linkHoverColor} !important;
				}

				.userbox-desktop .btn-userbox-login {
					border-radius: ${theme.spacing.smSpacing}px;
					border-color: ${theme.colors.borderColor};
					background: ${theme.colors.backgroundColor};
					color: ${theme.colors.textColor};
				}

				.userbox-desktop .btn-userbox-login:hover {
					color: ${theme.colors.textColor} !important;
					background-color: ${theme.colors.borderColor};
					border-color: ${theme.colors.borderColor};
				}

				.userbox-desktop .btn-userbox-login:active {
					color: ${theme.colors.backgroundColor} !important;
					background-color: ${theme.colors.textSecondaryColor};
				}

				.userbox-desktop .btn-userbox .ant-menu-submenu-title {
					border-radius: ${theme.spacing.smSpacing}px;
					padding: ${theme.spacing.smSpacing}px ${theme.spacing.lgSpacing}px;
					color: ${theme.colors.textColor};
					background-color: ${theme.colors.backgroundColor};
				}

				.userbox-desktop .btn-userbox .ant-menu-submenu-title:hover {
					color: ${theme.colors.backgroundColor};
					background-color: ${theme.colors.linkHoverColor};
				}

				.userbox-desktop .btn-notifications {
					margin: 0 ${theme.spacing.mdSpacing}px !important;
				}

				.btn-notifications p.ant-scroll-number-only-unit.current {
					font-size: ${theme.spacing.mdSpacing}px;
				}

				.btn-notifications .ant-badge .ant-badge-count {
					font-size: ${theme.spacing.smSpacing + 2}px;
					padding: 0 ${theme.spacing.xsSpacing}px;
					border-radius: ${theme.spacing.lgSpacing}px;
				}

				.userbox-dropdown-popup .ant-menu-sub .ant-menu-item-selected {
					color: ${theme.colors.linkColor};
				}

				.userbox-dropdown-popup .ant-menu-sub .ant-menu-item-selected a {
					color: ${theme.colors.linkColor};
				}

				.userbox-dropdown-popup
					.ant-menu-sub:not(.ant-menu-horizontal)
					.ant-menu-item-selected {
					background-color: ${theme.colors.secondaryOpacityColor};
				}

				.userbox-dropdown-popup .ant-menu-sub .ant-menu-item:hover,
				.userbox-dropdown-popup .ant-menu-sub .ant-menu-item:hover a {
					color: ${theme.colors.linkColor};
				}

				.userbox-dropdown-popup .ant-menu-sub .ant-menu-item:hover {
					background-color: ${theme.colors.secondaryOpacityColor};
				}

				.userbox-dropdown-popup .ant-menu-sub .ant-menu-item:active {
					background: ${theme.colors.secondaryOpacityColor};
				}

				.userbox-mobile {
					color: ${theme.colors.backgroundColor};
				}

				.userbox-mobile:not(.ant-menu-horizontal) .ant-menu-item-selected {
					color: ${theme.colors.backgroundColor};
				}

				.userbox-mobile .ant-menu-submenu .ant-menu {
					padding: 0 0 0 ${theme.spacing.mdSpacing}px;
				}

				.userbox-mobile .ant-menu-submenu .ant-menu-submenu-title {
					padding: 0 ${theme.spacing.lgSpacing}px !important;
					padding-right: ${theme.spacing.xxlSpacing + 8}px !important;
					border-radius: ${theme.spacing.smSpacing}px;
				}

				.userbox-mobile .ant-menu-submenu .ant-menu-submenu-title:hover,
				.userbox-mobile .ant-menu-submenu .ant-menu-submenu-title:active {
					background: ${theme.colors.backgroundColor};
					border-radius: ${theme.spacing.smSpacing}px;
					color: ${theme.colors.textColor};
				}

				.userbox-mobile
					.ant-menu-submenu
					.ant-menu-submenu-title:hover
					.ant-menu-submenu-arrow::before,
				.userbox-mobile
					.ant-menu-submenu
					.ant-menu-submenu-title:hover
					.ant-menu-submenu-arrow::after,
				.userbox-mobile
					.ant-menu-submenu
					.ant-menu-submenu-title:active
					.ant-menu-submenu-arrow::before,
				.userbox-mobile
					.ant-menu-submenu
					.ant-menu-submenu-title:active
					.ant-menu-submenu-arrow::after {
					background: ${theme.colors.backgroundMenuColor};
				}

				.userbox-mobile
					.ant-menu-submenu
					.ant-menu-submenu-title
					.ant-menu-submenu-arrow::before,
				.userbox-mobile
					.ant-menu-submenu
					.ant-menu-submenu-title
					.ant-menu-submenu-arrow::after {
					background: ${theme.colors.backgroundColor};
				}

				.userbox-mobile .ant-menu-submenu-selected {
					color: ${theme.colors.backgroundColor};
					border-radius: ${theme.spacing.smSpacing}px;
				}

				.userbox-mobile
					.ant-menu-submenu-selected
					.ant-menu-submenu-title
					.ant-menu-submenu-arrow::before,
				.userbox-mobile
					.ant-menu-submenu-selected
					.ant-menu-submenu-title
					.ant-menu-submenu-arrow::after {
					background: ${theme.colors.backgroundMenuColor};
				}

				.userbox-mobile .ant-menu-item-selected {
					color: ${theme.colors.textColor};
				}

				.userbox-mobile .ant-menu-item {
					padding: 0 ${theme.spacing.lgSpacing}px !important;
					border-radius: ${theme.spacing.smSpacing}px;
					color: ${theme.colors.backgroundColor};
				}

				.userbox-mobile .ant-menu-item a {
					color: ${theme.colors.backgroundColor};
				}

				.userbox-mobile .ant-menu-item:hover {
					background: ${theme.colors.backgroundColor};
					color: ${theme.colors.textColor} !important;
				}

				.userbox-mobile .ant-menu-item:hover a {
					color: ${theme.colors.textColor};
				}

				.userbox-mobile .ant-menu-item:hover .btn-userbox-login,
				.userbox-mobile .ant-menu-item:hover .btn-userbox-property .ant-menu-submenu-title {
					color: ${theme.colors.textColor};
				}

				.userbox-mobile .ant-menu-item:active {
					background: ${theme.colors.backgroundColor};
					color: ${theme.colors.textColor};
				}

				.userbox-mobile .ant-menu-item:active a {
					color: ${theme.colors.textColor};
				}

				.userbox-mobile .ant-menu-item:hover,
				.userbox-mobile .ant-menu-item-active,
				.userbox-mobile .ant-menu:not(.ant-menu-inline) .ant-menu-submenu-open,
				.userbox-mobile .ant-menu-submenu-active,
				.userbox-mobile .ant-menu-submenu-title:hover {
					color: ${theme.colors.backgroundColor};
				}

				.userbox-mobile .ant-menu-item:active .btn-userbox-login,
				.userbox-mobile .ant-menu-item:hover .btn-userbox-property .ant-menu-submenu-title {
					color: ${theme.colors.textColor};
				}

				.userbox-mobile .ant-menu-item .btn-userbox-login,
				.userbox-mobile .ant-menu-item .btn-userbox-property .ant-menu-submenu-title {
					color: ${theme.colors.backgroundColor};
				}

				.userbox-desktop .ant-menu-submenu.btn-notifications {
					margin: 0 ${theme.spacing.smSpacing}px;
				}

				.userbox-desktop .secondary:hover,
				.userbox-desktop .ant-menu-submenu-open.secondary,
				.userbox-desktop .ant-menu-submenu-active.secondary,
				.userbox-desktop .secondary .ant-menu-submenu-title:hover {
					color: ${theme.colors.secondaryHoverColor};
				}
			`}</style>
		</>
	);
};
