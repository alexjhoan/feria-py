import "./styles.less";

import React from "react";
import { Tag } from "antd";
import { useActiveFiltersTags } from "./ActiveFiltersTags.hook";
import { useTheme } from "../../../../Styles/ThemeHook";

const ActiveFiltersTags = () => {
	const { theme } = useTheme();
	const { filtersTagsToShow, changeFilters } = useActiveFiltersTags();

	return (
		<>
			{filtersTagsToShow.map((o, i) => {
				return (
					<Tag
						className="filter-tag"
						key={o.key + "_" + i}
						color={"default"}
						closable={o.deleteable}
						onClose={() => changeFilters(o.onChangeObject)}>
						{o.text}
					</Tag>
				);
			})}
			<style jsx global>{`
				.filter-tag {
					background: ${theme.colors.borderColor};
					padding: 0 ${theme.spacing.smSpacing}px;
					margin-right: ${theme.spacing.xsSpacing}px;
					margin-bottom: ${theme.spacing.xsSpacing}px;
					border: 1px solid ${theme.colors.borderColor};
				}

				.filter-tag .ant-tag-close-icon {
					font-size: ${theme.fontSizes.xsFontSize}px;
					margin-left: ${theme.spacing.xsSpacing}px;
				}
			`}</style>
		</>
	);
};

export { ActiveFiltersTags };
