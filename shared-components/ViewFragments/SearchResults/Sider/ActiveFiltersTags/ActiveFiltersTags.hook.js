import React from "react";
import { useFilters } from "../../../../Components/Filters/Filters.hook";

const FILTERTAGS_TO_SHOW = [
	"operation_type_id",
	"property_type_id",
	"estate_id",
	"neighborhood_id",
	"publicationDate",
	"searchstring",
	"seaview",
	"ownerID",
	"projects",
	"withBase",
	"auctionType",
	"privateOwner",
	"facilitiesGroup",
	"security",
	"socialHousing",
	"constStatesID",
	"floors",
	"dispositionID",
	"condominium",
	"minPrice",
	"maxPrice",
	"m2Min",
	"m2Max",
	"m2Type",
	"commonExpenses",
	"groundUssageID",
	"season",
	"dateFrom",
	"dateTo",
	"guests",
	"bathrooms",
	"bedrooms",
	"rooms",
	"seaDistanceID",
];
const FILTERTAGS_DELETABLES = ["operation_type_id"];

const useActiveFiltersTags = () => {
	const { filtersTags, changeFilters } = useFilters();

	const filtersTagsToShow = filtersTags
		? Object.keys(filtersTags).reduce((acc, o) => {
				if (!FILTERTAGS_TO_SHOW.includes(o)) return acc;
				if (filtersTags[o] == null) return acc;
				if (filtersTags[o] instanceof Array && filtersTags[o].length === 0)
					return acc;
				if (
					!(filtersTags[o] instanceof Array) &&
					typeof filtersTags[o].text === "undefined"
				)
					return acc;

				const deleteable = !FILTERTAGS_DELETABLES.includes(o);

				if (filtersTags[o] instanceof Array) {
					filtersTags[o].map(j => {
						acc.push({
							value: j.value,
							text: j.text,
							key: o,
							onChangeObject: {
								[o]: filtersTags[o].filter(i => i.value !== j.value),
							},
							deleteable,
						});
					});
				} else {
					let text = filtersTags[o].text;

					acc.push({
						value: filtersTags[o].value,
						text: text,
						key: o,
						onChangeObject: { [o]: null },
						deleteable,
					});
				}

				return acc;
		  }, [])
		: [];

	return {
		filtersTagsToShow,
		changeFilters,
	};
};

export { useActiveFiltersTags };
