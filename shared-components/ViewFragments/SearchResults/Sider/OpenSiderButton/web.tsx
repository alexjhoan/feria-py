import { Button, Space, Typography } from "antd";
import { Dispatch, SetStateAction } from "react";

import { SlidersOutlined } from "@ant-design/icons";
import { useOpenSiderButton } from "./hook";
import { useTheme } from "../../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

interface OpenSiderButtonProps {
	setSider: Dispatch<SetStateAction<boolean>>;
}

export const OpenSiderButton = ({ setSider }: OpenSiderButtonProps) => {
	const { theme } = useTheme();
	const { count } = useOpenSiderButton();
	const { t } = useTranslation();
	return (
		<Button onClick={() => setSider(false)}>
			<Space>
				<SlidersOutlined
					style={{
						color: theme.colors.primaryColor,
					}}
				/>
				<Typography.Text>{t("Filtrar")}</Typography.Text>
			</Space>
		</Button>
	);
};
