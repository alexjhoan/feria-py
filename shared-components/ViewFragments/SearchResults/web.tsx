import "./styles.less";

import { Affix, Col, Divider, Layout, Row, Space } from "antd";
import { BreadCrumbs, H1 } from "../../Components/SEO/web";
import React, { useRef } from "react";
import { useEffect, useState } from "react";

import { ActiveFiltersTags } from "./Sider/ActiveFiltersTags/web";
import { ButtonMapToggle } from "./Content/MapButton/web";
import { CloseSiderButton } from "./Sider/CloseSiderButton/web";
import { CreateSearchAlert } from "../../Components/SearchAlert/CreateSearchAlert/web";
import { Filters } from "./Sider/Filters/web";
import { Footer } from "../Footer/web";
import { FullScreen } from "../../Components/FullScreen/web";
import { Header } from "../Header/web";
import { OpenSiderButton } from "./Sider/OpenSiderButton/web";
import { Order } from "../../Components/Filters/Order/web";
import { PicHomeFooter } from "../Footer/PicHomeFooter/web";
import { SearchResultDisplay } from "./Content/SearchResultsCards/SearchResultsPagination/ResultDisplay/web";
import { SearchResultsCards } from "./Content/SearchResultsCards/web";
import { SearchResultsMap } from "./Content/SearchResultsMap/web";
import useBreakpoint from "antd/lib/grid/hooks/useBreakpoint";
import { useTheme } from "../../Styles/ThemeHook";

export const SearchPageLayout = () => {
	const screen = useBreakpoint();
	const { theme } = useTheme();

	const siderWidth = 300;
	const fixedBarHeight = 2 * theme.spacing.mdSpacing + 40; // buttonHeight

	// sider
	const [collapsible, setCollapsible] = useState(false);
	const [sider, setSider] = useState(false);
	const siderRef = useRef(null);
	// end sider

	// lock body scroll on sider open mobile
	useEffect(() => {
		if (collapsible && !sider) {
			siderRef.current.scrollTop = 0;
			document.body.style.overflow = "hidden";
		} else if (!map && collapsible) {
			document.body.style.overflow = "unset";
		}
	}, [sider, collapsible]);
	// end lock body scroll

	// map
	const [map, setMap] = useState(false);
	useEffect(() => {
		if (map && collapsible) {
			document.body.style.overflow = "hidden";
		} else {
			document.body.style.overflow = "unset";
		}
	}, [map]);
	// end map

	// Affixed Mobile Buttons
	const affixedMobileButtons = (
		<Row
			gutter={{
				lg: theme.spacing.lgSpacing,
				sm: theme.spacing.mdSpacing,
				xs: theme.spacing.mdSpacing,
			}}
			style={{ flexWrap: "nowrap" }}>
			{collapsible && (
				<Col>
					<OpenSiderButton setSider={setSider} />
				</Col>
			)}
			<Col>
				<ButtonMapToggle map={map} setMap={setMap} />
			</Col>
			<Col>
				<Order />
			</Col>
		</Row>
	);

	// Sider
	const siderContent = (
		<Row gutter={[0, theme.spacing.lgSpacing]} className={"sider-content"} ref={siderRef}>
			{/* Sider Closer */}
			{collapsible && (
				<Col span={24} className={!sider && "closed-sider-top-fixed"}>
					<CloseSiderButton setSider={setSider} />
				</Col>
			)}

			{/* Tag Active Filters */}
			<Col span={24}>
				<ActiveFiltersTags />
			</Col>

			{/* Alert Button */}
			<Col span={24}>
				<CreateSearchAlert />
			</Col>

			<Divider />

			{/* Filters */}
			<Col span={24}>
				<Filters />
			</Col>

			{/* Sider Closer in Mobile Collapse */}
			{collapsible && (
				<Col span={24} className={!sider && "closed-sider-footer-fixed"}>
					<CloseSiderButton setSider={setSider} showSearchResults />
				</Col>
			)}
		</Row>
	);

	// Content
	const contentContent = (
		<Row
			gutter={[
				0,
				{
					xs: theme.spacing.lgSpacing,
					sm: theme.spacing.lgSpacing,
					lg: theme.spacing.xxlSpacing,
				},
			]}>
			<Col span={24} style={{ paddingTop: 16 }}>
				<Row
					justify={"space-between"}
					gutter={theme.spacing.smSpacing}
					style={{ flexWrap: "nowrap" }}>
					{!(map && collapsible) && (
						<Col flex={1} className="search-result-text-info">
							<Space size={0} direction={"vertical"} style={{ width: "100%" }}>
								{/* H1 */}
								<H1 />

								{/* BreadCrumbs */}
								{!screen.xs && <BreadCrumbs />}

								{/* Search Result Display */}
								{!map && <SearchResultDisplay />}
							</Space>
						</Col>
					)}
					{collapsible ? (
						<div className="search-results-affix-bar">{affixedMobileButtons}</div>
					) : (
						<Col>{affixedMobileButtons}</Col>
					)}
				</Row>
			</Col>

			{map && (
				<Col span={24}>
					<FullScreen
						className={"map-content"}
						offset={
							screen.lg
								? theme.headerHeight + 2 * theme.spacing.xxlSpacing + 80
								: theme.headerHeight + fixedBarHeight
						}>
						{/* Search Results Map */}
						<SearchResultsMap />
					</FullScreen>
				</Col>
			)}

			{/* Search Results and Bigdate Search Result */}
			{!map && <SearchResultsCards />}

			{!map && (
				<Col span={24}>
					{/* Footer */}
					<Footer />
				</Col>
			)}
		</Row>
	);

	return (
		<>
			<Layout>
				<Header />
				<Layout className={"container big search-page " + (map ? "map-view" : "list-view")}>
					<Layout.Sider
						className={collapsible && "fixed-sider"}
						width={collapsible ? "100%" : siderWidth + "px"}
						collapsed={sider}
						collapsedWidth={"0px"}
						breakpoint={"lg"}
						onCollapse={e => setSider(e)}
						onBreakpoint={broken => setCollapsible(broken)}
						trigger={null}>
						{collapsible ? (
							<FullScreen
								offset={theme.headerHeight}
								className="sider-container-fullscreen">
								{siderContent}
							</FullScreen>
						) : (
							siderContent
						)}
					</Layout.Sider>
					<Layout.Content>
						{map ? (
							<Affix offsetTop={theme.headerHeight + theme.spacing.xxlSpacing}>
								{contentContent}
							</Affix>
						) : (
							contentContent
						)}
					</Layout.Content>
				</Layout>
				{!map && <PicHomeFooter />}
			</Layout>
			<style jsx global>{`
				.header_container .header {
					border-bottom: 1px solid ${theme.colors.borderColor};
					padding: 0 ${theme.spacing.mdSpacing}px;
				}
				.map-content {
					border-radius: ${theme.spacing.mdSpacing}px;
				}
				.search-page {
					margin: ${collapsible
							? theme.spacing.mdSpacing + theme.headerHeight
							: theme.spacing.xxlSpacing}px
						auto;
					justify-content: ${collapsible ? "center" : "space-between"};
				}
				.search-page .ant-layout-sider .sider-content {
					border: 1px solid ${theme.colors.borderColor};
					border-radius: ${theme.spacing.smSpacing}px;
					background: ${collapsible ? "transparent" : theme.colors.backgroundColor};
					padding: ${collapsible ? theme.spacing.lgSpacing : theme.spacing.smSpacing}px;
				}
				.search-page .ant-layout-content {
					padding: 0 ${theme.spacing.smSpacing}px;
					max-width: ${collapsible
						? "100%"
						: "calc(100% - " + (siderWidth + (theme.spacing.xxlSpacing + 13)) + "px)"};
				}
				.search-page .search-results-affix-bar {
					background-color: ${theme.colors.backgroundColor};
					top: ${theme.headerHeight - 1}px;
					padding: ${theme.spacing.mdSpacing}px;

					border-bottom: ${collapsible
						? "1px solid " + theme.colors.borderColor
						: "none"};
					border-top: ${collapsible ? "1px solid " + theme.colors.borderColor : "none"};
				}
				.search-page .ant-layout-content h1 {
					font-size: ${theme.fontSizes.smTitle};
					line-height: ${theme.fontSizes.smLineHeight};
				}
				.search-page .ant-layout-sider.fixed-sider {
					top: ${theme.headerHeight}px;
					height: ${"calc(100vh - " + theme.headerHeight + "px)"};
					background: ${theme.colors.backgroundColor};
					visibility: ${!sider ? "visible" : "hidden"};
				}
				.search-page .ant-layout-sider.fixed-sider .sider-content {
					padding-bottom: ${theme.spacing.smSpacing * 10}px;
					padding-top: ${theme.spacing.smSpacing * 5}px;
				}
				.search-page .ant-layout-sider.fixed-sider .closed-sider-top-fixed {
					padding: 0px ${theme.spacing.lgSpacing}px !important;
					top: ${theme.headerHeight - 1}px;
					background: ${theme.colors.backgroundColor};
				}
				.search-page .ant-layout-sider.fixed-sider .closed-sider-footer-fixed {
					background: ${theme.colors.backgroundColor};
					padding: ${theme.spacing.lgSpacing}px !important;
				}
			`}</style>
		</>
	);
};
