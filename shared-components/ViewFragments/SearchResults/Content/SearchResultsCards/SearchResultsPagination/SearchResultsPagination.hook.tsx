import { useSearchResultsFilters } from "../../../Sider/Filters/SearchResultsFilters.hook";
import { useSearchResultsCards } from "../SearchResultsCards.hook";
import { useFilters } from "../../../../../Components/Filters/Filters.hook";
import { useTranslation } from "react-i18next";

const RESULTS_PER_PAGE = 20;

export const useSearchResultsPagination = () => {
	const { t } =  useTranslation()
	const { filters } = useFilters();
	const { changeFiltersAndUpdateURL } = useSearchResultsFilters(true);
	const { loading, error, paginatorInfo } = useSearchResultsCards();

	const changePage = newPage => {
		if (newPage !== filters?.page) {
			changeFiltersAndUpdateURL({
				page: { value: newPage, text: t("page") + newPage },
			});
		}
	};

	return {
		loading,
		error,
		data: {
			count: RESULTS_PER_PAGE,
			currentPage: filters?.page || 1,
			lastPage: paginatorInfo?.lastPage,
			total: paginatorInfo?.total,
			perPage: RESULTS_PER_PAGE,
		},
		changePage,
	};
};
