import { Skeleton, Typography } from "antd";

import { formatMoney } from "../../../../../../Utils/Functions";
import { useSearchResultsPagination } from "../SearchResultsPagination.hook";
import { useTheme } from "../../../../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

export const SearchResultDisplay = props => {
	const { theme } = useTheme();
	const activeMarkers = props.activeMarkers;
	const totalMarkers = props.totalMarkers;

	const {
		loading,
		data: { total, currentPage, perPage },
		error,
	} = useSearchResultsPagination();

	const { t } = useTranslation();

	if (loading || props.loading) return <Skeleton title paragraph={false} active />;
	if (error) return null;

	if (totalMarkers == 0) {
		return <Typography.Text>{t("Sin resultados")}</Typography.Text>;
	} else if (totalMarkers == 1) {
		return (
			<Typography.Text>
				{t("Mostrando")} <Typography.Text strong>1</Typography.Text> {t("resultado")}
			</Typography.Text>
		);
	}

	if (activeMarkers) {
		return (
			<Typography.Text>
				{t("Mostrando")} <Typography.Text strong>{activeMarkers}</Typography.Text>
				{" propiedades de"}{" "}
				<Typography.Text strong>
					{totalMarkers > 400 ? t("+400") : totalMarkers}
				</Typography.Text>
			</Typography.Text>
		);
	}

	const from = (currentPage - 1) * perPage + 1;
	const to = currentPage * perPage > total ? total : currentPage * perPage;

	return (
		<Typography.Text
			className={"search-result-display"}
			style={{ fontSize: theme.fontSizes.smFontSize }}>
			{t("Mostrando")}{" "}
			<Typography.Text strong>{formatMoney(from) + " - " + formatMoney(to)}</Typography.Text>{" "}
			de <Typography.Text strong>{total > 400 ? t("más de 400") : total}</Typography.Text>{" "}
			{t("resultados")}
		</Typography.Text>
	);
};
