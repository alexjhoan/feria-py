import "./styles.less";

import { Card, Typography } from "antd";

import { ImagenOptimizada as Img } from "../../../../Components/Image/web";
import { removeUrlProtocol } from "../../../../Utils/Functions";
import { useTheme } from "../../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Meta } = Card;
const { Title, Link, Paragraph } = Typography;

interface NewsSectionProps {
	news: any;
	size?: "default" | "large";
}

export const News = ({ news, size = "default" }: NewsSectionProps) => {
	const { theme } = useTheme();
	const { t } = useTranslation();

	const cardTitle = (
		<Title
			className="newsDescription"
			ellipsis={size == "large" ? { rows: 2 } : { rows: 3 }}
			level={size == "large" ? 1 : 4}>
			{news.title}
		</Title>
	);

	const cardDescription = <Paragraph ellipsis={{ rows: 2 }}>{news.description}</Paragraph>;

	const cardImage = (
		<div className="article-image">
			<Img src={news.image} alt={news.title} />
		</div>
	);

	return (
		<>
			<div className={"article" + (size == "large" ? " postLarge" : " postSmall")}>
				<Link
					className="article-link"
					title={news.title}
					href={removeUrlProtocol(`/blog/${news.slug}`)}>
					<Card
						className={size == "large" ? "newsLarge" : "newsSmall"}
						bordered={false}
						hoverable={false}
						cover={cardImage}>
						<Meta
							title={size == "large" && cardTitle}
							description={size == "large" ? cardDescription : cardTitle}
						/>
					</Card>
				</Link>
			</div>
			<style jsx global>{`
				.article.postSmall {
					border-bottom: 1px solid ${theme.colors.borderColor};
					padding-top: ${theme.spacing.lgSpacing}px;
					padding-bottom: ${theme.spacing.lgSpacing}px;
				}

				.article:hover .newsDescription {
					color: ${theme.colors.primaryColor};
				}
				.article .article-image {
					border-radius: ${theme.spacing.xsSpacing}px;
				}
				.article.postLarge .article-image {
					border-radius: ${theme.spacing.smSpacing}px;
				}
				.newsLarge .article-image {
					margin-bottom: ${theme.spacing.lgSpacing}px;
				}

				.newsSmall .ant-card-body {
					margin-right: ${theme.spacing.lgSpacing}px;
				}

				@media screen and (max-width: ${theme.breakPoints.md}) {
					.article {
						padding: ${theme.spacing.lgSpacing}px;
						margin: 0 -${theme.spacing.lgSpacing}px;
					}

					.newsLarge .newsDescription {
						font-size: ${theme.fontSizes.mdTitle};
						line-height: ${theme.fontSizes.mdLineHeight};
					}

					.newsSmall .article-image {
						margin-bottom: ${theme.spacing.lgSpacing}px;
					}

					.newsSmall .newsDescription {
						font-size: ${theme.spacing.lgSpacing}px;
					}
				}
			`}</style>
		</>
	);
};
