import React from 'react';
import {
  Text,
  Button,
  View,
  Image,
  Dimensions,
  ImageBackground,
} from 'react-native';
import { useBannerHome } from './BannerHome.hook';

export function BannerHome() {
  const {loading, banner} = useBannerHome();
  if (loading || (banner && banner.loading)) return null;

  const bh = 700;

  return (
    <View style={{flex: 1, height: bh}}>
      <ImageBackground
        source={{uri: banner.data.image}}
        style={{
          width: '100%',
          height: bh,
          resizeMode: 'cover',
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}>
        <View
          style={{
            width: '92%',
            height: 80,
            backgroundColor: banner.data.logo_bg_color,
            marginBottom: '4%',
            borderRadius: 6,
            paddingHorizontal: 8,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <Image
            source={{uri: banner.data.logo}}
            style={{width: 140, height: 80, resizeMode: 'contain'}}
          />
          <Button
            title={'Conoce Más'}
            accessibilityLabel={'Conoce más de ' + banner.data.name}
            color={banner.data.logo_text_color}            
            onPress={() => {
              console.log('Click Banner Home');
            }}
          />
        </View>
      </ImageBackground>
    </View>
  );
}
