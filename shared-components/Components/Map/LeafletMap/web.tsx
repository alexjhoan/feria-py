import React, { useState } from "react";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";
import L from "leaflet";
import Measure from "react-measure";
import { useTheme } from "../../../Styles/ThemeHook";
import Head from "next/head";

const MarkerIconPrimary = new L.Icon({
	iconUrl:
		"https://www.trulia.com/images/app-shopping/map-marker-txl3R/MapMarkerHouseIcon_large@2x.png",
	iconSize: [30, 38],
	iconAnchor: [15, 38],
});
const MarkerIconSecondary = new L.Icon({
	iconUrl:
		"https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png",
	shadowUrl: "https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png",
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41],
});
const MarkerIconDefault = new L.Icon({
	iconUrl:
		"https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png",
	shadowUrl: "https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png",
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41],
});

const DotMarker = ({ type = "primary" }) =>
	new L.divIcon({
		className: "",
		html: `<div class='dot_marker dot_marker_${type}'></div>`,
		iconSize: [25, 25],
		iconAnchor: [12, 12],
	});

const PriceMarker = m =>
	new L.divIcon({
		className: "",
		html: `<div class='kJLafD gLfCZo'>${m.id}</div>`,
		iconSize: [30, 42],
		iconAnchor: [15, 42],
	});

export const LeafletMap = ({
	defaultZoom = 18,
	height = null,
	width = null,
	center,
	children = null,
	zIndex = 1,
	options = {},
	markers = [],
}) => {
	const [measure, setMeasure] = useState(null);
	const { theme } = useTheme();

	return (
		<div style={{ zIndex: zIndex, width: "100%", height: "100%" }}>
			<Head>
				<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" />
			</Head>
			<Measure bounds onResize={m => m.entry && setMeasure(m)}>
				{({ measureRef }) => (
					<div ref={measureRef} style={{ width: "100%", height: "100%" }}>
						{measure && measure.entry && measure.entry.height && measure.entry.width && (
							<Map
								{...options}
								center={[center.latitude, center.longitude]}
								zoom={defaultZoom}
								style={{
									height: height ? height : Math.round(measure.entry.height),
									width: width ? width : Math.round(measure.entry.width),
								}}>
								<TileLayer url="https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png" />
								{markers.map((m, i) => {
									let icon = MarkerIconPrimary;
									if (m.icon && m.icon == "primary") {
										icon = DotMarker({ type: m.icon });
									} else if (m.icon && m.icon == "secondary") {
										icon = DotMarker({ type: m.icon });
									}
									//icon = PriceMarker(m);
									return (
										<Marker
											key={"leafletmarker_" + i}
											position={[m.latitude, m.longitude]}
											icon={icon}>
											{m.popup && <Popup>{m.popup.text}</Popup>}
										</Marker>
									);
								})}
							</Map>
						)}
					</div>
				)}
			</Measure>
			<style jsx global>{`
				.dot_marker.dot_marker_primary {
					background-color: ${theme.colors.primaryColor};
				}
				.dot_marker.dot_marker_primary:hover {
					border-color: ${theme.colors.primaryHoverColor};
				}
				.dot_marker.dot_marker_secondary {
					background-color: ${theme.colors.secondaryColor};
				}
				.dot_marker.dot_marker_secondary:hover {
					border-color: ${theme.colors.secondaryOpacityColor};
				}
				.dot_marker:hover {
					box-shadow: rgb(255, 255, 255) 0px 0px 0px 1px inset;
					width: 35px;
					height: 35px;
					border-width: 10px;
					border-style: solid;
					border-image: initial;
				}
				.dot_marker {
					display: inline-block;
					width: 25px;
					height: 25px;
					transform: translate(-50%, -50%);
					cursor: pointer;
					background-clip: padding-box;
					border-width: 3px;
					border-style: solid;
					border-image: initial;
					border-radius: 50%;
					transition: all 0.3s cubic-bezier(0.39, 0.575, 0.565, 1) 0s;
					border-color: rgb(255, 255, 255);
				}

				.gLfCZo[class~="hover"],
				.gLfCZo:hover {
					color: rgb(255, 255, 255);
					background-color: rgb(0, 173, 187);
				}
				.gLfCZo[class~="visited"],
				.gLfCZo:visited {
					color: rgb(255, 255, 255);
					background-color: rgb(134, 144, 153);
				}
				.kJLafD:hover {
					color: inherit;
				}
				.gLfCZo {
					text-transform: uppercase;
					transform: translate(-50%, calc(-100% - 4px));
					color: rgb(255, 255, 255);
					background-color: rgb(5, 34, 134);
					border-width: 1px;
					border-style: solid;
					border-color: rgb(255, 255, 255);
					border-image: initial;
				}
				.kJLafD {
					cursor: pointer;
					position: absolute;
					width: max-content; /*lo tuve q editar*/
					min-width: 30px;
					display: inline-block;
					box-shadow: rgba(59, 65, 68, 0.2) 0px 2px 4px 0px;
					font-family: TruliaSans, system, -apple-system, Roboto, "Segoe UI Bold", Arial,
						sans-serif;
					font-weight: bold;
					color: rgb(59, 65, 68);
					font-size: 12px;
					line-height: 1.33;
					padding: 2px 4px;
					border-radius: 4px;
				}

				.gLfCZo[class~="hover"]::before,
				.gLfCZo:hover::before {
					background-color: rgb(0, 173, 187);
				}
				.gLfCZo[class~="visited"]::before,
				.gLfCZo:visited::before {
					background-color: rgb(134, 144, 153);
				}
				.gLfCZo::before {
					background-color: rgb(5, 34, 134);
				}
				.gLfCZo::before {
					border-bottom: 1px solid rgb(255, 255, 255);
					border-right: 1px solid rgb(255, 255, 255);
				}
				.gLfCZo::before {
					width: 8px;
					height: 8px;
					border-bottom-right-radius: 2px;
					content: " ";
					position: absolute;
					transform: rotate(45deg);
					bottom: -5px;
					left: calc(50% - 5.5px);
					box-shadow: rgba(59, 65, 68, 0.1) 2px 2px 2px 0px;
				}
			`}</style>
		</div>
	);
};
