import React, { useEffect, useRef } from "react";
import { useListingInfoWindow } from "./ListingInfoWindow.hook";
import { PropertyCard, PropertyCardSkeleton } from "../../../Property/PropertyCard/web";
import { Drawer, Grid } from "antd";
import { useTheme } from "../../../../Styles/ThemeHook";
const { useBreakpoint } = Grid;

const K_INFOWINDOW_WIDTH = 250;
const K_INFOWINDOW_MARGIN_SIDES = 100;
const K_INFOWINDOW_MARGIN_TOP = 120;

const ListingInfoWindow = ({ show, listingId, coords, onClose }) => {
	const { theme } = useTheme();
	const screen = useBreakpoint();

	const divRef = useRef(null);

	if (!show || listingId == 0) return null;

	const infoWindowContent = (
		<>
			{false ? (
				<PropertyCardSkeleton />
			) : (
				<PropertyCard key={listingId} id={listingId} hideActions />
			)}
		</>
	);

	if (!screen.lg)
		return (
			<Drawer
				closeIcon={null}
				bodyStyle={{ padding: theme.spacing.smSpacing }}
				onClose={onClose}
				visible={show}
				height={"auto"}
				placement={"bottom"}
				children={infoWindowContent}
				maskStyle={{ backgroundColor: "transparent" }}
				maskClosable
				footer={null}
			/>
		);

	return (
		<>
			<div
				className={"map-infowindow-container"}
				ref={divRef}
				style={{
					width: K_INFOWINDOW_WIDTH,
					position: "absolute",
					zIndex: 5,
					top: K_INFOWINDOW_MARGIN_TOP,
					...coords,
				}}>
				{infoWindowContent}
			</div>
		</>
	);
};

export {
	ListingInfoWindow,
	K_INFOWINDOW_WIDTH,
	K_INFOWINDOW_MARGIN_SIDES,
	K_INFOWINDOW_MARGIN_TOP,
};
