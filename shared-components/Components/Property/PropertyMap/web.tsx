import "./styles.less";

import { Button, Col, Empty, Row, Skeleton, Typography, message } from "antd";
import { K_LISTING_MARKER_SIZE, ListingMarker } from "../../Map/Listing/Marker/web";

import { GoogleMap } from "../../Map/GoogleMap/web";
import { LeafletMapDynamic } from "../../Map/LeafletMap/LeafletMapDynamic/web";
import { PropComponentMode } from "../PropertyInterfaces";
import React from "react";
import { usePropertyMap } from "./hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Text } = Typography;

export function PropertyMap({
	id,
	defaultZoom = 16,
	width,
	height,
	type = "leaflet",
	options = {},
	onClick = false,
	mode = "auto",
}: {
	id: number;
	defaultZoom?: number;
	width: number | string;
	height: number | string;
	type?: "leaflet" | "google";
	options?: object;
	onClick?: CallableFunction | false;
	mode?: PropComponentMode;
}) {
	const { marker, loading, error, loadingSendLead, sendLead } = usePropertyMap({ id, mode });
	const { t } = useTranslation();
	const { theme } = useTheme();
	if (loading) return <Skeleton.Image className={"map-skeleton"} />;
	if (!marker)
		return (
			<Empty
				imageStyle={{ height: 50, opacity: 0.9 }}
				description={
					<Text style={{ color: theme.colors.textInverseColor }}>
						{t("La propiedad no tiene ubicación")}
					</Text>
				}>
				<Button
					loading={loadingSendLead}
					onClick={() =>
						sendLead().then(response => {
							response.success
								? message.success("Consulta enviada")
								: message.error("Error al enviar la consulta");
						})
					}
					ghost>
					{t("Pedir ubicación")}
				</Button>
			</Empty>
		);

	/**
	 * tiene q ser display flex, si no se rompe el child height 100%.
	 */

	if (type == "google") {
		return (
			<div
				style={{
					width: width,
					height: height,
					display: "flex",
					cursor: !!onClick ? "pointer" : "default",
				}}
				onClick={x => !!onClick && onClick(x)}>
				<GoogleMap
					defaultZoom={defaultZoom}
					streetView={false}
					center={marker}
					options={{ marker_size: K_LISTING_MARKER_SIZE, ...options }}>
					<ListingMarker
						key={"marker_" + id}
						id={id}
						lat={marker.latitude}
						lng={marker.longitude}
						text={"X"}
						active={false}
						zoomPrecio={false}
						updateInfoWindowPosition={() => {}}
					/>
				</GoogleMap>
			</div>
		);
	}

	return (
		<div
			style={{
				width: width,
				height: height,
				display: "flex",
				cursor: !!onClick ? "pointer" : "default",
			}}
			onClick={x => !!onClick && onClick(x)}>
			<LeafletMapDynamic
				defaultZoom={defaultZoom}
				center={marker}
				markers={[marker]}
				options={options}
			/>
		</div>
	);
}
