import { useMutation, useQuery } from "@apollo/client";
import { gql } from "@apollo/client";
import { type } from "i18next-intervalplural-postprocessor";
import { useState, useEffect } from "react";
import { useAuthCheck } from "../../User/useAuthCheck";

const HISTORY_QUERY = gql`
	query history {
		me {
			id
			history {
				property_id
			}
		}
	}
`;

const SEEN_MUTATION = gql`
	mutation seen($property_id: ID!) {
		addVisitHistory(property_id: $property_id) {
			id
			history {
				property_id
			}
		}
	}
`;

export function useSeenStatus({ id }) {
	const [result, setResult] = useState(false);
	const { isLoggedIn } = useAuthCheck();
	const { data, loading, error } = useQuery(HISTORY_QUERY, {
		skip: !isLoggedIn,
	});

	useEffect(() => {
		if (data) setResult(data.me.history.some(e => e.property_id == id));
	}, [data]);

	return { loading, result, error };
}

export function useMarkSeen() {
	const [mutation, { data, loading }] = useMutation(SEEN_MUTATION, {
		onError: x => console.error("useMarkSeen no puede marcar como vista la prop", x),
	});

	const markSeen = property_id => {
		return mutation({ variables: { property_id } });
	};

	return { markSeen, loading, data };
}
