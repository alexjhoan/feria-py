import "./styles.less";

import { Skeleton, Typography } from "antd";

import { usePublicationTime } from "./PublicationTime.hook";
import { useTheme } from "../../../Styles/ThemeHook";

export const PublicationTime = ({ id, separator = false }) => {
	const { text, loading, error } = usePublicationTime({ id });
	const { theme } = useTheme();

	if (loading) return <PublicationTimeSkeleton />;
	if (error) return null;

	return (
		<>
			<Typography.Text className="property-publication-time">
				{separator && <Typography.Text className="separator">·</Typography.Text>}
				{text}
			</Typography.Text>
			<style jsx global>{`
				.property-card .property-publication-time {
					font-size: ${theme.fontSizes.xsFontSize};
				}
				.property-publication-time .separator {
					margin: 0 ${theme.spacing.xsSpacing}px;
				}
			`}</style>
		</>
	);
};

export const PublicationTimeSkeleton = () => {
	return <Skeleton paragraph={{ rows: 1, width: 90 }} active title={false} />;
};
