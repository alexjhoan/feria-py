import "./styles.less";

import { BedIcon, SinkIcon, SquareM2Icon } from "../../../Components/CustomIcons/web";
import { Col, Descriptions, Row, Skeleton, Table, Typography } from "antd";
import { DownOutlined, UpOutlined } from "@ant-design/icons";
import React, { useEffect, useState } from "react";

import { ColumnsType } from "antd/lib/table";
import { PageLink } from "../../Link/web";
import { formatMoney } from "../../../Utils/Functions";
import { useBigDataProperties } from "./hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const DescriptionTitle = text => <Typography.Text strong>{text}</Typography.Text>;

export function BigDataProperties({ id, componentTitle = null, scrollable = false }) {
	const { data, loading, error } = useBigDataProperties({ property_id: id });
	const { theme } = useTheme();
	const { t } = useTranslation();
	const [width, setWidth] = useState(undefined);

	useEffect(() => {
		function sizeChange() {
			setWidth(window.innerWidth);
		}

		window.addEventListener("resize", sizeChange);

		sizeChange();

		return () => window.removeEventListener("resize", sizeChange);
	}, []);

	const columns: ColumnsType = [
		{
			title: <Typography.Text strong>{t("Propiedad")}</Typography.Text>,
			dataIndex: "property",
			ellipsis: true,
			width: width < 768 ? "100%" : 300,
			fixed: "left",
			render: property => {
				if (property.id == id) {
					return "Este inmueble";
				} else {
					return (
						<PageLink pathname="/singleProps" as={property.url}>
							{property.title}
						</PageLink>
					);
				}
			},
		},
		{
			title: <BedIcon />,
			dataIndex: "bedrooms",
			responsive: ["md"],
			width: 60,
			render: bedrooms => (
				<Typography.Text strong={bedrooms.id == id}>
					{bedrooms.value > 0 ? `${bedrooms.value}${t("do")}` : `${t("mon")}`}
				</Typography.Text>
			),
		},
		{
			title: <SinkIcon />,
			dataIndex: "bathrooms",
			responsive: ["md"],
			width: 60,
			render: bathrooms => (
				<Typography.Text strong={bathrooms.id == id}>
					{bathrooms.value}
					{t("ba")}
				</Typography.Text>
			),
		},
		{
			title: <SquareM2Icon />,
			dataIndex: "m2",
			responsive: ["md"],
			width: 70,
			render: m2 =>
				m2.value > 0 ? (
					<Typography.Text strong={m2.id == id}>
						{formatMoney(m2.value)}m2
					</Typography.Text>
				) : (
					"-"
				),
		},
		{
			title: <Typography.Text strong>{t("Precio")}</Typography.Text>,
			dataIndex: "price_amount_usd",
			responsive: ["md"],
			width: 120,
			render: price => (
				<Typography.Text strong={price.id == id}>
					U$S {formatMoney(price.value)}
				</Typography.Text>
			),
		},
		{
			title: <Typography.Text strong>{t("Precio por m2")}</Typography.Text>,
			dataIndex: "price_m2",
			responsive: ["md"],
			width: 130,
			ellipsis: true,
			render: price =>
				price.value > 0 && price.value < Infinity ? (
					<Typography.Text strong={price.id == id}>
						U$S {formatMoney(price.value)}
					</Typography.Text>
				) : (
					"-"
				),
		},
		{
			title: <Typography.Text strong>{t("Fecha de publicación")}</Typography.Text>,
			dataIndex: "fromNow",
			responsive: ["md"],
			ellipsis: true,
			width: 175,
			render: date => <Typography.Text strong={date.id == id}>{date.value}</Typography.Text>,
		},
	];

	return (
		<>
			<div className="bigdata-property-table" style={{ padding: "32px 0" }}>
				{componentTitle && (
					<Typography.Title level={4}>{t(`${componentTitle}`)}</Typography.Title>
				)}
				{loading ? (
					<Skeleton active />
				) : (
					<Table
						className="pd-table"
						columns={columns}
						dataSource={data}
						size="small"
						scroll={scrollable ? { y: "100%", x: "100%" } : { x: "100%" }}
						expandable={{
							expandIconColumnIndex: width < 768 ? 7 : -1,
							expandIcon: ({ expanded, onExpand, record }) =>
								expanded ? (
									<div className="tableArrow down">
										<UpOutlined
											className="icon-arrow"
											onClick={e => onExpand(record, e)}
										/>
									</div>
								) : (
									<div className="tableArrow down">
										<DownOutlined
											className="icon-arrow"
											onClick={e => onExpand(record, e)}
										/>
									</div>
								),
							rowExpandable: () => {
								return true;
							},
							expandedRowRender: property => (
								<Descriptions
									layout="vertical"
									size="middle"
									column={2}
									className="expandable-table">
									<Descriptions.Item label={DescriptionTitle("Precio")}>
										{property.price_amount_usd.value}
									</Descriptions.Item>
									<Descriptions.Item label={DescriptionTitle("Precio por m2")}>
										U$S {formatMoney(property.price_m2.value)}
									</Descriptions.Item>
									<Descriptions.Item label={DescriptionTitle("Publicación")}>
										{property.created_at}
									</Descriptions.Item>
									<Descriptions.Item label={DescriptionTitle("Dormitorios")}>
										{property.bedrooms.value}
									</Descriptions.Item>
									<Descriptions.Item label={DescriptionTitle("Baños")}>
										{property.bathrooms.value}
									</Descriptions.Item>
									<Descriptions.Item label={DescriptionTitle("Superficie")}>
										{formatMoney(property.m2.value)} m2
									</Descriptions.Item>
								</Descriptions>
							),
						}}
					/>
				)}
			</div>
			<style jsx global>{`
				.bigdata-property-table .pd-table table .ant-table-tbody > tr > td {
					padding: ${theme.spacing.smSpacing}px !important;
				}
				.bigdata-property-table .pd-table table .ant-table-thead > tr > th {
					padding: ${theme.spacing.mdSpacing}px ${theme.spacing.smSpacing}px !important;
				}
				.bigdata-property-table .tableArrow {
					color: ${theme.colors.textColor};
					background: ${theme.colors.backgroundColor};
					border: ${"1px solid " + theme.colors.borderColor};
				}
			`}</style>
		</>
	);
}
