import {
	FragmentDefiner,
	useReadFragment,
} from "../../../GlobalHooks/useReadFragment";

export const FRAGMENT_PROPERTY_TYPE = new FragmentDefiner(
	"Property",
	`
      project {
          id
      }
      property_type {
        id
        name
      }
  `
);
export function usePropertyType({ id }) {
	let { loading, data, error } = useReadFragment(FRAGMENT_PROPERTY_TYPE, id);
	let text = "";

	if (data) {
		if (data.project?.length > 0) text = "Proyecto";
		else text = data.property_type?.name;
	}

	return {
		loading,
		data: text,
		error,
	};
}
