import "./styles.less";

import { Skeleton, Typography } from "antd";

import { usePropertyType } from "./hook";
import { useTheme } from "../../../Styles/ThemeHook";

export const PropertyTypeTag = ({ id }) => {
	const { data, loading, error } = usePropertyType({ id });
	const { theme } = useTheme();
	if (error) return null;
	if (loading) return <PropertyTypeTagSkeleton />;

	return (
		<>
			<Typography.Text className={"property-type-tag"}>{data}</Typography.Text>
			<style jsx global>{`
				.property-card .property-type-tag {
					font-size: ${theme.fontSizes.xsFontSize};
				}
			`}</style>
		</>
	);
};

export const PropertyTypeTagSkeleton = () => {
	return <Skeleton active={true} title={false} paragraph={{ rows: 1, width: 80 }} />;
};
