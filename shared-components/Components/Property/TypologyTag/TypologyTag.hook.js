import {
	FragmentDefiner,
	useReadFragment,
} from "../../../GlobalHooks/useReadFragment";
import { FRAGMENT_BEDROOMSTAG } from "./BedroomsTag/BedroomsTag.hook";
import { FRAGMENT_M2TAG } from "./M2Tag/M2Tag.hook";
import { FRAGMENT_BATHROOMSTAG } from "./BathroomsTag/BathroomsTag.hook";
import { FRAGMENT_GUESTSTAG } from "./GuestsTag/GuestsTag.hook";
import { FRAGMENT_ROOMSTAG } from "./RoomsTag/RoomsTag.hook";
import {
	showBathrooms,
	showBedrooms,
	showRooms,
	showGuests,
	showM2,
} from "../../../Utils/Functions";

export const FRAGMENT_TYPOLOGYTAG = new FragmentDefiner(
	"Property",
	`
    id
`
).uses(
	FRAGMENT_BEDROOMSTAG,
	FRAGMENT_M2TAG,
	FRAGMENT_BATHROOMSTAG,
	FRAGMENT_GUESTSTAG,
	FRAGMENT_ROOMSTAG
);

//todo::huespedes en temporales

export const FRAGMENT_TYPOLOGY_TAGS = new FragmentDefiner(
	"Property",
	`
    operation_type {
        id
    }
    property_type {
        id
    }
`
);

export function useTypologyTag({ id }) {
	const { loading, data, error } = useReadFragment(FRAGMENT_TYPOLOGY_TAGS, id);

	return {
		loading,
		data: {
			showBedrooms: data ? showBedrooms(data.property_type?.id, data.operation_type?.id): false,
			showBathrooms: data ? showBathrooms(
				data.property_type?.id,
				data.operation_type?.id
			) : false,
			showRooms: data ? showRooms(data.property_type?.id, data.operation_type?.id) : false,
			showGuests: data ? showGuests(data.operation_type?.id) : false,
			showM2: data ? showM2(data.operation_type?.id) : false,
		},
		error,
	};
}
