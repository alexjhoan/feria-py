import {FragmentDefiner, useReadFragment} from "../../../../GlobalHooks/useReadFragment";
import { useState, useEffect } from "react";
import { formatMoney } from "../../../../Utils/Functions";


export const FRAGMENT_M2TAG = new FragmentDefiner("Property",`
    m2
    project {
        id
        minM2
        link
    }
    hectares 
    operation_type {
        id
    }
`);

export function useM2Tag({id, mode = "auto"}) {
    const {loading, data, error} = useReadFragment(FRAGMENT_M2TAG, id);
    const [show, setShow] = useState(false);
    const [text, setText] = useState("");

    useEffect(() => {
        if(data){
            if((mode === "auto" && data.project[0]) || mode === "project"){
                setText("+" + formatMoney(data.project[0].minM2) + " m2");
                setShow(data.project[0].minM2 > 0);
            } else {
                if (data.hectares != null && data.hectares > 0) {
                   setText(formatMoney(data.hectares) +  ' hectareas');
                   setShow(data.hectares > 0);
                } else {
                    setText(formatMoney(data.m2) +  ' m2');
                    setShow(data.m2 > 0);
                }
            } 
        }
    }, [data]);

    return { 
        loading, 
        text, 
        error,
        show
    }
}

