import { useBedroomsTag } from "./BedroomsTag.hook";
import { Space, Typography } from "antd";
import { TagSkeleton } from "../web";
import { useTranslation } from "react-i18next";
import React from "react";
import { BedIcon } from "../../../CustomIcons/web";
import { useTheme } from "../../../../Styles/ThemeHook";

export function BedroomsTag({ id, mode = "auto", icon = false }) {
	const { text, loading, error } = useBedroomsTag({ id, mode });
	const { t } = useTranslation();
	const { theme } = useTheme();
	if (loading) return <TagSkeleton />;

	return (
		<Space size={theme.spacing.smSpacing}>
			{icon && <BedIcon style={{ color: theme.colors.borderColor }} />}
			<Typography.Text ellipsis>{t(text)}</Typography.Text>
		</Space>
	);
}
