import { useBathroomsTag } from "./BathroomsTag.hook";
import { Space, Typography } from "antd";
import { TagSkeleton } from "../web";
import { useTranslation } from "react-i18next";
import React from "react";
import { useTheme } from "../../../../Styles/ThemeHook";
import { SinkIcon } from "../../../CustomIcons/web";

export function BathroomsTag({ id, mode = "auto", icon = false }) {
	const { text, loading, error } = useBathroomsTag({ id, mode });
	const { t } = useTranslation();
	const { theme } = useTheme();

	if (error) return null;
	if (loading) return <TagSkeleton />;

	return (
		<Space size={theme.spacing.smSpacing}>
			{icon && <SinkIcon style={{ color: theme.colors.borderColor }} />}
			<Typography.Text ellipsis>{t(text)}</Typography.Text>
		</Space>
	);
}
