import "./styles.less";

import { Card, Col, Row, Skeleton, Space } from "antd";

import { Description } from "../Description/web";
import { Favorite } from "../Favorite/web";
import { LazyImageGallery } from "../LazyImageGallery/web";
import { LeadButtons } from "../InformationRequest/LeadButtons/web";
import { ListingTypeTag } from "../ListingTypeTag/web";
import { LocationTag } from "../LocationTag/web";
import { OwnerLogo } from "../OwnerLogo/web";
import { PageLink } from "../../Link/web";
import { PriceTag } from "../PriceTag/web";
import { PropComponentMode } from "../PropertyInterfaces";
/* NO BORRAR, ESTAMOS PROBANDO  */
// import { PropertyTypeTag } from "../PropertyTypeTag/web";
// import { PublicationTime } from "../PublicationTime/web";
/* NO BORRAR, ESTAMOS PROBANDO  */
import { SeenIcon } from "../SeenStatus/web";
import { Title } from "../Title/web";
import { TypologyTag } from "../TypologyTag/web";
import { usePropertyCard } from "./PropertyCard.hook";
import { useTheme } from "../../../Styles/ThemeHook";

export type PropertyCardSizes = "default" | "large";
interface PropertyCardProps {
	id: string;
	size?: PropertyCardSizes;
	hideActions?: boolean;
	staticGallery?: boolean;
	mode?: PropComponentMode;
	onPropertyClick?: () => void;
}

export function PropertyCard({
	id,
	size = "default",
	hideActions = false,
	staticGallery = false,
	mode,
	onPropertyClick = null,
}: PropertyCardProps) {
	const { data, loading, error } = usePropertyCard({ id });
	const { theme } = useTheme();
	if (error) return null;
	if (loading) return <PropertyCardSkeleton />;

	const cardExtra = (
		<Row className="property-card-top" justify={"space-between"} align={"top"}>
			<Col flex={1}>
				<ListingTypeTag id={id} />
			</Col>
			<Col>
				<Favorite id={id} type={"text"} fillOpacity={0.5} />
			</Col>
		</Row>
	);

	const cardTop = (
		<>
			{cardExtra}
			<PageLink
				isExternal={data.isExternal}
				avoidRouter={data.avoidRouter ?? false}
				pathname={data.pathname}
				data={data}
				as={data.link}
				callback={onPropertyClick}>
				<LazyImageGallery id={id} staticSlide={staticGallery} />
			</PageLink>
			<SeenIcon id={id} />
		</>
	);

	const cardBottom = (
		<>
			{size == "large" && <OwnerLogo id={id} />}
			<PageLink
				isExternal={data.isExternal}
				avoidRouter={data.avoidRouter ?? false}
				pathname={data.pathname}
				data={data}
				as={data.link}
				callback={onPropertyClick}>
				<Row>
					{/* NO BORRAR, ESTAMOS PROBANDO  */}
					{/* <Col>
						<PropertyTypeTag id={id} />
					</Col>
					{size == "large" && (
						<Col>
							<PublicationTime id={id} separator />
						</Col>
					)} */}
					{/* NO BORRAR, ESTAMOS PROBANDO  */}
					<Col span={24}>
						<PriceTag id={id} />
					</Col>
					<Col span={24}>
						<TypologyTag id={id} />
					</Col>
					<Col span={24}>
						<Title id={id} hideInformation level={4} />
					</Col>
					{size == "large" && (
						<Col span={24}>
							<Description id={id} hideInformation />
						</Col>
					)}
					<Col span={24}>
						<LocationTag id={id} />
					</Col>
				</Row>
			</PageLink>
			{!hideActions && <LeadButtons id={id} mode={mode} />}
		</>
	);

	return (
		<>
			<Card size={"small"} cover={cardTop} className={"property-card property-card-" + size}>
				{cardBottom}
			</Card>

			<style jsx global>{`
				.property-card {
					border: none;
				}

				.property-card .ant-card-cover {
					border-bottom: 1px solid ${theme.colors.borderColor};
					border-radius: ${theme.spacing.smSpacing}px ${theme.spacing.smSpacing}px 0 0;
				}

				.property-card .ant-card-cover img,
				.property-card .ant-card-cover .back-drop {
					-webkit-border-radius: ${theme.spacing.smSpacing}px ${theme.spacing.smSpacing}px
						0 0;
					border-radius: ${theme.spacing.smSpacing}px ${theme.spacing.smSpacing}px 0 0;
				}

				.property-card .ant-card-cover {
					margin-right: 0px;
					margin-left: 0px;
				}

				.property-card .ant-card-cover .property-card-top {
					padding: ${theme.spacing.smSpacing}px ${theme.spacing.smSpacing}px 0px;
				}
				.property-card .ant-card-body {
					padding: ${theme.spacing.smSpacing}px;
				}

				@media screen and (min-width: ${theme.breakPoints.sm}) {
					.property-card.property-card-large .ant-card-cover {
						border-bottom: 0;
						border-radius: ${theme.spacing.smSpacing}px 0 0 ${theme.spacing.smSpacing}px;
					}
					.property-card.property-card-large .ant-card-cover img {
						border-radius: ${theme.spacing.smSpacing}px 0 0 ${theme.spacing.smSpacing}px;
					}
					.property-card.property-card-large .ant-card-body {
						padding: ${theme.spacing.lgSpacing}px;
						border-left: 1px solid ${theme.colors.borderColor};
					}
				}
			`}</style>
		</>
	);
}

export function PropertyCardSkeleton() {
	const { theme } = useTheme();
	return (
		<>
			<Space className={"property-card-skeleton"} direction={"vertical"}>
				<Skeleton.Image style={{ width: "100%", height: "180px" }} />
				<div className={"property-card-body"}>
					<Skeleton active />
				</div>
			</Space>
			<style global jsx>{`
				.property-card-skeleton {
					width: 100%;
					background-color: ${theme.colors.backgroundColor};
					border-radius: ${theme.spacing.smSpacing}px;
					border: 1px solid ${theme.colors.borderColor};
				}
				.property-card-skeleton .property-card-body {
					padding: ${theme.spacing.mdSpacing}px ${theme.spacing.smSpacing}px;
					padding-top: 0;
				}
				.ant-skeleton-content .ant-skeleton-title {
					margin-top: 4px;
				}
				.ant-skeleton-content .ant-skeleton-title + .ant-skeleton-paragraph {
					margin: 16px 0px 0px;
				}
				.ant-skeleton.ant-skeleton-element {
					width: 100%;
				}
			`}</style>
		</>
	);
}
