import "./styles.less";
import "moment/locale/es";

import { Calendar, Col, Row, Skeleton, Space, Typography } from "antd";
import React, { FC } from "react";

import { CustomCell } from "./CustomCell/web";
import { CustomHeader } from "./CustomHeader/web";
import locale from "antd/lib/date-picker/locale/es_ES";
import moment from "moment";
import { useAvailabilityCalendar } from "./hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

moment.locale("es");

interface AvailabilityCalendarProps {
	id: string;
}

export const AvailabilityCalendar: FC<AvailabilityCalendarProps> = ({ id }) => {
	const {
		loading,
		firstMonth,
		secondMonth,
		nextMonth,
		prevMonth,
		occupancies,
	} = useAvailabilityCalendar({
		id,
	});
	const { t } = useTranslation();
	const { theme } = useTheme();

	if (loading) return <Skeleton active />;

	return (
		<>
			<Row
				className="availability-calendar"
				gutter={[theme.spacing.xxlSpacing, theme.spacing.lgSpacing]}>
				<Col xs={24} md={12}>
					<Calendar
						locale={locale}
						className="availability-calendar"
						value={firstMonth}
						fullscreen={false}
						headerRender={() => (
							<CustomHeader month={firstMonth} prevMonth={prevMonth} />
						)}
						dateFullCellRender={date => (
							<CustomCell date={date} month={firstMonth} occupancies={occupancies} />
						)}
					/>
				</Col>
				<Col xs={24} md={12}>
					<Calendar
						locale={locale}
						className="availability-calendar"
						value={secondMonth}
						fullscreen={false}
						headerRender={() => (
							<CustomHeader month={secondMonth} nextMonth={nextMonth} />
						)}
						dateFullCellRender={date => (
							<CustomCell date={date} month={secondMonth} occupancies={occupancies} />
						)}
					/>
				</Col>

				<Col span={24}>
					<div style={{ textAlign: "center" }}>
						<Space size="middle">
							<Space>
								<div className="available-box"></div>
								<Typography>{t("Disponible")}</Typography>
							</Space>
							<Space>
								<div className="available-box no-available-box"></div>
								<Typography>{t("No Disponible")}</Typography>
							</Space>
						</Space>
					</div>
				</Col>
			</Row>
			<style jsx>{`
				.available-box {
					width: ${theme.spacing.xxlSpacing}px;
					height: ${theme.spacing.xxlSpacing}px;
					border-radius: ${theme.spacing.smSpacing}px;
					background-color: ${theme.colors.backgroundColorAternative};
					border: 1px solid ${theme.colors.borderColor};
					box-sizing: border-box;
				}
				.no-available-box {
					background-color: ${theme.colors.errorColor};
					border: 1px solid ${theme.colors.errorColor};
				}
			`}</style>
		</>
	);
};
