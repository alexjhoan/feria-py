import "./styles.less";

import { Button, Empty, Skeleton, Typography, message } from "antd";

import { GoogleMap } from "../../Map/GoogleMap/web";
import { ImagenOptimizada } from "../../Image/web";
import { PropComponentMode } from "../PropertyInterfaces";
import React from "react";
import getConfig from "next/config";
import { usePropertyStreetView } from "./hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { googleMapsApiKey } = getConfig().publicRuntimeConfig;
const { Text } = Typography;

export function PropertyStreetView({ id, width, height, mode }) {
	const { marker, loading, error, loadingSendLead, sendLead } = usePropertyStreetView({
		id,
		mode,
	});
	const { theme } = useTheme();
	const { t } = useTranslation();

	if (loading) return <Skeleton active />;
	if (error) return null;
	if (!marker)
		return (
			<Empty
				imageStyle={{ height: 50, opacity: 0.9 }}
				description={
					<Text style={{ color: theme.colors.textInverseColor }}>
						{t("La propiedad no tiene ubicación")}
					</Text>
				}>
				<Button
					loading={loadingSendLead}
					onClick={() =>
						sendLead().then(response => {
							response.success
								? message.success("Consulta enviada")
								: message.error("Error al enviar la consulta");
						})
					}
					ghost>
					{t("Pedir ubicación")}
				</Button>
			</Empty>
		);

	return (
		<>
			<div style={{ width: width, height: height, display: "flex" }}>
				<GoogleMap streetView={true} center={marker} />
			</div>
		</>
	);
}

export function PropertyStreetViewStatic({
	id,
	size = "small",
	onClick = false,
	mode = "auto",
}: {
	id: any;
	size?: "small" | "big";
	onClick: CallableFunction | false;
	mode?: PropComponentMode;
}) {
	const { marker, loading, error } = usePropertyStreetView({ id, mode });

	if (loading) return <Skeleton.Image className={"street-view-skeleton"} />;
	if (error || !marker) return null;

	const url = `https://maps.googleapis.com/maps/api/streetview?size=${
		size == "big" ? 640 : 320
	}x${size == "big" ? 640 : 320}&location=${marker.latitude},${
		marker.longitude
	}&fov=80&heading=70&pitch=0&key=${googleMapsApiKey}`;

	return (
		<>
			<div className="static-street-view" onClick={x => !!onClick && onClick(x)}>
				<ImagenOptimizada src={url} alt="map" />
			</div>
			<style jsx>{`
				.static-street-view {
					width: 100%;
					height: 100%;
					cursor: ${!!onClick ? "pointer" : "default"};
				}
				.static-street-view :global(img) {
					width: 100%;
					height: 100%;
					object-fit: cover;
					transition: all ease 300ms;
				}
				.static-street-view:hover :global(img) {
					transform: scale(1.1);
				}
			`}</style>
		</>
	);
}
