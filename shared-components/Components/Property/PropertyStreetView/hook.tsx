import { usePropertyMap } from "../PropertyMap/hook";

export const usePropertyStreetView = ({ id, mode }) => {
	return usePropertyMap({ id, mode });
};
