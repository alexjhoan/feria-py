import { Button, Checkbox, Col, Form, Input, Row, Radio, Typography, Popover, Space } from "antd";
const { Text } = Typography;
import { useContact } from "../hook";
import React, { useEffect, useRef, useState } from "react";
import { QuestionCircleOutlined } from "@ant-design/icons";
import { useUser } from "../../../User/User.hook";
import { useTranslation } from "react-i18next";
import { LeadButtons } from "../LeadButtons/web";
import { Rule } from "antd/lib/form";
import { DateSelector } from "./DateSelector/web";
import { useGoogleAnalytics } from "../../../../GlobalHooks/web/GoogleAnalytics.hook";
import { useRouter } from "next/router";

export const ScheduleVisitForm = ({ id, onComplete = null, form, identifier = 1, mode }) => {
	const { sendContact, resetForm, formCompleted, loading, disabledEmail } = useContact({
		id,
		onComplete,
		type: "schedule",
		mode,
	});
	const Router = useRouter();
	const { user } = useUser();
	const { t } = useTranslation();
	const GA = useGoogleAnalytics();

	const [selectedDate, setSelectedDate] = useState(null);
	useEffect(() => form.setFieldsValue({ date: selectedDate }), [selectedDate]);

	const reset = () => {
		resetForm();
		form.resetFields();
	};

	useEffect(() => {
		Router.events.on("routeChangeComplete", () => reset());
	}, []);

	useEffect(() => {
		if (user.data) {
			form.setFieldsValue({
				nombre: user.data.me.name,
				telefono: user.data.me.phone,
				email: user.data.me.email,
			});
		}
	}, [user]);

	useEffect(() => {
		if (formCompleted) {
			GA.Event({
				category: "formulario",
				action: "formulario ic2",
				label: "schedule visit form",
				value: 1,
			});
		}
	}, [formCompleted]);

	const validateMessages = {
		required: "El ${name} es requerido!",
		types: {
			email: "El ${name} no es valido!",
			number: "El ${name} no es un numero valido!",
		},
	};

	const validateRules: { [x: string]: Rule[] } = {
		nombre: [{ required: true }],
		telefono: [
			{
				required: true,
				pattern: new RegExp("^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-s./0-9]*$"),
				message: t("Teléfono no válido"),
			},
		],
		email: [{ required: true, type: "email" }],
	};

	const submitForm = () => {
		form.validateFields()
			.then(values => sendContact(values))
			.catch(info => console.log("Validate Failed:", info));
	};

	return (
		<>
			<Form
				form={form}
				name={"leadForm_" + identifier}
				initialValues={{
					nombre: user.data?.me.name,
					telefono: user.data?.me.phone,
					email: user.data?.me.email,
					visit_type: "VIRTUAL",
					date: selectedDate,
				}}
				scrollToFirstError
				validateMessages={validateMessages}>
				<Form.Item
					labelCol={{ span: 24 }}
					name="visit_type"
					label={
						<Space>
							<Text type={"secondary"}>{t("Tipo de Visita")}</Text>
							<Popover
								overlayClassName={"note-popover"}
								content={t(
									"Si te gustaría dar un tour por esta casa sin dejar la tuya, selecciona el vídeo chat y discute las opciones disponibles con el agente que estés conectado."
								)}>
								<QuestionCircleOutlined />
							</Popover>
						</Space>
					}>
					<Radio.Group buttonStyle="solid" className={"visit-type-radio"}>
						<Radio.Button value="EN_PERSONA">{t("En Persona")}</Radio.Button>
						<Radio.Button value="VIRTUAL">{t("Virtual")}</Radio.Button>
					</Radio.Group>
				</Form.Item>
				<Form.Item name="date" label={null} style={{ display: "none" }}>
					<Input />
				</Form.Item>
				<Form.Item>
					<DateSelector selected={selectedDate} onSelect={setSelectedDate} />
				</Form.Item>
				<Row gutter={12}>
					<Col span={12}>
						<Form.Item name="nombre" hasFeedback rules={validateRules.nombre}>
							<Input className="secondary" placeholder={t("Nombre")} />
						</Form.Item>
					</Col>
					<Col span={12}>
						<Form.Item name="telefono" hasFeedback rules={validateRules.telefono}>
							<Input className="secondary" placeholder={t("Teléfono")} />
						</Form.Item>
					</Col>
				</Row>
				<Form.Item name="email" hasFeedback rules={validateRules.email}>
					<Input
						disabled={disabledEmail}
						className="secondary"
						placeholder={t("Email")}
					/>
				</Form.Item>
				<Form.Item name="financing_information" valuePropName="checked">
					<Checkbox>{t("Quiero info. sobre financiamiento")}</Checkbox>
				</Form.Item>
				<Form.Item>
					<LeadButtons id={id} mode={mode}>
						<Button
							type="primary"
							loading={loading}
							disabled={formCompleted}
							className="btn-send-lead"
							onClick={submitForm}>
							{formCompleted
								? t("Enviado")
								: loading
								? t("Enviando")
								: t("Coordinar Visita")}
						</Button>
					</LeadButtons>
				</Form.Item>
			</Form>
		</>
	);
};
