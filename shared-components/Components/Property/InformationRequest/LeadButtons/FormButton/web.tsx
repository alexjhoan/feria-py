import "./styles.less";

import { Button, Modal } from "antd";
import React, { useState } from "react";

import { ButtonSize } from "antd/lib/button";
import { CloseCircleOutlined } from "@ant-design/icons";
import { FullScreen } from "../../../../FullScreen/web";
import { InformationRequest } from "../../web";
import { useIsAuthModalShowing } from "../../../../../GlobalHooks/useIsAuthModalShowing";
import { useTheme } from "../../../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

export const FormButton = ({ id, size = "middle" }: { id: string; size?: ButtonSize }) => {
	const { t } = useTranslation();
	const { theme } = useTheme();
	const { isAuthModalShowing } = useIsAuthModalShowing();
	const [modal, setModal] = useState(false);

	return (
		<>
			<Button
				size={size}
				type="primary"
				style={{ width: "100%" }}
				onClick={e => {
					e.preventDefault();
					e.stopPropagation();
					setModal(true);
				}}>
				{t("Contactar")}
			</Button>
			<Modal
				className="form-modal"
				visible={modal && !isAuthModalShowing}
				footer={null}
				maskClosable
				closeIcon={<CloseCircleOutlined />}
				onCancel={() => setModal(false)}
				title={t("Contactar al anunciante")}>
				<FullScreen offset={55} className="modal-form-content">
					<InformationRequest onComplete={() => setModal(false)} id={id} />
				</FullScreen>
			</Modal>
			<style jsx global>{`
				@media screen and (max-width: ${theme.breakPoints.lg}) {
					.form-modal .modal-form-content {
						padding: ${theme.spacing.lgSpacing}px;
					}
				}
			`}</style>
		</>
	);
};
