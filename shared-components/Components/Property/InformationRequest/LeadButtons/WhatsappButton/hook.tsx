import { FragmentDefiner, useReadFragment } from "../../../../../GlobalHooks/useReadFragment";
import { gql } from "@apollo/client";
import { useContext, useEffect, useState } from "react";
import { ConfigStateContext } from "../../../../../Contexts/Configurations/context";
import { useTranslation } from "react-i18next";
import { useMutation } from "@apollo/client";

export const FRAGMENT_HAS_WHATSAPP = new FragmentDefiner(
	"Property",
	`
    title
    link
    owner {
        id
        has_whatsapp
    }
`
);


const REQUEST_WPP_MUTATION = gql`
    mutation request_wpp_mutation($property_id: Int!){
		requestPhone(property_id: $property_id, isWpp:true)
    }
`;



export const useWhatsppButton = (propId: number) => {
    const {loading, data, error} = useReadFragment(FRAGMENT_HAS_WHATSAPP, propId);
    const {main_domain} = useContext(ConfigStateContext);
    const {t} = useTranslation();
    const [wpMessage, setWppMessage] = useState<string>('');

    useEffect(()=>{
        if(data?.title && data?.link){
            setWppMessage(` ${t('Hola, ví esta propiedad en InfoCasas y me gustaría tener más información.')} %0D%0A ${data?.title} %0D%0A https://${main_domain}/${data?.link}`);
        }
    },[data])

    const [wppMutation, { loading:asd, data: whatsappNumberData }] = useMutation(REQUEST_WPP_MUTATION, {
		onError: error => {console.error('User Unauthenticated.')},
    });

    const getWpp = () => {
        return wppMutation({variables: { property_id: propId }})
    }


    return {
        loading,
        error,
        hasWhatsapp: data ? data.owner.has_whatsapp : false,
        authError: whatsappNumberData?.error,
        getWppNumber: getWpp,
        wppMessage: wpMessage
    }
}

