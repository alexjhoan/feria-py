import { useEffect, useState } from "react"
import { FragmentDefiner, useReadFragment } from "../../../../GlobalHooks/useReadFragment";
import { formatMoney, postRequest, parserMoney, parseMoney } from "../../../../Utils/Functions";

const PRICE_FRAGMENT = new FragmentDefiner("Property", `
  price {
    amount
  }
`);

const BBVA_SERVICE = "https://prod.infocasas.com.uy/microservices/simulador/?/bbva/simulation";
const BBVA_GET_VALUES = "https://prod.infocasas.com.uy/microservices/simulador/?/bbva/getValuesProperty/";

export const useBBVAFinancing = ({id}) => {
  const { data: propertyData } = useReadFragment(PRICE_FRAGMENT, id);
  const [loan, setLoan] = useState(0);
  const [propertyPrice, setPropertyPrice] = useState(0);
  const [income, setIncome] = useState(0);
  const [years, setYears] = useState(25);
  const [currency, setCurrency] = useState("ui");
  const [type, setType] = useState(0);
  const [asset, setAsset] = useState(0);
  const [payment, setPayment] = useState(0);
  const [loading, setLoading] = useState(true);
  const [firstTime, setFirstTime] = useState(true);
  const [error, setError] = useState("");
  const [fixErrorObject, setFixErrorObject] = useState(null);
  const [extraData, setExtraData] = useState(null);
  let timer = null;

  const ASSETS = [
    {
      value: "sinnomina",
      text: "No"
    },
    {
      value: "nomina",
      text: "Si"
    }
  ]

  const TYPES = [
    {
      value: "primeravivienda",
      text: "Vivienda Permanente"
    },
    {
      value: "segundavivienda",
      text: "Segunda Vivienda"
    }
  ]

  const CURRENCIES = [
    {
      value: "ui",
      text: "Unidades Indexadas"
    },
    {
      value: "usd",
      text: "Dólares"
    }
  ]

  const YEARS = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,23,24,25].map(year => ({
    value: year,
    text: `${year} año${year > 1 ? "s" : ""}`
  }))

  const fixError = () => {
    if(fixErrorObject.monto){
      setLoan(parserMoney(fixErrorObject.monto));
    }
    if(fixErrorObject.plazo){
      setYears(fixErrorObject.plazo);
    }
    if(fixErrorObject.ingreso){
      setIncome(parserMoney(fixErrorObject.ingreso));
    }
  }

  useEffect(() => {
    if(propertyData){
      setPropertyPrice(propertyData.price.amount);
    }
  }, [propertyData]);

  useEffect(() => {
    const getValues = async () => {
      const response = await fetch(BBVA_GET_VALUES + propertyPrice);
      const data = await response.json();

      if (data) {
        setPropertyPrice(parserMoney(data.montopropiedad));
        setLoan(parserMoney(data.montoasolicitar));
        setIncome(parserMoney(data.ingreso_mensual));
        setYears(data.plazo);
        setType(data.destino);
        setAsset(data.cliente);
        setPayment(data.cuota_por_defecto);
        setFirstTime(false);
      }
    }

    if(propertyPrice > 0){
      getValues();
    }
  }, [propertyPrice]);

  useEffect(() => {
    const bbvaService = async () => {
      setLoading(true);
      const data = await postRequest(BBVA_SERVICE, {
        valor_propiedad: formatMoney(propertyPrice),
        montopropiedad: `U$S ${formatMoney(propertyPrice)}`,
        IDpropiedad: id,
        montoasolicitar: formatMoney(loan),
        ingreso_mensual: formatMoney(income),
        monedaprestamo: currency,
        plazo: years,
        destino: type,
        cliente: asset
      });

      if (data) {
        setPayment(data.primer_cuota_con_gastos_uy);
        setExtraData(data);

        if(data.notice){
          setError({__html: data.notice});
          let errorObject = {
            monto: null,
            plazo: null,
            ingreso: null
          };
          if(data.monto){
            errorObject.monto = data.monto;
          }
          if(data.plazo){
            errorObject.plazo = data.plazo;
          }
          if(data.ingreso){
            errorObject.ingreso = data.ingreso;
          }
          setFixErrorObject(errorObject);
        } else {
          setError("");
        }
        setLoading(false);
      }
    }

    if (!firstTime && loan > 0 && income > 0) {
      clearTimeout(timer);
      timer = setTimeout(() => {
        bbvaService();
      }, 600);
    }

    return () => clearTimeout(timer);
  }, [loan, income, years, currency, type, asset])

  return {
    loan: {
      amount: loan,
      change: setLoan
    },
    property: {
      amount: propertyPrice
    },
    income: {
      amount: income,
      change: setIncome
    },
    years: {
      totalYears: YEARS,
      value: years,
      change: setYears
    },
    currency: {
      options: CURRENCIES,
      value: currency,
      change: setCurrency
    },
    type: {
      options: TYPES,
      value: type,
      change: setType
    },
    asset: {
      options: ASSETS,
      value: asset,
      change: setAsset
    },
    payment,
    error,
    fixError,
    extraData
  }
}