import { useState } from "react";
import { Row, Col, Typography, Button, notification } from "antd";
import { useTranslation } from "react-i18next";
import { useTheme } from "../../../../Styles/ThemeHook";
import { ImagenOptimizada } from "../../../Image/web";
import { FinancingInput } from "../FinancingInput/FinancingInput";
import { useBBVAFinancing } from "./hook";
import { InputWithPercentage, Input, Select } from "../dataInputs";
import { ContactModal } from "../ContactModal/web";
import "./styles.less";

const LEAD_SERVICE = "https://prod.infocasas.com.uy/microservices/simulador/?/bbva/sendForm";

export const BBVAFinancing = ({ id }) => {
	const {
		loan: { amount: loan, change: setLoan },
		property: { amount: propertyPrice },
		income: { amount: income, change: setIncome },
		years: { value: years, change: setYears, totalYears },
		currency: { value: currency, change: setCurrency, options: currencies },
		type: { value: type, change: setType, options: types },
		asset: { value: asset, change: setAsset, options: assets },
		payment,
		error,
		fixError,
		extraData,
	} = useBBVAFinancing({ id });
	const { t } = useTranslation();
	const { theme } = useTheme();
	const [modal, setModal] = useState(false);

	return (
		<div className="property-financing-BBVA">
			<Row gutter={[0, theme.spacing.mdSpacing]}>
				<Col span={24}>
					<Typography.Title level={4}>{t("Financiación")}</Typography.Title>
				</Col>
				<Col xs={24} md={16}>
					<div className="property-financing-box">
						<div className="bbva-logo">
							<ImagenOptimizada
								src="https://cdn2.infocasas.com.uy/web/5d496d2a66e91_infocdn__landing-bbva_uy-07-min.png"
								alt="BBVA"
							/>
							<span>Creando Oportunidades</span>
						</div>
						<Row gutter={[theme.spacing.xxlSpacing, theme.spacing.mdSpacing]}>
							<Col xs={24} md={12}>
								<FinancingInput
									title="Importe a Solicitar"
									input={
										<InputWithPercentage
											value={loan}
											max={propertyPrice}
											onChange={setLoan}
											percentage={Math.round((loan * 100) / propertyPrice)}
											currency="U$S"
										/>
									}
									fullWidth
								/>
							</Col>
							<Col xs={24} md={12}>
								<FinancingInput
									title="Ingresos Mensuales"
									input={
										<Input value={income} onChange={setIncome} currency="$" />
									}
									fullWidth
								/>
							</Col>
							<Col xs={24} md={12}>
								<FinancingInput
									title={t("Moneda del Préstamo")}
									input={
										<Select
											value={currency}
											options={currencies}
											onChange={setCurrency}
										/>
									}
									fullWidth
								/>
							</Col>
							<Col xs={24} md={12}>
								<FinancingInput
									title="Plazo"
									input={
										<Select
											value={years}
											options={totalYears}
											onChange={setYears}
										/>
									}
									fullWidth
								/>
							</Col>
							<Col xs={24} md={12}>
								<FinancingInput
									title="Tipo de Vivienda"
									input={
										<Select value={type} options={types} onChange={setType} />
									}
									fullWidth
								/>
							</Col>
							<Col xs={24} md={12}>
								<FinancingInput
									title="Cobro Haberes en BBVA"
									input={
										<Select
											value={asset}
											options={assets}
											onChange={setAsset}
										/>
									}
									fullWidth
								/>
							</Col>
						</Row>
					</div>
				</Col>
				<Col xs={24} md={8} className="property-financing-monthly">
					{payment ? (
						<>
							<Typography.Title>Tu cuota mensual:</Typography.Title>
							<Typography.Title className="financing-price">
								$ {payment}
							</Typography.Title>
							<Button
								className="bbva-financing-button"
								onClick={() => setModal(true)}>
								Obtener más información
							</Button>
						</>
					) : null}
					{error ? (
						<div className="financing-error">
							<Typography.Paragraph>
								<div dangerouslySetInnerHTML={error} />
								<span onClick={fixError} className="financing-error-fix">
									Corregir y volver a calcular.
								</span>
							</Typography.Paragraph>
						</div>
					) : null}
				</Col>
			</Row>
			<ContactModal
				sendLeadURL={LEAD_SERVICE}
				extraData={extraData}
				onSuccess={() => {
					setModal(false);
					notification.success({
						message: <b>{t("¡Gracias por tu consulta!")}</b>,
						description: t(
							"Tus datos han sido enviados con éxito. En las próximas horas nos contacataremos contigo para darte más información."
						),
						style: { maxWidth: "calc(100% - 16px)" },
					});
				}}
				title="Calcular cuota de préstamo"
				topText="Completa estos datos para recibir más información"
				visible={modal}
				closeModal={() => setModal(false)}
			/>
		</div>
	);
};
