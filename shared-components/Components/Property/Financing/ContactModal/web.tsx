import { useEffect, useState } from "react";
import { Button, Modal, Form, Row, Col, Input, Typography } from "antd";
import { CloseCircleOutlined } from "@ant-design/icons";
import { FullScreen } from "../../../FullScreen/web";
import { useTheme } from "../../../../Styles/ThemeHook";
import { useUser } from "../../../User/User.hook";
import { Rule } from "antd/lib/form";
import { postRequest } from "../../../../Utils/Functions";

export const ContactModal = ({
	visible,
	closeModal,
	title,
	topText = null,
	bottomText = null,
	extraData,
	sendLeadURL,
	onSuccess,
	showCI = false,
}) => {
	const { theme } = useTheme();
	const { user } = useUser();
	const [form] = Form.useForm();
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		form.setFieldsValue({
			nombre: user.data?.me.firstName,
			apellido: user.data?.me.lastName,
			email: user.data?.me.email,
			telefono: user.data?.me.phone,
			ci: user.data?.me.ci,
		});
	}, [user]);

	const validateMessages = {
		required: "El ${name} es requerido!",
		types: {
			email: "El ${name} no es valido!",
			number: "El ${name} no es un numero valido!",
		},
	};
	const validateRules: { [x: string]: Rule[] } = {
		nombre: [{ required: true }],
		apellido: [{ required: true }],
		telefono: [
			{
				required: true,
				pattern: new RegExp("^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-s./0-9]*$"),
				message: "Teléfono no válido",
			},
		],
		email: [{ required: true, type: "email" }],
	};

	const submitForm = () => {
		form.validateFields().then(async values => {
			setLoading(true);
			let data = await postRequest(sendLeadURL, {
				...values,
				...extraData,
			});
			onSuccess();
			setLoading(false);
		});
	};

	return (
		<Modal
			className="form-modal"
			visible={visible}
			footer={null}
			forceRender
			maskClosable
			closeIcon={<CloseCircleOutlined />}
			onCancel={closeModal}
			title={title}>
			<FullScreen offset={55} className="modal-form-content financing-form">
				<Form
					form={form}
					name="financing-form"
					initialValues={{
						nombre: user.data?.me.firstName,
						apellido: user.data?.me.lastName,
						email: user.data?.me.email,
						telefono: user.data?.me.phone,
						ci: user.data?.me.ci,
					}}
					scrollToFirstError
					validateMessages={validateMessages}>
					<Row gutter={12}>
						{topText ? (
							<Col xs={24}>
								<Typography.Paragraph>{topText}</Typography.Paragraph>
							</Col>
						) : null}
						<Col xs={24} md={12}>
							<Form.Item name="nombre" hasFeedback rules={validateRules.nombre}>
								<Input className="secondary" placeholder="Nombre *" />
							</Form.Item>
						</Col>
						<Col xs={24} md={12}>
							<Form.Item name="apellido" hasFeedback rules={validateRules.apellido}>
								<Input className="secondary" placeholder="Apellido *" />
							</Form.Item>
						</Col>
						<Col xs={24}>
							<Form.Item name="email" hasFeedback rules={validateRules.email}>
								<Input className="secondary" placeholder="Correo Electrónico *" />
							</Form.Item>
						</Col>
						<Col xs={24}>
							<Form.Item name="tel" hasFeedback rules={validateRules.telefono}>
								<Input className="secondary" placeholder="Teléfono *" />
							</Form.Item>
						</Col>
						{showCI ? (
							<Col xs={24}>
								<Form.Item name="ci" hasFeedback>
									<Input className="secondary" placeholder="C.I." />
								</Form.Item>
							</Col>
						) : null}
						<Col xs={24}>
							<Form.Item>
								<Button
									type="primary"
									className="btn-send-lead"
									onClick={() => {}}
									loading={loading}
									onClick={submitForm}>
									Enviar Consulta
								</Button>
							</Form.Item>
						</Col>
						{bottomText ? (
							<Col xs={24}>
								{bottomText.map((text, i) => (
									<Typography.Paragraph key={"contat_p_" + i} type="secondary">
										{text}
									</Typography.Paragraph>
								))}
							</Col>
						) : null}
					</Row>
				</Form>
			</FullScreen>
			<style jsx global>{`
				@media screen and (max-width: ${theme.breakPoints.lg}) {
					.form-modal .modal-form-content {
						padding: ${theme.spacing.lgSpacing}px;
					}
				}
				.financing-form .ant-typography-secondary {
					font-size: 0.9em;
					margin-bottom: 5px;
				}
			`}</style>
		</Modal>
	);
};
