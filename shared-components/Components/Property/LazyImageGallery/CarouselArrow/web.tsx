import { LeftArrowIcon, RightArrowIcon } from "../../../CustomIcons/web";
import React, { FC } from "react";

import { useTheme } from "../../../../Styles/ThemeHook";

interface CarouselArrowProps {
	onClick?: () => {};
	className?: "string";
	side?: "right" | "left";
	position?: "inside" | "outside";
	type?: "filled" | "ghost";
}

export const CarouselArrow: FC<CarouselArrowProps> = ({
	onClick = null,
	side = "right",
	type = "ghost",
	position = "inside",
	className,
	...rest
}) => {
	const { theme } = useTheme();
	if (className.indexOf("slick-disabled") != -1) return null;

	return (
		<React.Fragment>
			<div
				className={"carrouselArrow " + side}
				onClick={e => {
					e.stopPropagation();
					e.preventDefault();
					onClick();
				}}>
				{side == "right" ? (
					<RightArrowIcon className="icon-arrow" />
				) : (
					<LeftArrowIcon className="icon-arrow" />
				)}
			</div>
			<style jsx>{`
                .carrouselArrow {
                    font-size:30px;
                    opacity: 0.85;
                    // color: ${theme.colors.textColor};
                    color: ${type == "filled" ? theme.colors.textColor : theme.colors.borderColor};
                    position absolute;
                    top: 50%;
                    margin-top: -18px;
                    width: 35px;
                    height: 35px;
                    padding: 0px;
                    border-radius: 20px;
                    // background: ${theme.colors.backgroundColor};
                    background:${type == "filled" ? theme.colors.backgroundColor : "transparent"};
                    display:flex;
                    align-items:center;
                    justify-content:center;
                    z-index:2;
                    cursor:pointer;
                    border:${type == "filled" ? "1px solid " + theme.colors.borderColor : "none"};
                    transition: all ease 200ms;
                }
                .carrouselArrow:hover{
                    background: ${
						type == "filled"
							? theme.colors.borderColor
							: theme.colors.textSecondaryColor
					};
                    color: ${
						type == "filled" ? theme.colors.textColor : theme.colors.backgroundColor
					};
                    opacity: 1 !important;
                    transform: ${type == "filled" ? "scale(1.1)" : "none"};
                }
                .carrouselArrow.right{
                    right: ${
						position == "inside" ? theme.spacing.smSpacing : -theme.spacing.lgSpacing
					}px;
                }
                .carrouselArrow.left{
                    left: ${
						position == "inside" ? theme.spacing.smSpacing : -theme.spacing.lgSpacing
					}px;
                }
                .carrouselArrow :global(.icon-arrow){
                    font-size: ${type == "filled" ? 18 : 20}px;
                    text-shadow: ${type == "filled" ? "none" : "0 0 3px rgba(0,0,0,0.6)"};
                    position: relative;
                }
                .carrouselArrow.left :global(.icon-arrow){
                    left: -1px;
                }
                .carrouselArrow.right :global(.icon-arrow){
                    right: -1px;
                }
                @media screen and (max-width: ${theme.breakPoints.lg}){
                   
                    .carrouselArrow.right{
                        right: ${
							position == "inside"
								? theme.spacing.smSpacing
								: -theme.spacing.mdSpacing
						}px;
                    }
                    .carrouselArrow.left{
                        left: ${
							position == "inside"
								? theme.spacing.smSpacing
								: -theme.spacing.mdSpacing
						}px;
                    }
                }
            `}</style>
		</React.Fragment>
	);
};
