import "./styles.less";

import React, { FC } from "react";
import { Skeleton, Typography } from "antd";
import { TitleProps, useTitle } from "./Title.hook";

import { useTheme } from "../../../Styles/ThemeHook";

export const Title: FC<TitleProps> = ({
	id,
	level = 1,
	limitLength = 1,
	hideInformation = false,
	mode = "auto",
}) => {
	const { title, loading, error } = useTitle(id, mode);
	const { theme } = useTheme();

	if (loading) return <TitleSkeleton />;
	if (error) return null;

	const ellipsis = hideInformation ? { rows: limitLength } : false;
	return (
		<>
			<Typography.Title
				level={level}
				ellipsis={ellipsis}
				className="property-title"
				title={title}>
				{title}
			</Typography.Title>
			<style jsx global>{`
				// NO BORRAR, ESTAMOS PROBANDO 
				// .property-card .property-title {
				// 	font-size: ${theme.fontSizes.smFontSize};
				// 	line-height: ${theme.fontSizes.baseLineHeight};
				// 	margin-bottom: 0px;
				// }
				.property-card .property-title {
					font-size: ${theme.fontSizes.baseFontSize};
					line-height: ${theme.fontSizes.baseLineHeight};
					margin-bottom: 0px;
					font-weight: normal;
				}
				.pd-main-content .property-title {
					font-size: ${theme.fontSizes.lgTitle};
					line-height: ${theme.fontSizes.lgLineHeight};
				}

				.pd_affix_top .property-title {
					font-size: ${theme.fontSizes.xsTitle};
					line-height: ${theme.fontSizes.xsLineHeight};
				}
				@media screen and (max-width: ${theme.breakPoints.lg}) {
					.pd-main-content .property-title {
						font-size: ${theme.fontSizes.smTitle};
						line-height: ${theme.fontSizes.smLineHeight};
					}
				}
			`}</style>
		</>
	);
};

export const TitleSkeleton = () => {
	return <Skeleton title paragraph={false} className="title-skeleton" />;
};
