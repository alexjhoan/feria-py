import { LinkBox } from './LinkBox/web';
import { MetaTags } from './MetaTags/web';
import { H1 } from './H1/web';
import { BreadCrumbs } from './BreadCrumbs/web';
import { SeoText } from './SeoText/web';
import { LastSearchCard } from './LastSearchCard/web';

export { LinkBox, LastSearchCard, SeoText, BreadCrumbs, H1, MetaTags };
