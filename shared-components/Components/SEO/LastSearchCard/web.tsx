import { useSEO } from "../hook";
import { useTranslation } from "react-i18next";
import { Card, Skeleton, Space, Button, Typography } from "antd";
import { FlagOutlined } from "@ant-design/icons";
import Link from "next/link";
import { useTheme } from "../../../Styles/ThemeHook";
const { Text } = Typography;
import "./styles.less";

export const LastSearchCard = () => {
	const { seoMetaTags, loading: load } = useSEO();
	const { t } = useTranslation();
	const { theme } = useTheme();
	if (load)
		return (
			<Card size={"small"} className="more-similars">
				<Skeleton active />
			</Card>
		);

	if (!seoMetaTags.lastSearch) {
		return null;
	}

	return (
		<Link href={seoMetaTags.lastSearch.url}>
			<a>
				<Card size={"small"} className="more-similars">
					<div className="more-similars-container">
						<FlagOutlined />
						<Space direction="vertical" size={0}>
							<Text>{t("Ver más propiedades en") + " "}</Text>
							<Text strong style={{ fontSize: theme.fontSizes.mdTitle }}>
								{seoMetaTags.lastSearch.text}
							</Text>
						</Space>
						<Button ghost>{t("Ver Más")}</Button>
					</div>
				</Card>
			</a>
		</Link>
	);
};
