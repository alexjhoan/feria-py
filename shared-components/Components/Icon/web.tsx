import React from "react";
import { IconProps } from "./Icon.hook";
import theme from "../../../styles/IC2_theme";

export const Icon = ({
	name,
	size = 14,
	color = theme.colors.text,
}: IconProps) => {
	return (
		<>
			<i>{name}</i>
			<style jsx>{`
				i {
					font-family: "infocasas";
					font-size: ${size}px;
					color: ${color};
					font-style: normal;
					font-weight: normal;
					speak: none;
				}
			`}</style>
		</>
	);
};
