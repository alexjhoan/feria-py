import React, { FC } from "react"
import { Text } from "react-native"
import { IconProps } from "./Icon.hook"


export const Icon = ({name, size=30, color='#000'}:IconProps) => {
    return (<>
        <Text style={{
            fontFamily: 'ic-icons',
            fontSize: size,
            color: color,
        }}>{name}</Text>
    </>)
} 
