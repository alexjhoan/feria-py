import { Collapse, Empty, Select, Space, Tooltip, Typography } from "antd";
import { DownOutlined, LoadingOutlined } from "@ant-design/icons";
import { LocationProps, useLocation } from "./Location.hook";

import React from "react";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Option } = Select;
const { Panel } = Collapse;
const { Title } = Typography;

export const Location = ({ collapsable = false, labeled = false, ...props }: LocationProps) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const {
		selectedLocations,
		location,
		show,
		onSearch,
		onChange,
		searchResults,
		searchLoading,
		selectedByUrl,
	} = useLocation(props);

	if (!show) return null;

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter"></span>
		</Tooltip>
	);

	const printOption = (opt, i) => {
		let text = opt.estate ? `${opt.estate.name}, ${opt.name}` : opt.name;
		let key = opt.estate
			? `${opt.estate.id}#${opt.id}#${opt.estate.name}#${opt.name}`
			: `${opt.id}#null#${opt.name}#null`;
		return (
			<Option key={key} value={key}>
				{t(text)}
			</Option>
		);
	};

	const filter = (
		<Select
			className="secondary"
			dropdownClassName="secondary"
			mode="multiple"
			style={{ width: "100%" }}
			placeholder="Buscar por ubicación"
			value={selectedLocations}
			onChange={onChange}
			onSearch={onSearch}
			filterOption={false}
			labelInValue
			notFoundContent={
				searchLoading ? (
					<Space>
						<LoadingOutlined style={{ fontSize: 18 }} />
						<Typography.Text type={"secondary"}>{t("Cargando")}</Typography.Text>
					</Space>
				) : location && location.length > 0 ? (
					<Empty description={t("Sin resultados")} imageStyle={{ height: "50px" }} />
				) : null
			}>
			{searchResults && searchResults?.searchLocation.map(printOption)}
		</Select>
	);

	const label = "Ubicación";

	return (
		<div>
			<div className={"filter location-filter"}>
				{collapsable ? (
					<Collapse
						ghost
						bordered={false}
						expandIconPosition="right"
						defaultActiveKey={selectedByUrl ? "false" : "true"}
						expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
						<Panel
							extra={selectedLocations.length >= 1 && extraIcon()}
							header={t(label)}
							key={"true"}
							className={selectedLocations.length >= 1 && "is-selected-some-filter"}>
							{filter}
						</Panel>
					</Collapse>
				) : (
					<>
						{labeled && <Title level={4}>{t(label)}</Title>}
						{filter}
					</>
				)}
			</div>
			<style jsx global>{``}</style>
		</div>
	);
};
