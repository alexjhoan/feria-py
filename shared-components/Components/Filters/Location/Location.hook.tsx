import { useState, useEffect } from "react";
import { gql, useLazyQuery } from "@apollo/client";
import { useFilters } from "../Filters.hook";
import { isTemporal } from "../../../Utils/Functions";

const ALL_LOCATIONS_QUERY = gql`
	query Locations($strSearch: String!, $estate_id: Int) {
		searchLocation(searchTerm: $strSearch, estate_id: $estate_id) {
			... on Estate {
				id
				name
			}
			... on Neighborhood {
				id
				name
				estate {
					id
					name
				}
			}
			__typename
		}
	}
`;

export interface LocationProps {
	filterChanged: ({}: any) => void;
	collapsable?: boolean;
	labeled?: boolean;
}

export const useLocation = ({ filterChanged }: LocationProps) => {
	// hook setup
	const { filters, filtersTags } = useFilters();
	const [locationSearch, setLocationSearch] = useState(null);
	const [selectedLocations, setSelectedLocations] = useState([]);

	useEffect(() => {
		if (filtersTags.neighborhood_id && filtersTags.estate_id) {
			setSelectedLocations(
				filtersTags.neighborhood_id.map(location => ({
					value: `${filtersTags.estate_id.value}#${location.value}#${filtersTags.estate_id.text}#${location.text}`,
					label: `${filtersTags.estate_id.text}, ${location.text}`,
				}))
			);
		} else if (filtersTags.estate_id) {
			setSelectedLocations([
				{
					value: `${filtersTags.estate_id.value}#null#${filtersTags.estate_id.text}#null`,
					label: filtersTags.estate_id.text,
				},
			]);
		} else {
			setSelectedLocations([]);
		}
	}, [filtersTags.neighborhood_id, filtersTags.estate_id]);

	// hook functions
	const onChange = (values, options) => {
		let estate_id = null;
		let neighborhood_id = null;

		if (values.length > 1) {
			const [estateId, neighborhoodId, estateName, neighborhoodName] = values[0].key.split(
				"#"
			);
			const [
				selectedEstateId,
				selectedNeighborhoodId,
				selectedEstateName,
				selectedNeighborhoodName,
			] = values[values.length - 1].key.split("#");

			if (selectedEstateId == estateId) {
				estate_id = {
					value: selectedEstateId,
					text: selectedEstateName,
				};

				if (selectedNeighborhoodId != "null") {
					neighborhood_id = [];
					values.forEach(value => {
						const [
							valueEstateId,
							valueNeighborhoodId,
							valueEstateName,
							valueNeighborhoodName,
						] = value.key.split("#");
						if (valueNeighborhoodId != "null") {
							neighborhood_id.push({
								value: valueNeighborhoodId,
								text: valueNeighborhoodName,
							});
						}
					});
				}
			} else {
				estate_id = {
					value: selectedEstateId,
					text: selectedEstateName,
				};
				if (selectedNeighborhoodId != "null") {
					neighborhood_id = [
						{
							value: selectedNeighborhoodId,
							text: selectedNeighborhoodName,
						},
					];
				}
			}
		} else if (values.length == 1) {
			const [estateId, neighborhoodId, estateName, neighborhoodName] = values[0].key.split(
				"#"
			);
			estate_id = {
				value: estateId,
				text: estateName,
			};
			if (neighborhoodId != "null") {
				neighborhood_id = [
					{
						value: neighborhoodId,
						text: neighborhoodName,
					},
				];
			}
		}

		filterChanged({
			estate_id,
			neighborhood_id,
		});
	};

	const onSearch = (val: string) => {
		setLocationSearch(val);
		if (val && val.length > 0) searchQuery({ variables: { strSearch: val } });
	};

	const [searchQuery, { data, loading }] = useLazyQuery(ALL_LOCATIONS_QUERY);

	return {
		selectedLocations,
		location: locationSearch,
		show: true,
		onSearch,
		onChange,
		searchResults: data,
		searchLoading: loading,
		selectedByUrl: filtersTags.estate_id,
	};
};
