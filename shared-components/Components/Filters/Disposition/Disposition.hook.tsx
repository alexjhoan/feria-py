import { showDispositions } from "../../../Utils/Functions";
import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { useState, useEffect } from "react";
import { useFilters } from "../Filters.hook";

const FRAGMENT_DISPOSITION_OPTIONS = new FragmentDefiner(
	"Filter",
	`
      id
      name
      options
`
);
export const FRAGMENT_DISPOSITION = new FragmentDefiner(
	"Filters",
	`
    disposition {
      ${FRAGMENT_DISPOSITION_OPTIONS.query()}
    }
`
);
export interface DispositionProps {
	labeled?: boolean;
	collapsable?: boolean;
	filterChanged: ({}: any) => void;
}

export const useDisposition = ({ filterChanged }: DispositionProps) => {
	// hook setup
	const { filters } = useFilters();
	const [disposition, setDisposition] = useState(filters?.dispositionID || []);
	const [firstTime, setFirstTime] = useState(true);

	const { data, loading } = useReadFragment(FRAGMENT_DISPOSITION_OPTIONS, "disposition");

	useEffect(() => {
		if (!firstTime) {
			updateFilters();
		} else {
			setFirstTime(false);
		}
	}, [disposition]);

	const updateFilters = () => {
		let filtered = data?.options
			.filter(o => disposition.includes(o["id"]))
			.map(v => {
				return { value: v["id"], text: v["nombre"] };
			});
		filterChanged({ dispositionID: filtered });
	};

	const show = showDispositions(filters?.property_type_id);

	return {
		disposition: {
			set: setDisposition,
			options: data?.options,
			loading: loading,
			value: disposition,
		},
		show,
	};
};
