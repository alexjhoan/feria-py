import { Checkbox, Col, Collapse, Row, Skeleton, Space, Tooltip, Typography } from "antd";
import { DispositionProps, useDisposition } from "./Disposition.hook";

import { DownOutlined } from "@ant-design/icons";
import React from "react";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Panel } = Collapse;

export const Disposition = ({
	labeled = false,
	collapsable = false,
	...props
}: DispositionProps) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const { disposition, show } = useDisposition(props);

	if (!show) return null;

	const handleChecked = checked => disposition.set(checked);

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter"></span>
		</Tooltip>
	);

	const filter = disposition.loading ? (
		<Space style={{ width: "100%" }} size={0} direction={"vertical"}>
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
		</Space>
	) : (
		<Checkbox.Group value={disposition.value} onChange={handleChecked}>
			<Row gutter={[0, theme.spacing.lgSpacing]}>
				{disposition.options?.map(o => (
					<Col span={24} key={`key_${o["id"]}_disposition`}>
						<Checkbox value={o["id"]}>{t(o["nombre"])}</Checkbox>
					</Col>
				))}
			</Row>
		</Checkbox.Group>
	);

	const label = "Disposición";

	return (
		<div className="filter disposition-filter">
			{collapsable ? (
				<Collapse
					ghost
					bordered={false}
					defaultActiveKey={disposition.value.length >= 1 ? "true" : "false"}
					expandIconPosition="right"
					expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
					<Panel
						extra={disposition.value.length >= 1 && extraIcon()}
						disabled={disposition.loading}
						header={t(label)}
						key="true">
						{filter}
					</Panel>
				</Collapse>
			) : (
				<>
					{labeled && (
						<Typography.Title disabled={disposition.loading} level={4}>
							{t(label)}
						</Typography.Title>
					)}
					{filter}
				</>
			)}
		</div>
	);
};
