import "./styles.less";

import {
	Button,
	Col,
	Collapse,
	Input,
	InputNumber,
	Radio,
	Row,
	Skeleton,
	Space,
	Tooltip,
	Typography,
} from "antd";
import { CheckOutlined, DownOutlined } from "@ant-design/icons";
import { SurfaceRangeFilterProps, useSurfaceRange } from "./SurfaceRange.hook";
import { formatMoney, parserMoney } from "../../../Utils/Functions";

import { RadioChangeEvent } from "antd/lib/radio";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Panel } = Collapse;
const { Title } = Typography;

const SurfaceRange = ({
	collapsable = false,
	labeled = false,
	...props
}: SurfaceRangeFilterProps) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const { m2Min, m2Max, m2Type, saveFilter } = useSurfaceRange(props);

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter"></span>
		</Tooltip>
	);

	const opt = m2Type.options?.map(option => {
		return {
			value: option,
			text: option,
		};
	});

	const meterType = m2Type.loading ? (
		<Space style={{ width: "100%" }} size={0} direction={"vertical"}>
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
		</Space>
	) : (
		<Radio.Group
			value={m2Type.value?.value}
			defaultValue={"edificados"}
			onChange={(e: RadioChangeEvent) => {
				m2Type.set({ text: t(e.target.value), value: e.target.value });
			}}
			disabled={m2Type.loading}>
			<Row gutter={[0, theme.spacing.lgSpacing]}>
				{opt.map(o => (
					<Col span={24} key={`key_${o.text}_m2Type`}>
						<Radio value={o.value}>
							{t(o.text.charAt(0).toUpperCase() + o.text.slice(1))}
						</Radio>
					</Col>
				))}
			</Row>
		</Radio.Group>
	);

	const range = (
		<Input.Group>
			<InputNumber
				className="secondary"
				disabled={m2Type.loading}
				placeholder={"Min"}
				value={m2Min.value?.value}
				onChange={e =>
					m2Min.set({
						text: t("Desde") + " " + formatMoney(e) + " m²",
						value: e,
					})
				}
				formatter={value => formatMoney(value)}
				parser={value => parserMoney(value)}
				pattern="\d*"
				onPressEnter={saveFilter}
			/>
			<Input className="input-separate" placeholder="-" disabled />
			<InputNumber
				className="secondary"
				disabled={m2Type.loading}
				placeholder={"Max"}
				value={m2Max.value?.value}
				onChange={e =>
					m2Max.set({
						text: t("Hasta") + " " + formatMoney(e) + " m²",
						value: e,
					})
				}
				formatter={value => formatMoney(value)}
				parser={value => parserMoney(value)}
				pattern="\d*"
				onPressEnter={saveFilter}
			/>
			<Button disabled={m2Type.loading} ghost icon={<CheckOutlined />} onClick={saveFilter} />
		</Input.Group>
	);

	const filter = (
		<Space direction={"vertical"} size="small">
			{meterType}
			{range}
		</Space>
	);

	const label = t("Rango de Metraje");

	return (
		<div className="filter m2Type-filter">
			{collapsable ? (
				<Collapse
					ghost
					bordered={false}
					defaultActiveKey={m2Min.value || m2Max.value ? "true" : "false"}
					expandIconPosition="right"
					expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
					<Panel
						extra={(m2Min?.value?.value > 0 || m2Max?.value?.value > 0) && extraIcon()}
						header={label}
						key={"true"}
						disabled={m2Type.loading}>
						{filter}
					</Panel>
				</Collapse>
			) : (
				<>
					{labeled && (
						<Title disabled={m2Type.loading} level={4}>
							{label}
						</Title>
					)}
					{filter}
				</>
			)}
		</div>
	);
};

export { SurfaceRange };
