import { useState, useEffect } from "react";
import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { useFilters } from "../Filters.hook";

const FRAGMENT_SURFACERANGE_OPTIONS = new FragmentDefiner(
	"Filter",
	`
		id
		name
		options
	`
);

export const FRAGMENT_SURFACERANGE = new FragmentDefiner(
	"Filters",
	`
		surfaceRange {
		${FRAGMENT_SURFACERANGE_OPTIONS.query()}
		}
	`
);
export interface SurfaceRangeFilterProps {
	filterChanged: ({}: any) => void;
	collapsable?: boolean;
	labeled?: boolean;
}

export const useSurfaceRange = ({ filterChanged }: SurfaceRangeFilterProps) => {
	const {
		filtersTags: { m2Min, m2Max, m2Type },
	} = useFilters();

	const { loading, data } = useReadFragment(FRAGMENT_SURFACERANGE_OPTIONS, "surfaceRange");
	const [min, setMin] = useState<{ text; value } | null>(m2Min);
	const [max, setMax] = useState<{ text; value } | null>(m2Max);
	const [m2, setM2] = useState<{ text; value } | null>(m2Type);

	useEffect(() => setMin(m2Min), [m2Min]);
	useEffect(() => setMax(m2Max), [m2Max]);
	useEffect(() => setM2(m2Type), [m2Type]);

	useEffect(() => {
		if (min || max) setSurfaceFilter();
	}, [m2]);

	const setSurfaceFilter = () => {
		filterChanged({
			m2Min: min,
			m2Max: max,
			m2Type: m2,
		});
	};

	return {
		m2Min: { value: min, set: setMin },
		m2Max: { value: max, set: setMax },
		m2Type: { value: m2, set: setM2, options: data?.options, loading },
		saveFilter: setSurfaceFilter,
	};
};
