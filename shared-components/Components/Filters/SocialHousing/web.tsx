import "./styles.less";

import { Switch, Typography } from "antd";

import { useSocialHousing } from "./SocialHousing.hook";
import { useTranslation } from "react-i18next";

export const SocialHousing = props => {
	const {
		show,
		error,
		loading,
		data: { handleClick, checked },
	} = useSocialHousing(props);

	const { t } = useTranslation();

	if (!show) return null;
	if (error) return null;

	const Filter = (
		<>
			<Typography.Text disabled={loading}>{t("Vivienda social")}</Typography.Text>
			<Switch disabled={loading} checked={checked} onChange={handleClick} />
		</>
	);

	return <div className="filter socialhousing-filter">{Filter}</div>;
};
