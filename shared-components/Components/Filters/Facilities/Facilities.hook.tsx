import { showFacilities } from "../../../Utils/Functions";
import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { useState, useEffect } from "react";
import { useFilters } from "../Filters.hook";

export const FRAGMENT_FACILITIES_OPTIONS = new FragmentDefiner(
	"Filter",
	`
      id
      name
      options
	`
);
export const FRAGMENT_FACILITIES = new FragmentDefiner(
	"Filters",
	`
      facilities {
        ${FRAGMENT_FACILITIES_OPTIONS.query()}
      }
	`
);
export interface FacilitiesProps {
	filterChanged: ({}: any) => void;
	labeled?: boolean;
	collapsable?: boolean;
	inputType?: "select" | "checkbox-select";
}

export const useFacilities = ({ filterChanged }: FacilitiesProps) => {
	// hook setup
	const { filters } = useFilters();
	const [facilities, setFacilities] = useState(null);
	const [firstTime, setFirstTime] = useState(true);
	const selected = filters?.facilitiesGroup || [];

	const { data, loading } = useReadFragment(FRAGMENT_FACILITIES_OPTIONS, "facilities");

	useEffect(() => {
		if (!firstTime) {
			updateFilters();
		} else {
			setFirstTime(false);
		}
	}, [facilities]);

	const updateFilters = () => {
		let filtered = facilities?.map(v => {
			return { value: v["id"], text: "con " + v["nombre"] };
		});
		filterChanged({ facilitiesGroup: filtered });
	};

	const show = showFacilities(filters?.property_type_id, filters?.operation_type_id);

	return {
		facilities: {
			value: facilities,
			set: setFacilities,
			options: data?.options,
			loading,
			selected: selected,
		},
		show,
	};
};
