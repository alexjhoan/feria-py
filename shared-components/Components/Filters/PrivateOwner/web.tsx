import "./styles.less";

import { Switch, Typography } from "antd";

import { usePrivateOwner } from "./PrivateOwner.hook";
import { useTranslation } from "react-i18next";

export const PrivateOwner = props => {
	const {
		show,
		error,
		loading,
		data: { handleClick, checked },
	} = usePrivateOwner(props);

	const { t } = useTranslation();

	if (!show) return null;
	if (error) return <div>{t("error")}</div>;

	const filter = (
		<>
			<Typography.Text disabled={loading}>{t("Publicado por el dueño")}</Typography.Text>
			<Switch
				disabled={loading}
				checked={checked}
				onChange={handleClick}
				defaultChecked={checked}
			/>
		</>
	);

	return <div className="filter privateowner-filter">{filter}</div>;
};
