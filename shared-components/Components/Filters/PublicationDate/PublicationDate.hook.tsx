import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { SelectTypes } from "../../Inputs/Selects/Selects.hook";
import { useFilters } from "../Filters.hook";

const FRAGMENT_PUBLICATIONDATES_OPTIONS = new FragmentDefiner(
	"Filter",
	`
		id
		name
		options
	`
);

export const FRAGMENT_PUBLICATIONDATES = new FragmentDefiner(
	"Filters",
	`
		publicationDate {
			${FRAGMENT_PUBLICATIONDATES_OPTIONS.query()}
		}
	`
);

export const usePublicationDate = props => {
	const { filters, changeFilters } = useFilters();
	const {
		labeled = false,
		selectedValue = filters?.publicationDate,
		filterChanged = changeFilters,
	} = props;
	const { loading, data, error } = useReadFragment(
		FRAGMENT_PUBLICATIONDATES_OPTIONS,
		"publicationDate"
	);

	const handleChange = newOperation => {
		const resPublicationDate = newOperation
			? { value: newOperation.value, text: newOperation.text }
			: null;
		filterChanged({ publicationDate: resPublicationDate });
	};

	const type: SelectTypes = "radio";
	return {
		loading,
		error,
		show: true,
		data: {
			options: data?.options,
			onChange: handleChange,
			value: selectedValue,
			customKey: "value",
			valueKey: "text",
			type: type,
			maxCountOptions: 4,
		},
		labeled,
		label: data?.name,
	};
};
