import {
	FragmentDefiner,
	useReadFragment,
} from "../../../GlobalHooks/useReadFragment";
import { useSearchResultsFilters } from "../../../ViewFragments/SearchResults/Sider/Filters/SearchResultsFilters.hook";
import { useFilters } from "../Filters.hook";

const FRAGMENT_ORDERS_OPTIONS = new FragmentDefiner(
	"Filter",
	`id 
   name 
   options`
);

export const FRAGMENT_ORDERS = new FragmentDefiner(
	"Filters",
	` order {
	${FRAGMENT_ORDERS_OPTIONS.query()}
  }`
);

export const useOrder = () => {
	const { filters } = useFilters();
	const { changeFiltersAndUpdateURL } = useSearchResultsFilters();

	const { loading, data, error } = useReadFragment(
		FRAGMENT_ORDERS_OPTIONS,
		"order"
	);

	const handleChange = ({ value, text }) => {
		changeFiltersAndUpdateURL({ order: { value: value, text: text } });
	};

	return {
		loading: loading,
		data: {
			onChange: handleChange,
			options: data?.options,
			value: filters?.order,
			valueKey: "text",
			customKey: "value",
		},
		error: error,
		show: true,
	};
};
