import React from "react";
import { View, Text } from "react-native";
import { useOperationType, OperationTypeProps } from "./OperationType.hook";
import { Label } from "../../Inputs/Label/app";
import { CheckboxSelect } from "../../Inputs/app";

export const OperationType = (props: OperationTypeProps) => {
	const {
		show,
		loading,
		error,
		data,
		labeled,
		label,
		inputType,
	} = useOperationType(props);

	if (!show) return null;
	if (loading) return null;
	if (error) return <Text>Error</Text>;

	return (
		<View>
			<Label showLabel={labeled} labelContent={label}>
				{inputType == "radioselect" ? (
					<CheckboxSelect {...data} />
				) : (
					// <Select {...data} placeholder={label} />
					<Text>@TODO: select component</Text>
				)}
			</Label>
		</View>
	);
};
