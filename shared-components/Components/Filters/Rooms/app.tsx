import { Label } from "../../Inputs/Label/app";
import { ButtonSelect } from "../../Inputs/app";
import { useRooms } from "./Rooms.hook";
import React from "react";
import { View, Text } from "react-native";
import { FilterComponentType } from "../Filters.hook";

const Rooms = (props: FilterComponentType) => {
	const { loading, data, error, show, label, labeled } = useRooms(props);

	if (!show) return null;
	if (loading) return null;
	if (error) return <Text>error</Text>;

	return (
		<View>
			<Label labelContent={label} showLabel={labeled}>
				<ButtonSelect {...data} />
			</Label>
		</View>
	);
};

export { Rooms };
