import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { showRooms } from "../../../Utils/Functions";
import { SelectTypes } from "../../Inputs/Selects/Selects.hook";
import { useFilters } from "../Filters.hook";

const FRAGMENT_ROOMS_OPTIONS = new FragmentDefiner(
	"Filter",
	`
		  id
		  name
		  options
	  `
);

export const FRAGMENT_ROOMS = new FragmentDefiner(
	"Filters",
	`
		  rooms {
		  ${FRAGMENT_ROOMS_OPTIONS.query()}
		  }
	  `
);

export const useRooms = props => {
	const { filters, changeFilters } = useFilters();
	const {
		labeled = false,
		selectedValue = filters?.rooms,
		inputType = "select",
		currentFilters = filters,
		filterChanged = changeFilters,
	} = props;
	const { loading, data, error } = useReadFragment(FRAGMENT_ROOMS_OPTIONS, "rooms");

	let options = [];
	if (data) {
		options = data.options.map((element, _, { length }) => {
			let cant = element.amount;
			return {
				value: element.amount,
				amount: cant + (cant >= length ? "+" : ""),
			};
		});
	}

	const handleChange = newValue => {
		let res = [];
		if (newValue != null) {
			res = newValue.map(v => {
				return {
					value: v["value"],
					text: v["amount"] == 1 ? `${v["amount"]} Ambiente` : `${v["amount"]} Ambientes`,
				};
			});
		}
		filterChanged({ rooms: res });
	};

	const show = showRooms(currentFilters.property_type_id, currentFilters.operation_type_id);
	const type: SelectTypes = "multiple";

	return {
		loading: loading,
		error: error,
		show,
		data: {
			onChange: handleChange,
			value: selectedValue,
			options: options,
			customKey: "value",
			valueKey: "amount",
			type: type,
		},
		label: data?.name,
		labeled,
	};
};
