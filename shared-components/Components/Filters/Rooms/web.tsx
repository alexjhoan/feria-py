import { Checkbox, Col, Collapse, Row, Skeleton, Space, Tooltip, Typography } from "antd";

import { DownOutlined } from "@ant-design/icons";
import React from "react";
import { useRooms } from "./Rooms.hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Panel } = Collapse;

export const Rooms = ({ collapsable = false, ...props }) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const {
		show,
		error,
		loading,
		labeled,
		data: { onChange, options, customKey, valueKey, value = [] },
	} = useRooms(props);

	if (!show) return null;
	if (error) return <div>{t("error")}</div>;

	const handleChange = checked => {
		const res = options.filter(o => checked.includes(o[customKey]));
		onChange(res);
	};

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter"></span>
		</Tooltip>
	);

	const filter = (
		<Checkbox.Group value={value} onChange={handleChange}>
			<Row gutter={[0, theme.spacing.lgSpacing]}>
				{loading ? (
					<Space style={{ width: "100%" }} size={0} direction={"vertical"}>
						<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
						<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
						<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
					</Space>
				) : (
					options?.map(o => (
						<Col span={24} key={`key_${o[customKey]}_rooms`}>
							<Checkbox value={o[customKey]}>
								{o[valueKey] == 1
									? `${o[valueKey]} Ambiente`
									: `${o[valueKey]} Ambientes`}
							</Checkbox>
						</Col>
					))
				)}
			</Row>
		</Checkbox.Group>
	);

	const label = "Ambientes";

	return (
		<div className={"filter rooms-filter"}>
			{collapsable ? (
				<Collapse
					ghost
					bordered={false}
					defaultActiveKey={value?.length >= 1 ? "false" : "true"}
					expandIconPosition="right"
					expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
					<Panel
						extra={value?.length >= 1 && extraIcon()}
						disabled={loading}
						header={t(label)}
						key="true">
						{filter}
					</Panel>
				</Collapse>
			) : (
				<>
					{labeled && (
						<Typography.Title disabled={loading} level={4}>
							{t(label)}
						</Typography.Title>
					)}
					{filter}
				</>
			)}
		</div>
	);
};
