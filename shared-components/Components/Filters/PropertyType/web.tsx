import "./styles.less";

import { Checkbox, Col, Collapse, Row, Select, Skeleton, Space, Tooltip, Typography } from "antd";

import { DownOutlined } from "@ant-design/icons";
import React from "react";
import { usePropertyType } from "./PropertyType.hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Option } = Select;
const { Panel } = Collapse;

export const PropertyType = ({ collapsable = false, ...props }) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const {
		show,
		error,
		loading,
		inputType,
		label,
		labeled,
		data: { options, onChange, value = [], customKey },
	} = usePropertyType(props);

	if (!show) return null;
	if (error) return null;

	const handleChange = checked => {
		const res = options.filter(o => checked.includes(o[customKey]));
		onChange(res);
	};

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter"></span>
		</Tooltip>
	);

	const checkboxFilter = loading ? (
		<Space style={{ width: "100%" }} size={0} direction={"vertical"}>
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
		</Space>
	) : (
		<Checkbox.Group value={value} onChange={handleChange}>
			<Row gutter={[0, theme.spacing.lgSpacing]}>
				{options?.map(o => (
					<Col span={24} key={`key_${o[customKey]}_propertyType`}>
						<Checkbox value={o[customKey]}>{t(o.name)}</Checkbox>
					</Col>
				))}
			</Row>
		</Checkbox.Group>
	);

	const selectFilter = loading ? (
		<Skeleton.Button active className="property-type-skeleton" />
	) : (
		<Select
			disabled={loading}
			value={loading ? t("Tipo de Propiedad") : value}
			mode={"multiple"}
			showArrow
			optionFilterProp="children"
			onChange={handleChange}
			style={{ width: "100%" }}
			placeholder={t("Seleccione un Tipo de Propiedad")}>
			{options?.map(o => (
				<Option key={`name_${o[customKey]}`} value={o[customKey]}>
					{t(o.name)}
				</Option>
			))}
		</Select>
	);

	const filter = inputType == "checkboxselect" ? checkboxFilter : selectFilter;

	return (
		<>
			<div className="filter property-type-filter">
				{collapsable ? (
					<Collapse
						ghost
						bordered={false}
						expandIconPosition="right"
						defaultActiveKey={value.length > 0 ? "false" : "true"}
						expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
						<Panel
							extra={value.length >= 1 && extraIcon()}
							disabled={loading}
							header={t(label)}
							key={"true"}>
							{filter}
						</Panel>
					</Collapse>
				) : (
					<>
						{labeled && (
							<Typography.Title level={4} disabled={loading}>
								{label}
							</Typography.Title>
						)}
						{filter}
					</>
				)}
			</div>
			<style jsx global>{`
				.home-filters .property-type-filter {
					border-right: 1px solid ${theme.colors.borderColor};
				}

				.home-filters .property-type-filter .property-type-skeleton .ant-skeleton-button {
					border-radius: ${theme.spacing.smSpacing}px 0 0 ${theme.spacing.smSpacing}px;
				}

				.home-filters .property-type-filter .ant-select .ant-select-selector {
					border-radius: ${theme.spacing.smSpacing}px 0 0 ${theme.spacing.smSpacing}px;
					text-overflow: ${value.length > 0 ? "ellipsis" : "unset"};
				}

				@media screen and (max-width: ${theme.breakPoints.lg}) {
					.home-filters
						.property-type-filter
						.property-type-skeleton
						.ant-skeleton-button {
						border-radius: ${theme.spacing.smSpacing}px;
					}

					.home-filters .property-type-filter .ant-select .ant-select-selector {
						border-radius: ${theme.spacing.smSpacing}px;
					}
				}
			`}</style>
		</>
	);
};
