import { Button, Col, Collapse, Radio, Row, Skeleton, Space, Tooltip, Typography } from "antd";

import { DownOutlined } from "@ant-design/icons";
import React from "react";
import { useSeaDistance } from "./SeaDistance.hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Panel } = Collapse;

export const SeaDistance = ({ collapsable = false, labeled = false, ...props }) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const {
		show,
		error,
		loading,
		data: { onChange, value, valueKey, options },
	} = useSeaDistance(props);

	if (!show) return null;
	if (error) return <div>{t("error")}</div>;

	const handleRadio = e => {
		let val = e.target.value;
		const res = options.filter(o => o.id == val);
		onChange(res[0]);
	};

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter" style={{ marginTop: "6px" }}></span>
		</Tooltip>
	);

	const filter = loading ? (
		<Space style={{ width: "100%" }} size={0} direction={"vertical"}>
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
		</Space>
	) : (
		<Radio.Group onChange={handleRadio} value={value}>
			<Row gutter={[0, theme.spacing.lgSpacing]}>
				<Col span={24}>
					<Radio value={null}>{t("Cualquier Distancia")}</Radio>
				</Col>
				{options?.map(o => (
					<Col span={24} key={`key_${o.id}seaDistance`}>
						<Radio value={o.id}>{t(o[valueKey])}</Radio>
					</Col>
				))}
			</Row>
		</Radio.Group>
	);

	const label = "Distancia al mar";

	return (
		<div className="filter sea-distance-filter">
			{collapsable ? (
				<Collapse
					ghost
					bordered={false}
					expandIconPosition="right"
					defaultActiveKey={value ? "true" : "false"}
					expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
					<Panel
						extra={value && extraIcon()}
						disabled={loading}
						header={t(label)}
						key="true">
						{filter}
					</Panel>
				</Collapse>
			) : (
				<>
					{labeled && (
						<Typography.Title disabled={loading} level={4}>
							{t(label)}
						</Typography.Title>
					)}
					{filter}
				</>
			)}
		</div>
	);
};
