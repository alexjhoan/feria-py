import { CheckboxSelect } from "../../Inputs/app";
import { useSeaDistance } from "./SeaDistance.hook";
import { Label } from "../../Inputs/Label/app";
import { FilterComponentType } from "../Filters.hook";
import { View } from "react-native";
import React from "react";

const SeaDistance = (props: FilterComponentType) => {
	const { loading, data, error, show, label, labeled } = useSeaDistance(props);

	if (!show) return null;
	if (loading) return null;
	if (error) return <View>error</View>;

	return (
		<View>
			<Label labelContent={label} showLabel={labeled}>
				<CheckboxSelect {...data} />
			</Label>
		</View>
	);
};

export { SeaDistance };
