import { Checkbox, Col, Collapse, Row, Skeleton, Space, Tooltip, Typography } from "antd";

import { DownOutlined } from "@ant-design/icons";
import React from "react";
import { useBathrooms } from "./Bathrooms.hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Panel } = Collapse;

export const Bathrooms = ({ collapsable = false, ...props }) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const {
		show,
		error,
		loading,
		labeled,
		data: { onChange, options, customKey, valueKey, value = [] },
	} = useBathrooms(props);

	if (!show) return null;
	if (error) return <div>{t("error")}</div>;

	const handleChange = checked => {
		const res = options.filter(o => checked.includes(o[customKey]));
		onChange(res);
	};

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter"></span>
		</Tooltip>
	);

	const filter = loading ? (
		<Space style={{ width: "100%" }} size={0} direction={"vertical"}>
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
		</Space>
	) : (
		<Checkbox.Group value={value} onChange={handleChange}>
			<Row gutter={[0, theme.spacing.lgSpacing]}>
				{options?.map(o => (
					<Col span={24} key={`key_${o[customKey]}_bathrooms`}>
						<Checkbox value={o[customKey]}>
							{o[valueKey] == 1 ? `${o[valueKey]} Baño` : `${o[valueKey]} Baños`}
						</Checkbox>
					</Col>
				))}
			</Row>
		</Checkbox.Group>
	);

	const label = "Baños";

	return (
		<div className={"filter bathrooms-filter"}>
			{collapsable ? (
				<Collapse
					ghost
					bordered={false}
					defaultActiveKey={value?.length >= 1 ? "true" : "false"}
					expandIconPosition="right"
					expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
					<Panel
						extra={value?.length >= 1 && extraIcon()}
						disabled={loading}
						header={t(label)}
						key="true">
						{filter}
					</Panel>
				</Collapse>
			) : (
				<>
					{labeled && (
						<Typography.Title disabled={loading} level={4}>
							{t(label)}
						</Typography.Title>
					)}
					{filter}
				</>
			)}
		</div>
	);
};
