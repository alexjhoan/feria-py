import { Collapse, Input, Tooltip, Typography } from "antd";
import { KeywordProps, useKeyword } from "./Keyword.hook";

import { DownOutlined } from "@ant-design/icons";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Search } = Input;
const { Panel } = Collapse;

export const Keyword = ({ collapsable = false, ...props }: KeywordProps) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const { keyword, setKeyWord, handleSubmit, label, labeled } = useKeyword(props);

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter"></span>
		</Tooltip>
	);

	const filter = (
		<Search
			className="secondary"
			value={keyword}
			placeholder={t("Buscar por palabra clave")}
			onChange={e => setKeyWord(e.target.value)}
			onSearch={() => handleSubmit()}
			enterButton
		/>
	);

	return (
		<div className="filter keyword-filter">
			{collapsable ? (
				<Collapse
					ghost
					bordered={false}
					expandIconPosition="right"
					defaultActiveKey={keyword ? "true" : "false"}
					expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
					<Panel extra={keyword && extraIcon()} header={t(label)} key={"true"}>
						{filter}
					</Panel>
				</Collapse>
			) : (
				<>
					{labeled && <Typography.Title level={4}>{label}</Typography.Title>}
					{filter}
				</>
			)}
		</div>
	);
};
