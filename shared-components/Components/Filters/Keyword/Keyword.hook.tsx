import { useState, useEffect } from "react";
import { useFilters } from "../Filters.hook";

export interface KeywordProps {
	filterChanged: ({}: any) => void;
	labeled?: boolean;
	collapsable?: boolean;
}

export const useKeyword = ({ filterChanged, labeled = false }: KeywordProps) => {
	const { filters, changeFilters } = useFilters();
	const [keyword, setKeyWord] = useState<string>(filters.searchstring);
	useEffect(() => setKeyWord(filters.searchstring), [filters.searchstring]);

	const label = "Palabra clave";
	const handleSubmit = () => {
		let searchString = keyword ? { text: label + ": " + keyword, value: keyword } : null;
		filterChanged({
			searchstring: searchString,
		});
	};

	return {
		keyword,
		setKeyWord,
		handleSubmit,
		label,
		labeled: labeled,
	};
};
