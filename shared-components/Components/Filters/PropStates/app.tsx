import React from "react";
import { usePropStates } from "./PropStates.hook";
import { CheckboxSelect } from "../../Inputs/app";
import { Label } from "../../Inputs/Label/app";
import { View, Text } from "react-native";
import { FilterComponentType } from "../Filters.hook";

export const PropStates = (props: FilterComponentType) => {
	const { show, loading, data, error, labeled, label } = usePropStates(props);

	if (!show) return null;
	if (loading) return null;
	if (error) return <Text>Error</Text>;

	return (
		<View>
			<Label labelContent={label} showLabel={labeled}>
				<CheckboxSelect {...data} />
			</Label>
		</View>
	);
};
