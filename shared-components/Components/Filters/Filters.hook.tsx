import React, { useContext } from "react";
import {
	FiltersDispatchContext,
	FiltersStateContext,
} from "../../Contexts/Filters/context";
import { Filters, FiltersValues } from "../../Contexts/Filters/model";
import { isTemporal, isSell, propertiesTypeInTemporal, showBedrooms, showBathrooms, showRooms, showM2, showPropState, isRent, showFloors, showDispositions, showFacilities } from "../../Utils/Functions";

const getOnlyValues = (a): FiltersValues => {
	if (a == null || !a) return null;
	return Object.keys(a).reduce((acc, o) => {
		if (a[o] == null) {
			//acc[o] = null;
		} else if (Array.isArray(a[o])) {
			if (a[o].length > 0) {
				acc[o] = a[o].map(j => j.value);
			} else {
				//acc[o] = [];
			}
		} else if (a[o].value != null) {
			acc[o] = a[o].value;
		} else if (typeof a[o] == "string" && a[o] != ""){
			acc[o] = a[o]
		}
		return acc;
	}, {});

};

const useFilters = () => {
	const filters = useContext(FiltersStateContext);
	const dispatch = useContext(FiltersDispatchContext);

	const changeFilters = (new_filters: Filters) => {
		if("operation_type_id" in new_filters){
			if(isTemporal(new_filters.operation_type_id.value)){
				if(filters.property_type_id){
					new_filters.property_type_id = filters.property_type_id.filter(e => propertiesTypeInTemporal(e.value));
				}
				new_filters.m2Min = null;
				new_filters.m2Max = null;
				new_filters.m2Type = null;
			}

			if(!isTemporal(new_filters.operation_type_id.value)){
				new_filters.season = null;
				new_filters.dateFrom = null;
				new_filters.dateTo = null;
				new_filters.guests = null;
			}

			if(!isRent(new_filters.operation_type_id.value)){
				new_filters.commonExpenses = null;
			}
		}

		if("property_type_id" in new_filters){
			let properties = new_filters.property_type_id.map(e => e.value);
			let operation = filters.operation_type_id.value;

			if(!showBedrooms(properties, operation)){
				new_filters.bedrooms = null;
			}

			if(!showBathrooms(properties, operation)){
				new_filters.bathrooms = null;
			}

			if(!showRooms(properties, operation)){
				new_filters.rooms = null;
			}

			if(!showPropState(properties, operation)){
				new_filters.constStatesID = null;
			}

			if(!showFloors(properties)){
				new_filters.floors = null;
			}

			if(!showDispositions(properties)){
				new_filters.dispositionID = null;
			}

			if(!showFacilities(properties, operation)){
				new_filters.facilitiesGroup = null;
			}
		}

		return dispatch({ type: "merge", payload: new_filters });
	};

	return {
		filters: getOnlyValues(filters),
		filtersTags: filters,
		changeFilters,
	};
};


export { useFilters };
