import React from "react";
import { useGuests } from "./Guests.hook";
import { Label } from "../../Inputs/Label/app";
import { Input } from "../../Inputs/app";
import { View, Text } from "react-native";
import { FilterComponentType } from "../Filters.hook";

const Guests = (props: FilterComponentType) => {
	const { loading, data, error, show, label, labeled } = useGuests(props);

	if (!show) return null;
	if (loading) return <Text>Cargando</Text>;
	if (error) return <Text>Error</Text>;

	return (
		<View>
			<Label labelContent={label} showLabel={labeled}>
				<View>
					<Input {...data} />
				</View>
			</Label>
		</View>
	);
};

export { Guests };
