import Chart from "chart.js";
import { useEffect, useRef, useState } from "react";
import { encodeHashUrl, formatMoney } from "../../../Utils/Functions";
import { useRouter } from "next/router";

export const LineChart = ({ data, lineColor, lineWidth = 2, average = null, title, actualPropId = 0, activeColor, dontClick = false, labels = null, currency = "" }) => {
  const instance = useRef(null);
  const chart = useRef(null);
  const router = useRouter();

  useEffect(() => {
    if(data){
      if(instance.current){
        instance.current.destroy();
      }

      instance.current = new Chart(chart.current, {
        type: 'line',
        data: {
          labels: labels ? labels : data.map(() => title),
          datasets: [
            {
              data: data.map(property => ({
                y: property.value,
                id: property.id
              })),
              fill: false,
              backgroundColor: ({dataIndex}) => {
                if(data[dataIndex]?.id == actualPropId){
                  return activeColor;
                } else {
                  return lineColor;
                }
              },
              borderColor: lineColor,
              borderWidth: lineWidth,
              pointBorderColor: "#ffffff",
              pointBorderWidth: 3,
              pointRadius: 6,
              pointHoverBackgroundColor: activeColor
            },
            {
              data: data.map(() => average),
              fill: false,
              pointRadius: 0,
              backgroundColor: lineColor,
              borderDash: [5, 5],
              pointHoverRadius: 0
            }
          ]
        },
        options: {
          hover: {
            mode: 'nearest'
          },
          legend: {
            display: false
          },
          responsive: true,
          scales: {
            xAxes: [{
              ticks: {
                display: labels ? true : false
              }
            }],
            yAxes: [{
              ticks: {
                maxTicksLimit: 5,
                callback: (dataLabel) => {
                  return `${currency} ${formatMoney(dataLabel)}`;
                }
              }
            }]
          },
          tooltips: {
            callbacks: {
              label: (tooltipItem) => {
                return `${currency} ${formatMoney(tooltipItem.yLabel)}`
              }
            }
          },
          onClick: (event, array) => {
            if(array.length > 0){
              if(array[0]._index >= 0){
                const propData = data[array[0]._index];
                if(propData.url){
                  if(propData.isExternal){
                    window.open(propData.url, '_blank', 'location=yes,height=670,width=620,scrollbars=yes,status=yes')
                  } else {
                    router.push({ pathname: "/propSingle", query: { hashed: encodeHashUrl(propData) }}, "/" + propData.url);
                  }
                }
              }
            }
          }
        }
      });
    }
  }, [data]);

  return (
    <canvas ref={chart} />
  )
}