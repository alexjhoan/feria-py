import "./styles.less";

import { Carousel, Skeleton, Typography } from "antd";

import { CarouselArrow } from "../../Property/LazyImageGallery/CarouselArrow/web";
import { ImagenOptimizada } from "../../../Components/Image/web";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";
import { useWorkProgress } from "./WorkProgress.hook";

interface WorkProgressProps {
	setModalTabOpen?: void;
	setModalOpen?: void;
	id: string;
}

export const WorkProgress = ({ id, setModalTabOpen, setModalOpen }: WorkProgressProps) => {
	const { loading, images, error } = useWorkProgress(id);
	const { t } = useTranslation();
	const { theme } = useTheme();
	const settings = {
		dots: false,
		arrows: true,
		className: "workprogress",
		nextArrow: <CarouselArrow type={"filled"} position="outside" side={"right"} />,
		prevArrow: <CarouselArrow type={"filled"} position="outside" side={"left"} />,
		infinite: true,
		autoplay: false,
		slidesPerRow: 4,
		speed: 300,
		style: {
			height: "100%",
		},
		responsive: [
			{
				breakpoint: Number(
					theme.breakPoints.lg.substring(0, theme.breakPoints.lg.length - 2)
				),
				settings: {
					slidesToShow: 5,
					slidesToScroll: 2,
					infinite: images.length > 5,
				},
			},
			{
				breakpoint: Number(
					theme.breakPoints.xs.substring(0, theme.breakPoints.xs.length - 2)
				),
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: images.length > 2,
				},
			},
		],
	};

	if (error || images.length <= 0) {
		return null;
	}

	return (
		<>
			<Typography.Title level={4}>{t("Avances de obra")}</Typography.Title>
			{loading ? (
				<Skeleton.Image />
			) : (
				<Carousel {...settings}>
					{images.map(({ image }, index) => (
						<ImageComponent
							className={"workprogress-image"}
							height="100%"
							width="100%"
							src={image}
							key={index}
							alt={t("Avances de obra")}
						/>
					))}
				</Carousel>
			)}
		</>
	);
};

// TODO: definir estilos de este componente
const ImageComponent = props => {
	return <ImagenOptimizada {...props} />;
};
