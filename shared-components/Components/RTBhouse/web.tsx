import { useContext } from "react";
import { ConfigStateContext } from "../../Contexts/Configurations/context";

// Info: https://cdn1.infocasas.com.uy/web/5f807e0651675_infocdn__rtbhouse_iframe_codes.pdf
export const RTBhouse = ({
	page,
	data,
}: {
	page?: "home" | "offer" | "listing";
	data?: string | string[]; // ID o IDs de propiedad
}) => {
	const { rtb_id } = useContext(ConfigStateContext);

	const getPage = () => (page ? "_" + page : "");
	const getData = () => {
		if (page == "offer") return "_" + data;
		else if (page == "listing" && typeof data != "string") return "_" + data.join(",");
		if (typeof page == "undefined") return "&amp;ncm=1";
		else return "";
	};

	if (!rtb_id) return null;
	return (
		<iframe
			src={`https://us.creativecdn.com/tags?id=${rtb_id}${getPage()}${getData()}`}
			width="1"
			height="1"
			scrolling="no"
			frameBorder="0"
			style={{ display: "none !important" }}></iframe>
	);
};
