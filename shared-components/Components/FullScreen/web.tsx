import { ReactNode, useState } from "react";
import { WindowEventListener } from "../../GlobalHooks/web/WindowEventListener.hook";

export const FullScreen = ({
	className = "",
	children = null,
	offset = 0,
}: {
	className?: string;
	children?: ReactNode;
	offset?: number;
}) => {
	const [height, setHeight] = useState(
		typeof window != "undefined"
			? window.innerHeight - offset + "px"
			: "calc(100vh - " + offset + "px)"
	);
	WindowEventListener("resize", e => setHeight(window.innerHeight - offset + "px"));
	return (
		<>
			<div className={"fullscreen " + className} style={{ height: height }}>
				{children}
			</div>
			<style jsx>{`
				.fullscreen {
					height: calc(100vh - ${offset}px);
				}
			`}</style>
		</>
	);
};
