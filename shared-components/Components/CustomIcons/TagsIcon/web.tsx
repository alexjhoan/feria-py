import Icon from "@ant-design/icons";

const TagsIconSvg = () => (
	<svg
		width="1em"
		height="1em"
		viewBox="0 0 24 24"
		fill="none"
		xmlns="http://www.w3.org/2000/svg">
		<path
			fillRule="evenodd"
			clipRule="evenodd"
			d="M0 12.908L12.919 0H18.789L23.4443 4.68V10.517L10.5253 23.426L0.00100062 12.908H0ZM16.7716 8.742C17.3011 8.742 17.8089 8.53181 18.1833 8.15768C18.5577 7.78354 18.768 7.27611 18.768 6.747C18.768 6.21789 18.5577 5.71046 18.1833 5.33632C17.8089 4.96219 17.3011 4.752 16.7716 4.752C16.2422 4.752 15.7344 4.96219 15.36 5.33632C14.9856 5.71046 14.7753 6.21789 14.7753 6.747C14.7753 7.27611 14.9856 7.78354 15.36 8.15768C15.7344 8.53181 16.2422 8.742 16.7716 8.742Z"
			fill="#869099"
		/>
	</svg>
);

export const TagsIcon = props => <Icon component={TagsIconSvg} {...props} />;
