import Icon from "@ant-design/icons";

const FlagsIconSvg = () => (
	<svg
		width="1em"
		height="1em"
		viewBox="0 0 24 23"
		fill="none"
		xmlns="http://www.w3.org/2000/svg">
		<path
			fillRule="evenodd"
			clipRule="evenodd"
			d="M5.3367 5.339H23.3342V14.369H5.3357V5.339H5.3367ZM2.66185 2.659V22.609H0V0H23.3342V2.66H2.66185V2.659Z"
			fill="#869099"
		/>
	</svg>
);

export const FlagsIcon = props => <Icon component={FlagsIconSvg} {...props} />;
