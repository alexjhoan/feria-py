import Icon from "@ant-design/icons";

const MapIconSvg = () => (
	<svg
		width="1em"
		height="1em"
		viewBox="0 0 13 13"
		fill="none"
		xmlns="http://www.w3.org/2000/svg">
		<path
			d="M5.09492 4.33435V10.5393L7.85437 8.70047V2.49431L5.09492 4.33374V4.33435ZM3.46992 4.26715L1.625 3.0368V9.24173L3.46992 10.4721V4.26654V4.26715ZM9.47937 2.49554V8.69986L11.375 9.96443V3.75827L9.47937 2.49431V2.49554ZM9.44272 0.517434H9.47937V0.54187L13 2.88896V13L8.66687 10.1117L4.33313 13L0 10.1117V0L4.33313 2.88957L7.85437 0.54187V0.517434H7.89102L8.66687 0L9.44272 0.517434Z"
			fill="currentColor"
		/>
	</svg>
);
export const MapIcon = props => <Icon component={MapIconSvg} {...props} />;
