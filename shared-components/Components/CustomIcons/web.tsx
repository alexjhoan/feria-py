import { ArrowRightIcon } from "./ArrowRightIcon/web";
import { BedIcon } from "./BedIcon/web";
import { ChartsIcon } from "./ChartsIcon/web";
import { FlagsIcon } from "./FlagsIcon/web";
import { HeartIcon } from "./HeartIcon/web";
import { LeftArrowIcon } from "./LeftArrowIcon/web";
import { LocationIcon } from "./LocationIcon/web";
import { LocationOutlinedIcon } from "./LocationOutlineIcon/web";
import { MapIcon } from "./MapIcon/web";
import { OrderIcon } from "./OrderIcon/web";
import { RightArrowIcon } from "./RightArrowIcon/web";
import { SinkIcon } from "./SinkIcon/web";
import { SquareM2Icon } from "./SquareM2Icon/web";
import { TagsIcon } from "./TagsIcon/web";

export {
	BedIcon,
	HeartIcon,
	LocationIcon,
	LocationOutlinedIcon,
	MapIcon,
	OrderIcon,
	SinkIcon,
	SquareM2Icon,
	TagsIcon,
	ChartsIcon,
	FlagsIcon,
	ArrowRightIcon,
	RightArrowIcon,
	LeftArrowIcon,
};
