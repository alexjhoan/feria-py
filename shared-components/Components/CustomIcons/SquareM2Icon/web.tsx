import Icon from "@ant-design/icons";

const SquareM2Svg = () => (
	<svg
		width="1em"
		height="1em"
		viewBox="0 0 17 16"
		fill="none"
		xmlns="http://www.w3.org/2000/svg">
		<path
			d="M6.47039 11.997L4.14321 9.66842V11.997H6.47039ZM16.1403 15.997H4.14321V16H0.140436V15.997H0.12915V11.997H0.140436V0L16.1403 15.997Z"
			fill="currentColor"
		/>
	</svg>
);
export const SquareM2Icon = props => <Icon component={SquareM2Svg} {...props} />;
