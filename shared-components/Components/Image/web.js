import Measure from "react-measure";
import { useImage } from "./Image.hook";
import { useState } from "react";

const ImagenOptimizada = props => {
	const [dimensions, setdDimensions] = useState({
		width: 1,
		height: 1,
	});

	const [errored, setErrored] = useState(false);

	const { getImagenOptimizada } = useImage();

	return (
		<Measure
			bounds
			onResize={contentRect => {
				//only first time rendering
				if (dimensions.width <= 1) {
					setdDimensions(contentRect.bounds);
				}
			}}>
			{({ measureRef }) => {
				const white_pixel =
					"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mP8zwMAAhABDcCVldsAAAAASUVORK5CYII=";

				const width = String(
					Math.round(Math.max(props.minWidth ? props.minWidth : 1, dimensions.width))
				);

				const height = String(
					Math.round(Math.max(props.minHeight ? props.minHeight : 1, dimensions.height))
				);

				const src = getImagenOptimizada({
					src: props.src,
					width: width,
					height: height,
					resize: props.resize,
				});

				const imgProps = {
					src: dimensions.width > 1 ? src : white_pixel,
					alt: dimensions.width > 1 ? props.alt : "",
				};

				for (const i in props) {
					if (["className", "width", "height"].includes(i)) {
						imgProps[i] = props[i];
					}
				}

				if (errored) {
					imgProps.src =
						"https://tutaki.org.nz/wp-content/uploads/2019/04/no-image-1.png";
				}

				return (
					<>
						{props.backDrop && (
							<div
								className="back-drop"
								style={{ backgroundImage: `url(${imgProps.src})` }}
							/>
						)}
						<img
							ref={measureRef}
							{...imgProps}
							onError={() =>
								!errored && setErrored({ ...dimensions, ...errored, errored: true })
							}
						/>
						<style jsx>{`
							.back-drop {
								width: 100%;
								height: 100%;
								position: absolute;
								top: 0;
								left: 0;
								z-index: 1;
								filter: blur(8px) opacity(95%);
								-webkit-filter: blur(8px) opacity(95%);
							}
							img {
								position: relative;
								z-index: ${props.backDrop ? 2 : 0};
							}
						`}</style>
					</>
				);
			}}
		</Measure>
	);
};

export { ImagenOptimizada };
