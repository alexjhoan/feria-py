export interface GetImagenOptimizadaProps {
	src: String;
	width?: Number;
	height?: Number;
	resize?: String;
}

export const useImage = () => {
	const getImagenOptimizada = ({ src, width, height, resize }: GetImagenOptimizadaProps) => {
		const d = width + "x" + height;
		const resizes = resize ? resize : "outside";
		let res = String(src);
		res = res.replace("cdn1.", "cdn2.");
		res = res.replace("repo/img/", "repo/img/" + "th." + resizes + d + ".");
		res = res.replace("web/", "web/" + "th." + resizes + d + ".");

		return res;
	};

	return { getImagenOptimizada };
};
