export interface BigData {
    id: string
    searchId: string
    created_at: Date
    count_results: number
    avg_price_m2: number
    avg_price_m2_text?: string
    avg_price: number
    avg_price_text?: string
    avg_time_in_market: number
    avg_price_offset?: number
    avg_price_m2_offset?: number
    avg_time_in_market_offset?: number
}