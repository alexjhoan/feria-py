import "./styles.less";

import { Collapse, Empty, Skeleton, Space, Typography } from "antd";
import {
	DingtalkCircleFilled,
	DownOutlined,
	SlidersFilled,
	TagsFilled,
	UpOutlined,
} from "@ant-design/icons";
import React, { useState } from "react";

import { ChartLine } from "../../Charts/web";
import { useBigDataSearchResult } from "./hook";
import { useFilters } from "../../Filters/Filters.hook";
import { useTranslation } from "react-i18next";
import { useTheme } from "../../../Styles/ThemeHook";
import { LineChart } from "../../Charts/LineChart/web";
const { Text, Title } = Typography;

const CustomPanelHeader = ({ icon, title, active }) => {
	return (
		<Space size={"middle"}>
			<Text>{icon}</Text>
			<Text>{title}</Text>
			<Text>{active ? <UpOutlined /> : <DownOutlined />}</Text>
		</Space>
	);
};

export const BigDataSearchResults = () => {
	const { filters, filtersTags } = useFilters();
	const op =
		filtersTags && filtersTags.operation_type_id ? filtersTags.operation_type_id.text : false;

	const { bigData, bigDataMonthly, quantity, setQuantity } = useBigDataSearchResult({
		searchFilters: filters,
	});

	const [activePanels, setActivePanels] = useState([]);
	const { t } = useTranslation();
	const { theme } = useTheme();

	const onChange = panels => {
		if (quantity == 1) {
			setQuantity(12);
		}
		setActivePanels(panels);
	};

	if(bigData.data == null){
		return null;
	}

	const chartLabels =
		bigDataMonthly.data && bigDataMonthly.data.length > 0
			? bigDataMonthly.data.map(o => {
					const d = new Date(o.created_at);
					return t("MONTH_" + d.getMonth() + "_SHORT");
			  })
			: [];

	return (
		<>
			<Title level={3}>{t(`Indicadores estadísticos para esta búsqueda`)}</Title>
			{bigData.loading ? (
				<Skeleton active />
			) : (
				<Collapse
					ghost
					activeKey={activePanels}
					onChange={x => onChange(x)}
					className="search-big-data">
					<Collapse.Panel
						header={
							<CustomPanelHeader
								icon={<SlidersFilled />}
								title={
									t("El precio promedio por m² es de") +
									` ${bigData.data?.avg_price_m2_text}/m²`
								}
								active={activePanels.includes("1")}
							/>
						}
						key={"1"}
						showArrow={false}>
						{bigDataMonthly.loading && <Skeleton active />}
						{!bigDataMonthly.loading && bigDataMonthly.data.length < 2 && (
							<Empty
								description={t("No hay suficientes datos para mostrar la gráfica.")}
							/>
						)}
						{!bigDataMonthly.loading && bigDataMonthly.data.length > 2 && (
							/*
							<ChartLine
								className={"graphic_avg_price_m2"}
								labels={chartLabels}
								datasets={[
									{
										name: t("Precio por m²"),
										values: bigDataMonthly.data.map(o => o.avg_price_m2),
									},
								]}
								stroke={{ width: 4, color: theme.colors.secondaryColor }}
								height={250}
								currency={bigDataMonthly.currency}
							/>
							*/
							<LineChart 
								data={bigDataMonthly.data.map(o => ({value: o.avg_price_m2}))}
								lineColor={theme.colors.secondaryColor}
								title="Precio por m²"
								activeColor={theme.colors.primaryColor}
								dontClick
								labels={chartLabels}
								currency="U$S"
							/>
						)}
					</Collapse.Panel>
					{!!op && (
						<Collapse.Panel
							header={
								<CustomPanelHeader
									icon={<TagsFilled />}
									title={
										t(
											`El precio promedio de ${String(
												op
											).toLowerCase()} es de`
										) + ` ${bigData.data?.avg_price_text}`
									}
									active={activePanels.includes("2")}
								/>
							}
							key={"2"}
							showArrow={false}>
							{bigDataMonthly.loading && <Skeleton active />}
							{!bigDataMonthly.loading && bigDataMonthly.data.length < 2 && (
								<Empty
									description={t(
										"No hay suficientes datos para mostrar la gráfica."
									)}
								/>
							)}
							{!bigDataMonthly.loading && bigDataMonthly.data.length > 2 && (
								/*								
								<ChartLine
									className={"graphic_avg_price"}
									labels={chartLabels}
									datasets={[
										{
											name: t(`Precio de ${String(op).toLowerCase()}`),
											values: bigDataMonthly.data.map(o => o.avg_price),
										},
									]}
									stroke={{ width: 4, color: theme.colors.secondaryColor }}
									height={250}
									currency={bigDataMonthly.currency}
								/>
								*/
								<LineChart 
									data={bigDataMonthly.data.map(o => ({value: o.avg_price}))}
									lineColor={theme.colors.secondaryColor}
									title="Precio promedio"
									activeColor={theme.colors.primaryColor}
									dontClick
									labels={chartLabels}
									currency="U$S"
								/>
							)}
						</Collapse.Panel>
					)}
					<Collapse.Panel
						header={
							<CustomPanelHeader
								icon={<DingtalkCircleFilled />}
								title={
									t(`En promedio los inmuebles están publicados durante`) +
									` ${Math.round(bigData.data?.avg_time_in_market / 30)} ` +
									t(`meses`)
								}
								active={activePanels.includes("3")}
							/>
						}
						key={"3"}
						showArrow={false}>
						{bigDataMonthly.loading && <Skeleton active />}
						{!bigDataMonthly.loading && bigDataMonthly.data.length < 2 && (
							<Empty
								description={t("No hay suficientes datos para mostrar la gráfica.")}
							/>
						)}
						{!bigDataMonthly.loading && bigDataMonthly.data.length > 2 && (
							/*
							<ChartLine
								className={"graphic_avg_time_in_market"}
								labels={chartLabels}
								datasets={[
									{
										name: t("Meses publicado"),
										values: bigDataMonthly.data.map(o =>
											Math.round(o.avg_time_in_market / 30)
										),
									},
								]}
								stroke={{ width: 4, color: theme.colors.secondaryColor }}
								height={250}
								currency={bigDataMonthly.currency}
							/>
							*/
							<LineChart 
								data={bigDataMonthly.data.map(o => ({value: Math.round(o.avg_time_in_market/30)}))}
								lineColor={theme.colors.secondaryColor}
								title="Tiempo Promedio"
								activeColor={theme.colors.primaryColor}
								dontClick
								labels={chartLabels}
							/>
						)}
					</Collapse.Panel>
				</Collapse>
			)}
		</>
	);
};
