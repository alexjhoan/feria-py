import React, { useEffect } from "react";
import Console from "../../../../lib/console";
import getConfig from "next/config";
import { useAuthCheck } from "../useAuthCheck";
import { useUser } from "../User.hook";
import cookie from "js-cookie";
import Head from "next/head";
const { googleClientID } = getConfig().publicRuntimeConfig;

export const OneTapSignIn = () => {
	const { isLoggedIn, recheck } = useAuthCheck();
	const { oneTapSignIn } = useUser();

	const handleResponse = response => oneTapSignIn.send({ token: response.credential });

	useEffect(() => {
		if (!isLoggedIn && oneTapSignIn.response.data && !oneTapSignIn.response.loading) {
			cookie.set("frontend_token", oneTapSignIn.response.data.googleOneTapLogin.access_token);
			localStorage.setItem("user_md5", oneTapSignIn.response.data.googleOneTapLogin.user_md5);
			localStorage.setItem("one_tap_logged", "true");
			recheck();
		}
	}, [oneTapSignIn.response]);

	useEffect(() => {
		// @ts-ignore
		const google = window.google;
		if (!isLoggedIn && typeof google != "undefined") {
			const alreadyOneTapLogged = localStorage.getItem("one_tap_logged");

			google.accounts.id.initialize({
				client_id: googleClientID,
				callback: handleResponse,

				auto_select:
					typeof alreadyOneTapLogged != "undefined" && alreadyOneTapLogged == "true",
			});
			google.accounts.id.prompt(n => {
				Console.log("Debug OneTap - " + n.getNotDisplayedReason());
			});
		}
	}, [isLoggedIn]);

	return (
		<React.Fragment>
			<Head>
				<meta name="google-signin-client_id" content={googleClientID} />
				<meta
					name="google-signin-scope"
					content="https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email"
				/>
				<meta name="google-signin-cookiepolicy" content="single_host_origin" />
			</Head>
			<div id="one-tap-container"> </div>
			<style jsx global>{`
				#credential_picker_container {
					z-index: 100 !important;
				}
			`}</style>
		</React.Fragment>
	);
};
