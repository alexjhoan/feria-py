import "./styles.less";

import { Button, Col, List, Row, Typography } from "antd";

import { Notification } from "./Notification/web";
import React from "react";
import { useNotifications } from "./hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

export const NotificationList = () => {
	const {
		unread_notifications,
		notifications,
		loadingAll,
		loadingUser,
		setAllNotificationSeen,
	} = useNotifications();
	const { theme } = useTheme();
	const { t } = useTranslation();
	return (
		<>
			<List
				className="notification-list"
				itemLayout="horizontal"
				dataSource={notifications}
				header={
					<Row gutter={theme.spacing.mdSpacing}>
						<Col>
							<Typography.Text strong>{t("Notificaciones")}</Typography.Text>
						</Col>
						<Col>
							<Button
								disabled={unread_notifications == 0}
								className="secondary xsmall"
								size={"small"}
								onClick={() => setAllNotificationSeen()}
								children={t("Marcar todas como leídas")}
								loading={loadingAll || loadingUser}
							/>
						</Col>
					</Row>
				}
				footer={
					<Button
						size={"small"}
						className="secondary"
						href="/notificaciones"
						children={t("Ver más notificiaciones")}
					/>
				}
				renderItem={item => {
					return (
						<List.Item>
							<Notification data={item} />
						</List.Item>
					);
				}}
			/>
			<style jsx global>{`
				.notification-list .ant-list-header,
				.notification-list .ant-list-footer {
					padding: ${theme.spacing.smSpacing}px ${theme.spacing.mdSpacing}px;
				}
				.notification-list {
					font-size: ${theme.fontSizes.smFontSize};
					line-height: ${theme.fontSizes.xsTitle};
				}
				.notification-list .ant-list-item {
					padding: ${theme.spacing.mdSpacing}px;
				}
				.notification-list .xsmall {
					font-size: ${theme.fontSizes.xsFontSize};
					line-height: ${theme.fontSizes.baseFontSize};
				}
				.notification-list .ant-list-footer .ant-btn {
					width: 100%;
					font-size: ${theme.fontSizes.smFontSize};
				}
				.userbox-mobile .notification-list .ant-typography:not(.ant-typography-secondary) {
					color: ${theme.colors.backgroundColor};
				}
				.userbox-mobile .notification-list .ant-list-item {
					border-color: ${theme.colors.borderColor}21;
				}
				.userbox-mobile .notification-list .ant-btn {
					background: ${theme.colors.borderColor}21;
					color: ${theme.colors.backgroundColor};
				}
				.userbox-mobile .notification-list .ant-btn:disabled {
					opacity: 0.5;
				}
				.userbox-mobile .notification-list .ant-btn:not(disabled):hover {
					background: ${theme.colors.backgroundColorAternative};
					color: ${theme.colors.backgroundMenuColor};
					border-color: ${theme.colors.backgroundColorAternative};
				}
			`}</style>
		</>
	);
};
