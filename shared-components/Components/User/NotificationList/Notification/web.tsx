import { Row, Col, Typography, Badge, Tooltip } from "antd";
import { HomeFilled, LoadingOutlined } from "@ant-design/icons";
import Avatar from "antd/lib/avatar/avatar";
import React from "react";
import { useTheme } from "../../../../Styles/ThemeHook";
import { useNotifications } from "../hook";
import "./styles.less";
import { useTranslation } from "react-i18next";

export const Notification = ({ data: item }) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const { changeNotificationStatus, loadingChange, loadingUser } = useNotifications();
	return (
		<>
			<a href={item.url} className={"notification"}>
				<Row
					style={{ flexWrap: "nowrap" }}
					align={"middle"}
					justify={"space-between"}
					gutter={theme.spacing.mdSpacing}>
					<Col>
						<Avatar
							src={item.image}
							shape={"square"}
							size={"large"}
							icon={<HomeFilled />}
						/>
					</Col>
					<Col flex={1}>
						<Typography.Paragraph
							ellipsis={{ rows: 3 }}
							style={{
								margin: 0,
							}}>
							<span
								dangerouslySetInnerHTML={{
									__html: item.text,
								}}
							/>
						</Typography.Paragraph>
						<Typography.Text type={"secondary"} className="xsmall">
							{item.created_at}
						</Typography.Text>
					</Col>
					<Col>
						<div
							className={"badge-seen"}
							onClick={e => {
								e.stopPropagation();
								e.preventDefault();
								changeNotificationStatus(item.id, !item.seen);
							}}>
							{loadingChange || loadingUser ? (
								<LoadingOutlined />
							) : (
								<Tooltip
									title={
										item.seen
											? t("Marcar como leída")
											: t("Marcar como no leída")
									}
									placement={"left"}>
									<Badge
										dot
										color={
											item.seen
												? theme.colors.borderColor
												: theme.colors.primaryColor
										}
									/>
								</Tooltip>
							)}
						</div>
					</Col>
				</Row>
			</a>
			<style jsx global>{`
				.notification .xsmall {
					font-size: ${theme.fontSizes.xsFontSize};
					line-height: ${theme.fontSizes.baseFontSize};
				}
			`}</style>
		</>
	);
};
