import React, { useState, useEffect } from 'react'
import { useUser } from '../User.hook'

export const useAvatar = () => {
    const { user: { data: {me:userData} } } = useUser()
    const [initials, setInitials] = useState<string | null>(null)

    const getInitials = (name:string):string => {
        name = name.replace(/ +(?= )/g,'')
        name = name.trim()
        let words:string[] = name.split(" ")
        let output:string = words.length == 1? words[0][0] : words[0][0] + words[1][0] 
        return output.toUpperCase()
    }

    useEffect(() => {  
        let initials = getInitials(userData.name)
        setInitials(initials)
    }, [userData])

    return {
        user: userData || null,
        initials: initials
    }
}