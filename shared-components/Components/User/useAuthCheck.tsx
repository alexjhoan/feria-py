import gql from "graphql-tag"
import { useQuery, useLazyQuery } from "@apollo/client"
import { useEffect } from "react"


const AUTH_CHECK_QUERY = gql`{
	authCheck {
		id
		isLoggedIn
    	__typename
	}
}`

export const useAuthCheck = () => {
	
	const {data, refetch} = useQuery(AUTH_CHECK_QUERY)

	return {
		isLoggedIn: data?.authCheck.isLoggedIn,
		recheck: refetch
	}
}