import React from "react";
import { Input, Button } from "../../Inputs/web";
import { useUser } from "../User.hook";
import cookie from "js-cookie";
import { Logout } from "../LogOut/web";
import { useEffect } from "react";
import { useAuthCheck } from "../useAuthCheck";

export const Register = ({onOk = () => {}, setModalType = null} : {onOk? : Function, setModalType: Function}) => {
	const {
		user,
		register: { send, response },
		userInput: input,
		validationErrors,
		isLoggedIn,
	} = useUser();

	const {recheck} = useAuthCheck();

	const loading = response.loading ? <span>Loading...</span> : <></>;

	useEffect(() => {
		if (response.data && !response.loading) {
			cookie.set("frontend_token", response.data.registerIndividual.access_token);
			localStorage.setItem("user_md5", response.data.registerIndividual.user_md5);
			recheck();
			onOk();
		}
	}, [response]);

	return (
		<>
			{isLoggedIn ? (
				<div>
					<span>Welcome {user && user.data ? user.data.me.email : ""}</span>
					<Logout />
				</div>
			) : (
				<>
					<Button handleClick={() => setModalType("LogIn")} content={"LOGIN EN VEZ DE REGISTRO"} />
					<form
						className="loginForm"
						onSubmit={e => {
							e.preventDefault();
							e.stopPropagation();
							send();
						}}>
						<Input
							placeholder="Email"
							onChange={email => input.set({ ...input.value, email: email })}
							defaultValue={input.value.email}
							type="email"
							required={true}
						/>
						<Input
							placeholder="Password"
							onChange={pass => input.set({ ...input.value, password: pass })}
							defaultValue={input.value.password}
							type="password"
							required={true}
						/>
						<Button content="Registrarse" type="primary" handleClick={() => {}} />

						{validationErrors.map((err, i) => (
							<p key={"err" + i}>
								<small>{err}</small>
							</p>
						))}
						{loading}
					</form>
				</>
			)}
		</>
	);
};
