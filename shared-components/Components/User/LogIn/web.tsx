import "./styles.less";

import { Button, Col, Divider, Form, Input, Row, Space, Typography } from "antd";
import { useEffect, useState } from "react";

import { Brand } from "../../Logo/web";
import { ButtonType } from "antd/lib/button";
import { FacebookLogin } from "./FacebookLogin/web";
import { GoogleLogin } from "./GoogleLogin/web";
import cookie from "js-cookie";
import { useAuthCheck } from "../useAuthCheck";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";
import { useUser } from "../User.hook";

type LoginStepType = "first" | "login" | "register" | "password" | "passwordSuccess";

const Login = ({ onOk = () => {}, email }: { onOk?: Function; email?: string }) => {
	const { theme } = useTheme();
	const {
		login,
		register,
		userExists,
		forgotPassword,
		userInput: input,
		validationErrors: { errors, set: setErrors },
		socialLogin,
	} = useUser();

	const { t } = useTranslation();
	const { recheck } = useAuthCheck();
	const [step, setStep] = useState<LoginStepType>("first");

	const callbackLogin = data => {
		cookie.set("frontend_token", data.access_token);
		localStorage.setItem("user_md5", data.user_md5);
		recheck();
		onOk(data);
	};

	useEffect(() => {
		if (email) {
			input.set({ ...input.value, email: email });
			setStep("login");
		}
	}, []);

	useEffect(() => {
		if (login.response.data && !login.response.loading) {
			callbackLogin(login.response.data.login);
		} else if (register.response.data && !register.response.loading) {
			callbackLogin(register.response.data.registerIndividual);
		} else if (socialLogin.response.data && !socialLogin.response.loading) {
			callbackLogin(socialLogin.response.data.socialLogin);
		} else if (forgotPassword.response.data && !forgotPassword.response.loading) {
			setStep("passwordSuccess");
		}
	}, [login.response, register.response, socialLogin.response, forgotPassword.response]);

	useEffect(() => {
		if (!userExists.response.loading) {
			if (userExists.response.data) setStep("login");
			else if (
				userExists.response.error &&
				userExists.response.error?.graphQLErrors[0].extensions.category == "NotFound"
			)
				setStep("register");
		}
	}, [userExists.response]);

	useEffect(() => setErrors([]), [step]);

	const buttonAction = () => {
		if (step == "first") userExists.send();
		else if (step == "login") login.send();
		else if (step == "register") register.send();
		else if (step == "password") forgotPassword.send();
		else if (step == "passwordSuccess") onOk();
	};

	const Title = () => {
		let res = "Ingresa o Registrate para ver tu actividad";
		if (step == "login") res = "¡Bienvenido! Ingresá tu contraseña para continuar";
		else if (step == "register") res = "Crea una contraseña para continuar";
		else if (step == "password")
			res = "¿Olvidaste tu contraseña? Ingresá tu correo electronico para reiniciarla";
		else if (step == "passwordSuccess")
			res = "¡Gracias! Revisá tu correo electrónico para reiniciar tu contraseña.";

		return <Typography.Title level={4}>{t(res)}</Typography.Title>;
	};

	const EmailInput = (
		<Form.Item
			name={"email"}
			label={t("Correo electrónico")}
			rules={[
				{ required: true, message: t("Por favor ingrese un correo") },
				{
					pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
					message: t("Por favor ingrese un correo válido"),
				},
			]}>
			<Input
				className="input-email"
				placeholder={t("Ingresa tu correo electrónico")}
				value={input.value.email}
				onChange={e => input.set({ ...input.value, email: e.target.value })}
				// autoFocus
			/>
		</Form.Item>
	);

	const PasswordInput = (
		<Form.Item
			name={"password"}
			label={t("Contraseña")}
			rules={[
				{ required: true, message: t("Ingrese una contraseña") },
				{
					min: 8,
					message: t("Las contraseñas deben de tener al menos 8 letras"),
				},
				{
					max: 15,
					message: t("Las contraseñas deben de tener menos de 15 letras"),
				},
			]}>
			<Input.Password
				className="input-password"
				placeholder={t("Ingresa una contraseña, de 8 a 15 letras")}
				value={input.value.password}
				autoFocus
				onChange={e => input.set({ ...input.value, password: e.target.value })}
			/>
		</Form.Item>
	);

	const FormButton = () => {
		let text = "Continuar";
		let type: ButtonType = step == "passwordSuccess" ? "default" : "primary";
		if (step == "login") text = "Enviar";
		else if (step == "register") text = "Guardar Contraseña";
		else if (step == "password") text = "Enviar";
		else if (step == "passwordSuccess") text = "Cerrar";

		return (
			<Button
				className="btn-access"
				type={type}
				htmlType="submit"
				loading={
					login.response.loading ||
					register.response.loading ||
					userExists.response.loading
				}>
				{text}
			</Button>
		);
	};

	const errorMessage = (
		<>
			{errors.map((o, i) => (
				<Typography.Text type="danger" key={"error" + i}>
					{t(o)}
				</Typography.Text>
			))}
		</>
	);

	const GoogleButton = <GoogleLogin onClick={response => socialLogin.send(response)} />;

	const FacebookButton = <FacebookLogin onClick={response => socialLogin.send(response)} />;

	const AgentButton = (
		<Space direction={"vertical"} size={0}>
			<Typography.Text className="modal_footer_text">
				{t("¿Sos una inmobiliaria?")}
			</Typography.Text>
			<Typography.Link className="modal_footer_link" href={"/soyinmobiliaria"} strong>
				{t("Regístrate aquí")}
			</Typography.Link>
		</Space>
	);

	const PassButton = (
		<Space direction={"vertical"} size={0}>
			<Typography.Text>{t("¿Olvidaste tu contraseña?")}</Typography.Text>
			<Typography.Link onClick={() => setStep("password")} strong>
				{t("Ingresa aquí")}
			</Typography.Link>
		</Space>
	);

	const BackLoginButton = (
		<Space direction={"vertical"} size={0}>
			<Typography.Text>{t("¿Recordas tu contraseña?")}</Typography.Text>
			<Typography.Link onClick={() => setStep("login")} strong>
				{t("Ingresa aquí")}
			</Typography.Link>
		</Space>
	);

	const DoLaterButton = (
		<Typography.Link strong onClick={() => register.doLater()}>
			{t("Hacer esto despúes")}
		</Typography.Link>
	);

	const SuccesPasswordText = (
		<Typography.Text>{forgotPassword.response.data?.forgotPassword}</Typography.Text>
	);

	return (
		<>
			<Row justify={"center"} gutter={[0, 0]} className="auth-content">
				<Col className="brand login-space">
					<Brand />
				</Col>

				<Col className="title" span={24}>
					<Title />
				</Col>

				{step == "passwordSuccess" && <Col span={24}>{SuccesPasswordText}</Col>}

				<Col span={24}>
					<Form
						className="auth-form"
						layout={"vertical"}
						hideRequiredMark
						onFinish={buttonAction}>
						{errors.length > 0 && errorMessage}
						{(step == "first" || step == "password") && EmailInput}
						{(step == "login" || step == "register") && PasswordInput}
						<FormButton />
					</Form>
				</Col>

				{step == "first" && <Divider plain>O</Divider>}
				{step == "first" && (
					<Col span={24} className="login-space">
						{FacebookButton}
					</Col>
				)}
				{step == "first" && <Col span={24}>{GoogleButton}</Col>}

				{step != "passwordSuccess" && <Divider />}
				<Col span={24} className="bottom-content">
					{step == "first" && AgentButton}
					{step == "login" && PassButton}
					{step == "password" && BackLoginButton}
					{step == "register" && DoLaterButton}
				</Col>
			</Row>
			<style jsx global>{`
				.auth-modal .ant-modal-content .ant-modal-close {
					color: ${theme.colors.textInverseColor};
				}
				.auth-modal .ant-modal-content .ant-modal-close svg:hover {
					color: ${theme.colors.textColor};
				}
				.auth-modal .title,
				.auth-modal .auth-form .ant-form-item-label {
					padding-bottom: ${theme.spacing.smSpacing}px;
				}
				.auth-modal .auth-form .ant-form-item-label label {
					font-size: ${theme.fontSizes.xsFontSize};
					color: ${theme.colors.textSecondaryColor};
				}
				.auth-modal .auth-form .input-email,
				.auth-modal .auth-form .input-password,
				.auth-modal .auth-form .input-password input {
					font-size: ${theme.fontSizes.smFontSize};
				}
				.auth-modal .auth-form .ant-form-item-explain {
					margin-top: 6px;
					font-size: ${theme.fontSizes.xsFontSize};
				}
				.auth-modal .ant-divider-horizontal {
					margin: ${theme.spacing.xlSpacing}px 0;
					color: ${theme.colors.borderColor};
				}
				.auth-modal .ant-divider-horizontal .ant-divider-inner-text {
					color: ${theme.colors.textInverseColor};
				}
				.auth-modal .ant-btn {
					border-radius: ${theme.spacing.smSpacing}px;
				}
				.auth-modal .btn-access {
					margin-top: ${theme.spacing.xlSpacing}px;
				}
				.auth-modal .btn-access:hover {
					color: ${theme.colors.primaryColor};
					background: ${theme.colors.backgroundColor};
					border-color: ${theme.colors.primaryColor};
				}
				.auth-modal .ant-space-item a.ant-typography,
				.auth-modal .ant-space-item .ant-typography a {
					color: ${theme.colors.secondaryColor};
				}
				.auth-modal .ant-space-item .ant-typography {
					font-size: ${theme.fontSizes.smFontSize};
					line-height: ${theme.fontSizes.baseFontSize};
				}
				.auth-modal .btn-facebook:hover,
				.auth-modal .btn-facebook:focus {
					color: ${theme.colors.textColor};
				}
				.auth-modal .login-space {
					margin-bottom: ${theme.spacing.xlSpacing}px;
				}
				.auth-modal .ant-modal-content .ant-modal-body .modal-auth-container {
					padding: ${theme.spacing.xxlSpacing * 2}px;
				}
				@media screen and (max-width: ${theme.breakPoints.md}) {
					.auth-modal .ant-modal-content .ant-modal-body .modal-auth-container {
						padding: ${theme.spacing.lgSpacing}px ${theme.spacing.xlSpacing}px;
					}
				}
			`}</style>
		</>
	);
};

export { Login };
