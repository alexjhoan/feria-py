import { useState } from "react";
import { Button, Input } from "antd";
import { FacebookFilled } from "@ant-design/icons";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

import { useTranslation } from "react-i18next";

import getConfig from "next/config";
const { facebookAppID } = getConfig().publicRuntimeConfig;

const FacebookLoginButton = ({ onClick }: { onClick: Function }) => {
	const [token, setToken] = useState(null);
	const { t } = useTranslation();
	const responseFacebook = response => {
		if (response.status == "unknown") return false;
		if (!("email" in response) || response.email === "undefined" || response.email === "") {
			// el usuario no nos devolvio el mail en FB (caso guille)
			setToken(response.accessToken);
		} else {
			onClick({
				token: response.accessToken,
				provider: "facebook",
				email: null,
			});
		}
	};

	return (
		<>
			{token ? (
				<Input.Search
					prefix={<FacebookFilled />}
					placeholder={t("Ingresa tu correo electrónico")}
					autoFocus
					onSearch={value =>
						onClick({
							token: token,
							provider: "facebook",
							email: value,
						})
					}
					style={{ width: "100%" }}
				/>
			) : (
				<FacebookLogin
					appId={facebookAppID}
					autoLoad={false}
					fields="name,email,picture"
					callback={responseFacebook}
					cssClass="ingresar f"
					language="es_ES"
					render={renderProps => (
						<Button
							className="btn-facebook"
							icon={<FacebookFilled />}
							style={{ width: "100%" }}
							onClick={renderProps.onClick}>
							{t("Continuar con Facebook")}
						</Button>
					)}
				/>
			)}
		</>
	);
};

export { FacebookLoginButton as FacebookLogin };
