import { GoogleLogin as GL } from "react-google-login";
import { useState } from "react";
import { Button, Typography } from "antd";
import { GoogleOutlined } from "@ant-design/icons";
import { useTranslation } from "react-i18next";

import getConfig from "next/config";
const { googleClientID } = getConfig().publicRuntimeConfig;

export const GoogleLogin = ({ onClick }: { onClick: Function }) => {
	const { t } = useTranslation();

	const [disabled, setdisabled] = useState(false);
	const googleFailure = response => {
		if (response.error == "idpiframe_initialization_failed") setdisabled(true);
	};

	return (
		<GL
			clientId={googleClientID}
			onSuccess={response => {
				onClick({
					// @ts-ignore
					token: response.accessToken,
					provider: "google",
				});
			}}
			onFailure={googleFailure}
			cookiePolicy={"single_host_origin"}
			disabled={disabled}
			render={renderProps => (
				<Button
					className="btn-google"
					icon={<GoogleOutlined />}
					style={{ width: "100%" }}
					onClick={renderProps.onClick}
					disabled={renderProps.disabled}>
					{disabled
						? t("Para utilizar este botón debes activar las cookies de tercero")
						: t("Continuar con Google")}
				</Button>
			)}
		/>
	);
};
