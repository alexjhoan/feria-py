import React, { useEffect } from "react";

import cookie from "js-cookie";
import { useTranslation } from "react-i18next";
import { useUser } from "../User.hook";

export const Logout = () => {
	const { logout } = useUser();
	const { t } = useTranslation();

	useEffect(() => {
		if (logout.response.data && !logout.response.loading) {
			cookie.remove("frontend_token");
			localStorage.removeItem("user_md5");
			localStorage.removeItem("one_tap_logged");
			window.location.assign("/sitio/index.php?mid=login&func=logout");
		}
	}, [logout.response]);

	return <div onClick={logout.send}>{t("Cerrar Sesión")}</div>;
};
