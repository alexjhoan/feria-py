import { useState, useEffect, ReactNode } from "react";
import { InputProps } from "../Input/Input.hook";
import { ButtonTypes } from "../Button/Button.hook";
export interface RangeProps {
  minValue?: string;
  maxValue?: string;
  minPlaceholder?: string;
  maxPlaceholder?: string;
  inputSettings?: InputProps;
  buttonContent?: ReactNode;
  buttonChange?: boolean;
  buttonType?: ButtonTypes;
  onChange(min: string, max: string): void;
  status?: string;
}

export const useRange = ({
  minValue = "",
  maxValue = "",
  minPlaceholder = "Desde",
  maxPlaceholder = "Hasta",
  onChange,
  buttonChange = true,
  inputSettings = { type: "text" },
  buttonContent,
  buttonType = "primary",
  status,
}: RangeProps) => {
  const [min, setMin] = useState(minValue);
  const [max, setMax] = useState(maxValue);
  useEffect(() => {
    setMin(minValue);
    setMax(maxValue);
  }, [minValue, maxValue]);

  const saveChange = () => onChange(min, max);

  return {
    minInput: {
      ...inputSettings,
      placeholder: minPlaceholder,
      defaultValue: min,
      onChange: setMin,
      onBlur: !buttonChange ? saveChange : null,
    },
    maxInput: {
      ...inputSettings,
      placeholder: maxPlaceholder,
      defaultValue: max,
      onChange: setMax,
      onBlur: !buttonChange ? saveChange : null,
    },
    confirmButton: {
      showButton: buttonChange,
      contentButton: buttonContent,
      handleClick: saveChange,
      type: buttonType,
    },
    status,
  };
};
