import {useState, useEffect, ReactNode} from 'react';

export interface SmartDropdownProps {
  options?: object[];
  valueKey?: string;
  customKey?: string;
  categoryKey?: string;
  groupKey?: string;
  emptyText?: ReactNode;
  boldMatch?: boolean;
  match?: string;
  status?: string;
  onSelect?: (x: object) => void;
  loading?: boolean;
}

export const useSmartDropdown = ({
  options = [],
  categoryKey = 'category',
  onSelect = () => {},
  ...otherprops
}: SmartDropdownProps) => {
  /* navoptions */
  const [navOptions, setNavOptions]: Array<any> = useState([]);
  const groupNavOptions = () => {
    let res: Array<any> = [];
    options.map((o: any, i) => {
      const category = res.findIndex((r: any) => r['title'] == o[categoryKey]);
      if (category >= 0) {
        res[category]['data'].push(o);
      } else {
        const newCategory = {
          title: o[categoryKey],
          data: [o],
        };
        if (typeof o[categoryKey] == 'undefined') res.unshift(newCategory);
        else res.push(newCategory);
      }
    });
    setNavOptions(res);
  };
  /* end navoptions */

  /* options navigation  */
  const [navCursor, setNavCursor] = useState(-1);
  const increaseNavIndex = () => {
    const nc = navCursor <= options.length - 1 ? navCursor + 1 : 0;
    setNavCursor(nc);
  };
  const decreaseNavIndex = () => {
    const nc = navCursor >= 0 ? navCursor - 1 : -1;
    setNavCursor(nc);
  };
  const getNavIndexPrefix = (iNavGroup: number) => {
    return navOptions.reduce((acc: any, cur: any, idx: number) => {
      return (
        (idx >= 0 ? acc : acc.data.length) +
        (idx < iNavGroup ? cur.data.length : 0)
      );
    }, 0);
  };
  /* end options navigation  */

  useEffect(() => {
    groupNavOptions();
    setNavCursor(-1);
  }, [options]);

  return {
    navOptions,
    navCursor,
    increaseNavIndex,
    decreaseNavIndex,
    getNavIndexPrefix,

    onSelect,
    categoryKey,
    ...otherprops,
  };
};
