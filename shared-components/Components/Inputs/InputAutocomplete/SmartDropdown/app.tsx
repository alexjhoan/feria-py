import theme from '../../../../Styles/theme';

import {View, Text, SectionList, StyleSheet} from 'react-native';
import React, {FC} from 'react';
import {useSmartDropdown, SmartDropdownProps} from './SmartDropdown.hook';
import {SmartDropdownOption} from './SmartDropdownOption/app';

export const SmartDropdown: FC<SmartDropdownProps> = (
  props: SmartDropdownProps,
) => {
  const {
    loading,
    status,
    navOptions,
    navCursor,
    increaseNavIndex,
    decreaseNavIndex,
    getNavIndexPrefix,
    emptyText,
    ...optionprops
  } = useSmartDropdown(props);

  if (loading) {
    return (
      <View>
        <Text>Loading</Text>
      </View>
    );
  }

  return (
    <SectionList
      sections={navOptions}
      renderSectionHeader={({section: {title}}) => {
        if (typeof title == 'undefined') return null;
        else if (title.length == 0) return <View style={styles.separator} />;
        return <Text style={styles.title}>{title}</Text>;
      }}
      renderItem={({item}) => {
        return <SmartDropdownOption data={item} {...optionprops} />;
      }}
    />
  );
};

const styles = StyleSheet.create({
  separator: {
    borderBottomColor: theme.colors.backgroundLight,
    borderBottomWidth: 2,
  },
  title: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: theme.colors.backgroundLight,
    fontSize: theme.fontSizes.appText,
    color: theme.colors.text,
    fontWeight: 'bold',
  },
});
