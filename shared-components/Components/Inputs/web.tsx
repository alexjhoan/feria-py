import { Input } from "./Input/web";
import { Range } from "./Range/web";
import { Button } from "./Button/web";
import { CheckBox } from "./Checkbox/web";
import { Select } from "./Selects/Select/web";
import { CustomSelect } from "./Selects/CustomSelect/web";
import { ButtonSelect } from "./Selects/ButtonSelect/web";
import { InputAutocomplete } from "./InputAutocomplete/web";
import { CheckboxSelect } from "./Selects/CheckboxSelect/web";

export {
	Input,
	Range,
	Button,
	Select,
	CheckBox,
	CustomSelect,
	ButtonSelect,
	CheckboxSelect,
	InputAutocomplete,
};

// export type{ inputType, InputProps };
