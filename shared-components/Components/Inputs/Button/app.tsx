import theme from '../../../Styles/theme';
import React, {FC} from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Text} from 'react-native';
import {useButton, ButtonProps} from './Button.hook';

export const Button: FC<ButtonProps> = (props: ButtonProps) => {
  let bStyles = useButton(props);

  return (
    <>
      <TouchableOpacity
        onPress={() => props.handleClick()}
        style={[
          {
            height: bStyles.height,
            width: bStyles.width,
            backgroundColor: bStyles.background,
            borderRadius: bStyles.radius,
            paddingHorizontal: bStyles.paddingHorizontal,
            paddingVertical: bStyles.paddingVertical,
            borderColor: bStyles.color,
            borderWidth: bStyles.borderWith,
          }
        ]}
        activeOpacity={0.6}>
        <Text
          style={{
            fontSize: theme.fontSizes.appText,
            color: bStyles.color,
          }}>
          {props.content}
        </Text>
      </TouchableOpacity>
    </>
  );
};
