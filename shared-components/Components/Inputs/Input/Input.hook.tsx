import {useState, useEffect} from 'react';

export type inputType =
  | 'text'
  | 'textarea'
  | 'email'
  | 'number'
  | 'password'
  | 'search'
  | 'tel'
  | 'currency';

interface useInputProps {
  type?: inputType;
  onChange(value: any): void;
  defaultValue?: any;
}

export interface InputProps {
  placeholder?: string;
  defaultValue?: any;
  disabled?: boolean;
  focused?: boolean;
  type?: inputType;
  name?: string;
  required?: boolean;
  onChange?(value: any): void;
  onFocus?(value: any): void;
  onBlur?(value: any): void;
}

export const useInput = ({
  type = 'text',
  onChange = () => {},
  defaultValue,
}: useInputProps) => {
  const [value, setValue] = useState(
    type == 'currency' ? formatMoney(defaultValue) : defaultValue,
  );

  useEffect(() => {
    setValue(type == 'currency' ? formatMoney(defaultValue) : defaultValue);
  }, [defaultValue]);

  const handleChange = (v: any) => {
    let output = v;

    if (type == 'currency') {
      v = formatMoney(v);
      output = clearFormatMoney(v);
    }

    setValue(v);
    onChange(output);
  };

  return {
    value,
    handleChange,
  };
};

/**
 * Helpers
 */
const formatMoney = (value: any) => {
  value = isNaN(value) ? value : value.toString();
  value = value.replace(/[^\d-]/g, '');
  value = value.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
  return value;
};

const clearFormatMoney = (value: any) => {
  value = isNaN(value) ? value : value.toString();
  value = value.replace(/[^\d-]/g, '');
  return Number(value);
};
