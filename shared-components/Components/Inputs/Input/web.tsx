import { FC, useRef, useEffect } from "react";
import { useTheme } from "../../../Styles/ThemeHook";
import { InputProps, useInput } from "./Input.hook";

export const Input: FC<InputProps> = ({
	name = "",
	placeholder = "",
	defaultValue = "",
	disabled,
	focused,
	type = "text",
	required = false,
	onChange,
	onFocus = e => {},
	onBlur = e => {},
}: InputProps) => {
	const { value, handleChange } = useInput({ type, onChange, defaultValue });
	const { theme } = useTheme();
	const inputRef = useRef(null);

	useEffect(() => {
		const input = inputRef.current;
		if (input && typeof focused != "undefined") {
			focused ? input.focus() : input.blur();
		}
	}, [focused]);

	const inputProps = {
		name: name,
		disabled: disabled,
		required: required,
		placeholder: placeholder,
		ref: inputRef,
		value: value,
		onChange: (e: any) => handleChange(e.target.value),
		onBlur: (e: any) => {
			if (typeof onBlur == "function") onBlur(e.target.value);
		},
		onFocus: onFocus,
	};

	const inputType = () => {
		let res = "text";
		if (type != "currency") res = type;
		return res;
	};

	return (
		<>
			{type == "textarea" ? (
				<textarea {...inputProps}></textarea>
			) : (
				<input {...inputProps} type={inputType()} />
			)}

			<style jsx>{`
				textarea,
				input {
					appearance: none;
					border-radius: ${theme.spacing.smSpacing}px;
					border: 1px solid ${theme.colors.borderColor};
					width: 100%;
					box-sizing: border-box;
					padding: ${theme.spacing.smSpacing}px;
					display: inline-block;
					height: 100%;
					min-height: 45px;
					font-size: ${theme.fontSizes.baseFontSize};
				}
				input[type="number"]::-webkit-inner-spin-button,
				input[type="number"]::-webkit-outer-spin-button {
					-webkit-appearance: none;
					margin: 0;
				}
				textarea {
					height: auto;
				}

				textarea:disabled,
				input:disabled {
					cursor: default;
					pointer-events: none;
					touch-action: none;
					user-select: none;
					opacity: 0.5;
				}
			`}</style>
		</>
	);
};
