import { ReactNode } from "react";
import theme from "../../../Styles/_toBeDeleted_theme";
import { IconNames } from "../../Icon/Icon.hook";

export interface CheckBoxProps {
	checked: boolean;
	text: "string" | ReactNode;
	handleClick: () => void;
	type?: "checkbox" | "radio";
	disabled?: boolean;
	readonly?: boolean;
}

export const useCheckbox = ({ checked, type }: any) => {
	const color = checked ? theme.colors.primary : theme.colors.text;
	const icon = type == "checkbox" ? IconNames["ok-2"] : IconNames["dot-circled"];

	return {
		color: color,
		icon: icon,
	};
};
