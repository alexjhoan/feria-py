import React, { ReactNode, FC } from "react";
import theme from "../../../Styles/_toBeDeleted_theme";
import { CheckBoxProps } from "./Checkbox.hook";

export const CheckBox: FC<CheckBoxProps> = ({
	checked,
	text,
	handleClick,
	type = "checkbox",
	disabled,
	readonly,
}) => {
	return (
		<React.Fragment>
			<div
				className={
					"checkBox" +
					(checked ? " selected" : "") +
					(disabled ? " disabled" : "") +
					(readonly ? " readonly" : "")
				}
				onClick={() => handleClick()}>
				<div className={"check " + (type == "radio" ? "radio" : "checkbox")}>
					{type == "radio" ? <div className="icon-dot" /> : <i className="icon-ok-2" />}
				</div>
				<span className="text">{text}</span>
			</div>
			<style jsx>{`
				.checkBox {
					display: flex;
					align-items: center;
					justify-content: flex-start;
					font-size: ${theme.fontSizes.text}px;
					margin: 8px 0px;
					cursor: pointer;
					color: ${theme.colors.text};
					transition: color 300ms ease;
				}
				.checkBox:hover {
					color: ${theme.colors.textHover};
				}
				.check {
					margin-right: 8px;
				}
				.check.radio {
					border: 1px dotted ${theme.colors.textLighter};
					border-radius: 10px;
					padding: 2px;
					transition: border-color 300ms ease;
				}
				.checkBox:hover .check.radio,
				.selected .check.radio {
					border-color: ${theme.colors.primary};
				}
				.check.radio .icon-dot {
					width: 10px;
					height: 10px;
					border-radius: 6px;
					background-color: ${theme.colors.textLighter};
					transition: background-color 100ms ease;
				}
				.checkBox.selected:hover .check.radio .icon-dot {
					background-color: ${theme.colors.textLighter};
				}
				.checkBox:hover .check.radio .icon-dot,
				.selected .check.radio .icon-dot {
					background-color: ${theme.colors.primary};
				}

				.check.checkbox {
					border: 1px solid ${theme.colors.textLighter};
					width: 14px;
					height: 14px;
					border-radius: 3px;
					transition: border-color 300ms ease;
				}
				.check.checkbox i {
					display: none;
					font-size: 14px;
					color: ${theme.colors.primary};
					position: relative;
					top: -3px;
					left: -1px;
				}
				.checkBox:hover .check.checkbox {
					border-color: ${theme.colors.textLighterHover};
				}
				.selected .check.checkbox i {
					display: block;
				}

				.readonly {
					cursor: default;
					pointer-events: none;
					touch-action: none;
					user-select: none;
				}
				.disabled {
					cursor: default;
					pointer-events: none;
					touch-action: none;
					user-select: none;
					opacity: 0.5;
				}
			`}</style>
		</React.Fragment>
	);
};
