export interface ThemeModel {
	images: { logo: string; favicon: string; icono: string };
	fontFamily: string;
	headerHeight: number;
	colors: {
		primaryColor: string;
		primaryHoverColor: string;
		primaryDarkColor: string;
		secondaryColor: string;
		secondaryOpacityColor: string;
		secondaryHoverColor: string;
		textColor: string;
		textSecondaryColor: string;
		textInverseColor: string;
		backgroundColor: string;
		backgroundColorAternative: string;
		backgroundModalColor: string;
		backgroundMenuColor: string;
		linkColor: string;
		linkHoverColor: string;
		borderColor: string;
		favoriteColor: string;
		successColor: string;
		errorColor: string;
		warningColor: string;
	};
	breakPoints: {
		xs: string;
		sm: string;
		md: string;
		lg: string;
		xl: string;
		xxl: string;
	};
	spacing: {
		xsSpacing: number;
		smSpacing: number;
		mdSpacing: number;
		lgSpacing: number;
		xlSpacing: number;
		xxlSpacing: number;
	};
	fontSizes: {
		baseFontSize: string;
		baseLineHeight: number;

		lgFontSize: string;
		smFontSize: string;
		xsFontSize: string;

		xlTitle: string;
		xlLineHeight: string;
		lgTitle: string;
		lgLineHeight: string;
		mdTitle: string;
		mdLineHeight: string;
		smTitle: string;
		smLineHeight: string;
		xsTitle: string;
		xsLineHeight: string;
	};
}
