import { createContext, ReactNode } from 'react';
import { ThemeModel } from './model';
import InfoCasasTheme from '../../Styles/JSthemes/InfoCasasTheme';

const ThemeContext = createContext(null);

const ThemeProvider = ({
	children,
	initialState = InfoCasasTheme,
	active,
}: {
	children: ReactNode;
	initialState?: ThemeModel;
	active: boolean;
}) => {
	// @TODO evaluate when to initialize with other theme

	if (!active) {
		return <>{children}</>;
	}

	return (
		<ThemeContext.Provider value={initialState}>
			{children}
		</ThemeContext.Provider>
	);
};

export { ThemeContext, ThemeProvider };
