import React from 'react'
import {useTheme} from "../shared-components/Styles/ThemeHook";

export default function Footer() {
	const {theme} = useTheme()
	return (
		<React.Fragment>
			<div className="footer">
				<img src="/images/logos/infocasas.png" alt="." />
			</div>
			<style jsx global>{`
				.footer {
					background: #fa6416;
					padding: 4vh 0;
					display: flex;
					flex-direction: row;
					justify-content: center;
					align-items: center;
				}
				.footer img{
					width: auto;
			    max-height: 40px;
			    max-width: 90%;
			    height: auto;
				}
				@media (max-width: ${theme.breakPoints.sm}) {
					.footer img{
						max-height: 30px;
					}
				}
			`}</style>
		</React.Fragment>
	)
}