import React from 'react'
import { useTheme } from "../shared-components/Styles/ThemeHook";
import Banner from "./Banner"
import Footer from "./Footer"
import Advertising from "./Advertising"

export default function LandingComponent() {
	const { theme } = useTheme();
	return (
		<React.Fragment>
			<Banner />
			<Advertising />
			<Footer />
			<style jsx global>{`
				html{
					scroll-behavior: smooth;
				}
				.dNone{
					display: none!important;
				}
				.dBlock{
					display: block!important;
				}
				.dFlex{
					display: flex!important;
				}
				@media (min-width: ${theme.breakPoints.sm}){
					.dSmNone{
						display: none!important;
					}
					.dSmBlock{
						display: block!important;
					}
					.dSmFlex{
						display: flex!important;
					}
				}
				@media (min-width: ${theme.breakPoints.md}){
					.containerLanding{
						width: 90%;
						flex: 0 0 90%;
					}
					.dMdNone{
						display: none!important;
					}
					.dMdBlock{
						display: block!important;
					}
					.dMdInlineBlock{
						display: inline-block!important;
					}
					.dMdFlex{
						display: flex!important;
					}
				}
				@media (min-width: ${theme.breakPoints.lg}){
					.dLgNone{
						display: none!important;
					}
					.dLgBlock{
						display: block!important;
					}
					.dLgFlex{
						display: flex!important;
					}
				}
			`}</style>
		</React.Fragment>
	)
}