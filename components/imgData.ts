export const LargeImg = [
  {
    image: '/images/invLarge/sierra.png',
    url: 'https://www.instagram.com/sierradecorpy/',
    whatsapp: 'https://wa.me/+595972822094'
  },
  {
    image: '/images/invLarge/saccaro.png',
    url: 'https://www.instagram.com/saccaro.py/',
    whatsapp: 'https://wa.me/+595972822094'
  },
  {
    image: '/images/invLarge/yrupe.png',
    url: 'https://www.facebook.com/piletasyrupe/',
    whatsapp: 'https://wa.me/+595981110222'
  },
  {
    image: '/images/invLarge/officedesign.png',
    url: 'https://www.officedesign.com.py/',
    whatsapp: 'https://wa.me/+595972212251'
  },
  {
    image: '/images/invLarge/labellacuccina.png',
    url: 'http://www.labellacucina.com.py/',
    whatsapp: 'https://wa.me/+595984930817'
  },
  {
    image: '/images/invLarge/dsg.png',
    url: 'https://www.facebook.com/dsg.homeandoffice/',
    whatsapp: 'https://wa.me/+595972504035'
  },
  {
    image: '/images/invLarge/ingeco.png',
    url: 'http://www.ingeco.com.py/index.php',
    whatsapp: 'https://wa.me/+595971714045'
  },
  {
    image: '/images/invLarge/side.png',
    url: 'https://side.com.py/',
    whatsapp: 'https://wa.me/+595981980001'
  },
  {
    image: '/images/invLarge/ventum.jpeg',
    url: 'https://instagram.com/ventumaberturaspy?utm_medium=copy_link',
    whatsapp: 'https://wa.me/+595992589115'
  }
]

export const SmallImg = [
]