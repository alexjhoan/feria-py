import React from "react";
import {useTheme} from "../shared-components/Styles/ThemeHook";
import {Carousel} from "antd";
import {LeftOutlined, RightOutlined} from '@ant-design/icons';

export default function Banner() {
	const {theme} = useTheme()

	const settings = {
		dots: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2500,
		pauseOnHover: false,
		arrows: true,
		nextArrow: <RightOutlined />,
    prevArrow: <LeftOutlined />
	};

	return (
		<React.Fragment>
			<div className="banner dNone dMdBlock">
				<Carousel {...settings}>
					<img src='/images/banners/header_1.png' alt='banner1' />
					<img src='/images/banners/header_2.jpg' alt='banner2' />
					<img src='/images/banners/header_3.jpg' alt='banner3' />
					<img src='/images/banners/header_4.jpg' alt='banner4' />
				</Carousel>
			</div>
			<div className="banner dMdNone">
				<Carousel autoplay>
					<img src='/images/banners/header_1_mobile.png' alt='banner1' />
					<img src='/images/banners/header_2_mobile.jpg' alt='banner2' />
					<img src='/images/banners/header_3_mobile.jpg' alt='banner3' />
					<img src='/images/banners/header_4_mobile.jpg' alt='banner4' />
				</Carousel>
			</div>
			<style jsx global>{`
				.banner{
					margin-top: 64px;
				}
				.banner img {
					width: 100%;
					height: auto;
				}
				.banner .ant-carousel ul.slick-dots{
					position: relative;
					bottom: 0;
					margin-bottom: 0;
					padding: 34px 0 0;
				}
				.banner .ant-carousel ul.slick-dots li{
					width: 15px;
					height: 15px;
					margin-right: 6px;
					margin-left: 6px;
				}
				.banner .ant-carousel ul.slick-dots li button{
					border-radius: 100%;
  				background: #3A4145;
					height: 15px;
				}
				.banner .ant-carousel ul.slick-dots li.slick-active button{
					background: #3A4145;
					margin-top: 0;
				}
				.ant-carousel .slick-prev, .ant-carousel .slick-next {
					z-index: 1;
					transform: translateY(-50%);
					top: 44.5%;
				}
				.banner .ant-carousel svg{
					width: 35px;
					height: 35px;
					color: #fff;
					filter: drop-shadow(2px 4px 2px #000);
				}
				.banner .ant-carousel .slick-prev {
					left: 25px;
				}
				.banner .ant-carousel .slick-next {
					right: 35px;
				}
				
				@media (max-width: ${theme.breakPoints.xxl}) {
					.banner .ant-carousel ul.slick-dots li{
						width: 10px;
						height: 10px;
						margin-right: 4px;
						margin-left: 4px;
					}
					.banner .ant-carousel ul.slick-dots li button{
						height: 10px;
					}
				}
				@media (max-width: ${theme.breakPoints.md}) {
					.banner .ant-carousel ul.slick-dots{
						padding: 15px 0;
					}
				}
			`}</style>
		</React.Fragment>
	);
}
